#!/bin/bash

cd ../..
./vendor/bin/sail artisan ide-helper:generate
./vendor/bin/sail artisan ide-helper:meta
./vendor/bin/sail artisan ide-helper:models -W
./vendor/bin/php-cs-fixer fix ./app/Models
