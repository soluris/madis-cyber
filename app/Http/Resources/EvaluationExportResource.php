<?php

namespace App\Http\Resources;

use App\Enums\MaturityQuestionNames;
use App\Models\DangerLevelEvaluation;
use App\Models\Evaluation;
use App\Models\MaturityAnswer;
use App\Models\Measure;
use App\Models\MeasureLevel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class EvaluationExportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        /** @var Evaluation $evaluation */
        $evaluation = $this->resource;

        $res = [
            'organizationId' => $evaluation->organization_id, //    -Collectivité id
            'updatedDate' => $evaluation->updated_at->isoFormat('DD/MM/YYYY'), //    - Date de modification
            'updatedBy' => $evaluation->updated_by, //    - Modifié par
            'reference' => $evaluation->reference, //    - Référentiel
            'status' => Evaluation::STATUS[$evaluation->status], //    - Statut de l’évaluation

            // ***
//            'statusDangers' => '!TODO', //    - Statut Dangers @todo ??? quelle est la valeur attendue
            // ***
        ];

        $dangerLevelEvaluations = $evaluation->dangerLevels ?? collect();

        foreach ([1, 2, 3, 4, 5, 6] as $dangerId) {
            /** @var ?DangerLevelEvaluation $dangerLevelEvaluation */
            $dangerLevelEvaluation = $dangerLevelEvaluations->where('danger_id', '=', $dangerId)->first();
            $res["dangerLevel_$dangerId"] = (string) $dangerLevelEvaluation?->level?->name; //    - perception du danger X
        }

        $mesures = Measure::orderBy('fundamental', 'ASC')->get();
        $mesureLevels = $evaluation->measureLevels ?? collect();
        foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10] as $questionId) {
            /** @var ?Measure $mesure */
            $mesure = $mesures->get($questionId - 1);
            /** @var ?MeasureLevel $mesureLevel */
            $mesureLevel = $mesureLevels->where('measure_id', '=', $mesure->id)->first();

            $actualLevel = $mesureLevel?->actual_level;
            $res["q$questionId"] = $mesure->name;
            if ($actualLevel) {
                $res["q$questionId"] .= PHP_EOL . $mesure->getAttribute('level' . $actualLevel . '_actual_label');
            }
        }

        $maturityLevelAnswers = $evaluation->maturityLevels()->with(['question', 'answer'])->get();

        return [...$res,
            'actionPlan' => $this->_getActionPlanString($mesureLevels), //    - Plan d’action
            'motivation' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::MOTIVATION)?->name, //    - Motivation
            'execution' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::EXECUTION)?->name, //    - Execution
            'fundamental' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::FUNDAMENTALS)?->name, //    - Fondamentaux
            'organization' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::ORGANIZATION)?->name, //    - Organisation
            'maturityCyber' => $evaluation->maturity_cyber, //    - Indice de maturité
        ];
    }

    private function _getAnswerForQuestionName(?EloquentCollection $maturityLevelAnswers, MaturityQuestionNames $name): ?MaturityAnswer
    {
        return $maturityLevelAnswers?->firstWhere('question.title', '=', $name->value)?->answer;
    }

    private function _getActionPlanString(Collection $mesureLevels): string
    {
        $actionPlan = [];
        foreach ($mesureLevels as $mesureLevel) {
            if ($mesureLevel->expected_level > $mesureLevel->actual_level) {
                $actionPlan[] = implode(';', [
                    $mesureLevel->measure->short_name,
                    '[' . $mesureLevel->actual_level . ' >> ' . $mesureLevel->expected_level . ']',
                    (new Carbon($mesureLevel->end_date))->isoFormat('DD/MM/YYYY'),
                    $mesureLevel->manager,
                ]);
            }
        }

        return implode(PHP_EOL, $actionPlan);
    }
}
