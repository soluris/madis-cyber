<?php

namespace App\Http\Resources;

use App\Enums\MaturityQuestionNames;
use App\Models\DangerLevelEvaluation;
use App\Models\Evaluation;
use App\Models\MaturityAnswer;
use App\Models\MeasureLevel;
use App\Models\Organization;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationExportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     */
    public function toArray($request): array
    {
        /** @var Organization $organization */
        $organization = $this->resource;

        $evaluation = $organization->doneEvaluations()->limit(2)->get();
        /** @var ?Evaluation $lastEvaluation */
        $lastEvaluation = $evaluation->first();
        /** @var ?Evaluation $previousEvaluation */
        $previousEvaluation = $evaluation->get(1);

        $res = [
            'name' => $organization->name, //    - nom de la collectivité
            'shortName' => $organization->short_name, //    - nom court
            'territory' => (string) $organization->territory?->name, //    - libellé territoire
            'type' => $organization->type, //    - type de structure
            'insee' => $organization->address->codeInsee, //    - insee
            'siren' => $organization->siren, //    - siren
            'active' => $organization->active, //    - actif
            'website' => $organization->website, //    - site internet
            'info' => $organization->info, //    - infos complémentaires
            'fullAddress' => (string) $organization->address, //    - adresses (tous les champs)
            'referent' => (string) $organization->referent, //     - référents et responsable (tous les champs)
            'referentEmail' => $organization->referent->email, //     - référents et responsable (tous les champs)
            'referentCyber' => (string) $organization->referentCyber,
            'referentCyberEmail' => $organization->referentCyber->email,
            'referentElu' => (string) $organization->referentElu,
            'referentEluEmail' => $organization->referentElu->email,
            'lastEvaluation.cyberMaturity' => (float) $lastEvaluation?->maturity_cyber, //     - score indice maturité cyber
            'previousEvaluation.cyberMaturity' => (float) $previousEvaluation?->maturity_cyber, //    - score indice maturité cyber n-1
        ];

        $measureLevels = $lastEvaluation?->measureLevels ?? collect();

        foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10] as $measureKey) {
            /** @var ?MeasureLevel $measureLevel */
            $measureLevel = $measureLevels->get($measureKey - 1);
            $res["measure$measureKey.actualLevel"] = (string) $measureLevel?->actual_level; //    - niveau de mise en œuvre mesure X
        }

        foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10] as $measureKey) {
            /** @var ?MeasureLevel $measureLevel */
            $measureLevel = $measureLevels->get($measureKey - 1);
            $res["measure$measureKey.expectedLevel"] = (string) $measureLevel?->expected_level; //    - niveau de planification mesure X
        }

        $maturityLevelAnswers = $lastEvaluation?->maturityLevels()->with(['question', 'answer'])->get();

        $res = [...$res,
            'executionLevel' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::EXECUTION)?->name,  // niveau d'exécution
            'createdDate' => $organization->created_at->isoFormat('DD/MM/YYYY'), // date création de la collectivité
            'lastEvaluation.reference' => (string) $lastEvaluation?->reference, //     - version du référentiel
            'lastEvaluation.updatedBy' => (string) $lastEvaluation?->updated_by, //     - évaluation modifiée par
            'lastEvaluation.author' => (string) $lastEvaluation?->author, //     - évaluation crée par
        ];

        $dangerLevelEvaluations = $lastEvaluation?->dangerLevels ?? collect();

        foreach ([1, 2, 3, 4, 5, 6] as $dangerId) {
            /** @var ?DangerLevelEvaluation $dangerLevelEvaluation */
            $dangerLevelEvaluation = $dangerLevelEvaluations->where('danger_id', '=', $dangerId)->first();
            $res["dangerLevel$dangerId"] = (string) $dangerLevelEvaluation?->level->name; //    - perception du danger X
        }

        return [...$res,
            'maturityLevel.knowledge' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::KNOWLEDGE)?->name,  //     - niveau de connaissance
            'maturityLevel.organization' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::ORGANIZATION)?->name,  //     - niveau d'organisation
            'maturityLevel.motivation' => (string) $this->_getAnswerForQuestionName($maturityLevelAnswers, MaturityQuestionNames::MOTIVATION)?->name,  //     - niveau de motivation

            'alert' => (bool) $lastEvaluation?->isThresholdNotExceeded(),  //     - seuil non dépassé (true/false)
        ];
    }

    private function _getAnswerForQuestionName(?EloquentCollection $maturityLevelAnswers, MaturityQuestionNames $name): ?MaturityAnswer
    {
        return $maturityLevelAnswers?->firstWhere('question.title', '=', $name->value)?->answer;
    }
}
