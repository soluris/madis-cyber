<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EvaluationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'draft' => 'boolean|nullable',
            'status' => 'int|required',
            'current_step' => 'int|required',
            'organization_id' => 'int|required',
            'danger_levels.*.id' => 'int|nullable',
            'danger_levels.*.danger_id' => 'int|nullable',
            'danger_levels.*.danger_level_id' => 'int|nullable',
            'measure_levels' => 'array|nullable',
            'author' => 'string|nullable',
            'maturity_levels.*.maturity_question_id' => 'int|nullable',
            'maturity_levels.*.maturity_answer_id' => 'int|nullable',
            'maturity_levels.*.id' => 'int|nullable',
        ];
    }
}
