<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'firstname' => 'string|nullable',
            'lastname' => 'string|nullable',
            'email' => 'nullable|email',
            'organization_id' => 'int|nullable',
            'password' => 'string|nullable|min:12|regex:/[0-9]/',
            'role' => 'int',
            'last_login' => 'string|nullable',
            'civility' => 'string|required',
            'active' => 'boolean',
        ];
    }

    public function withValidator(Validator $validator)
    {
        $email = $validator->getData()['email'] ?? '';
        $id = $validator->getData()['id'] ?? false;

        $validator->after(
            function ($validator) use ($email, $id) {
                if (!$id && User::where('email', $email)->count()) {
                    $validator->errors()->add(
                        'email',
                        'L\'adresse email est déja utilisée. Veuillez choisir une autre adresse email.'
                    );
                }
            }
        );
    }
}
