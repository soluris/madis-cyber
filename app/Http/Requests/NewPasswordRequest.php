<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class NewPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'int|required',
            'reset_token' => 'string|required',
            'newpassword' => 'nullable',
        ];
    }

    public function withValidator(Validator $validator)
    {
        $newpassword = $validator->getData()['newpassword'] ?? '';

        $validator->after(
            function ($validator) use ($newpassword) {
                if (strlen($newpassword) < 12) {
                    $validator->errors()->add(
                        'newpassword', 'Le mot de passe doit contenir 12 caractères minimum.'
                    );
                }

                if (!preg_match('/\d/', $newpassword)) {
                    $validator->errors()->add(
                        'newpassword', 'Le mot de passe doit contenir au moins un chiffre.'
                    );
                }

                if (!preg_match('/[a-z]/', $newpassword)) {
                    $validator->errors()->add(
                        'newpassword', 'Le mot de passe doit contenir au moins une lettre minuscule.'
                    );
                }
                if (!preg_match('/[A-Z]/', $newpassword)) {
                    $validator->errors()->add(
                        'newpassword', 'Le mot de passe doit contenir au moins une lettre majuscule.'
                    );
                }
                if (!preg_match('/[^a-zA-Z0-9]/', $newpassword)) {
                    $validator->errors()->add(
                        'newpassword', 'Le mot de passe doit contenir au moins un caractère spécial.'
                    );
                }
            }
        );
    }
}
