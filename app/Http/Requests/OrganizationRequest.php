<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (isset($this->user()->role) && (User::ROLE_ADMIN === $this->user()->role)) {
            return true;
        }

        // User with role "gestionnaire" can update only his organization
        $organizationId = (int) $this->route('id');
        if ($organizationId) {
            return isset($this->user()->role) && (10 === $this->user()->role) && ($this->user()->organization_id === $organizationId);
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'string|required|max:255',
            'short_name' => 'string|required|max:255',
            'type' => 'string|required|max:255',
            'siren' => 'string|required|max:255',
            'website' => 'string|nullable|max:255',
            'info' => 'string|nullable',
            'active' => 'boolean|nullable',
            'address.address' => 'string|required|max:255',
            'address.moreInfos' => 'string|nullable|max:255',
            'address.cp' => 'string|required',
            'address.city' => 'string|required|max:255',
            'address.codeInsee' => 'string|required|max:255',
            'referent.civility' => 'string|required|max:255',
            'referent.firstname' => 'string|required|max:255',
            'referent.lastname' => 'string|required|max:255',
            'referent.function' => 'string|required|max:255',
            'referent.email' => 'email|required',
            'referent.phone' => 'string|required|max:255',
            'referent_cyber.civility' => 'string|required|max:255',
            'referent_cyber.firstname' => 'string|required|max:255',
            'referent_cyber.lastname' => 'string|required|max:255',
            'referent_cyber.function' => 'string|required|max:255',
            'referent_cyber.email' => 'email|required',
            'referent_cyber.phone' => 'string|required|max:255',
            'referent_elu.civility' => 'string|required|max:255',
            'referent_elu.firstname' => 'string|required|max:255',
            'referent_elu.lastname' => 'string|required|max:255',
            'referent_elu.function' => 'string|required|max:255',
            'referent_elu.email' => 'email|required',
            'referent_elu.phone' => 'string|required|max:255',
            'bilan1' => 'string|nullable',
            'bilan2' => 'string|nullable',
            'territory_id' => 'int|required',
        ];
    }
}
