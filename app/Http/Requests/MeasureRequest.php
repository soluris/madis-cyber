<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class MeasureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() && User::ROLE_ADMIN === Auth::user()->role;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'string|required|max:255',
            'short_name' => 'string|required|max:255',
            'levels' => 'array|min:3|max:3',
            'levels.*.actual_label' => 'string|required|max:255',
            'levels.*.preconized_label' => 'string|required|max:255',
            'levels.*.file' => 'string|nullable',
            'levels.*.description' => 'string|required',
            'levels.*.difficulty' => 'int|required',
            'levels.*.cost' => 'string|required',
            'levels.*.duration' => 'string|required',
            'levels.*.assistance' => 'int|required',
            'levels.*.info' => 'string|nullable',
        ];
    }
}
