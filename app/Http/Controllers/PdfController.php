<?php

namespace App\Http\Controllers;

use App\Models\DangerLevel;
use App\Models\Evaluation;
use App\Models\Measure;
use App\Models\MeasureLevel;
use App\Models\Organization;
use App\Models\User;
use App\Repository\GraphDataRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PdfController extends Controller
{
    public function ActionPlanPdf()
    {
        $organization_id = request()->route()->parameters['organization_id'];

        $authUser = Auth::user();
        $isAdmin = $authUser && (User::ROLE_ADMIN === $authUser->role);

        $organization = Organization::where('id', $organization_id)
            ->first();

        $evaluation = Evaluation::where('organization_id', $organization_id)
            ->where('status', Evaluation::STATUS_DONE)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->with('dangerLevels')
            ->with('measureLevels')
            ->with('measureLevels.measure')
            ->with('organization')
            ->orderBy('updated_at', 'DESC')
            ->first();

        $actionSorted = $this->getActionSorted($evaluation);

        $data = [
            'organization' => $organization,
            'evaluation' => $evaluation,
            'actions' => $actionSorted,
            'referentiel' => $evaluation->reference,
            'date' => $organization->updated_at->format('d/m/Y'),
        ];

        $pdf = App::make('snappy.pdf.wrapper');

        $pdf->setOption('header-html', view('pdf.planAction.header', $data));
        $pdf->setOption('footer-html', view('pdf.planAction.footer', $data));
        $pdf->setOption('cover', view('pdf.planAction.cover', $data));
        $pdf->setOption('xsl-style-sheet', view('pdf.planAction.toc', $data));
        $pdf->setOption('toc-header-text', 'Table des matières');
        $pdf->setOption('toc-text-size-shrink', 1);
        $pdf->setOption('toc', true);
        $pdf->setOption('margin-top', '18');
        $pdf->setOption('margin-bottom', '10');
        $pdf->setOption('margin-left', '8');
        $pdf->setOption('margin-right', '8');
        $pdf->loadView('pdf.planAction.planAction', $data);

        $date = date('Y-m-d');

        $filename = 'action_plan' . $date . '.pdf';

        if (file_exists(storage_path('app/action_plan/tmp.pdf'))) {
            unlink(storage_path('app/action_plan/tmp.pdf'));
        }

        // foreach measure, get all levels that should be added
        $measureDocs = collect($actionSorted)->map(function (MeasureLevel $ml) {
            $files = [];
            for ($i = $ml->actual_level + 1; $i <= $ml->expected_level; ++$i) {
                if (null !== $ml->measure->{'level' . $i . '_file'}) {
                    $files[] = str_replace('/storage/', storage_path('app/public/'), $ml->measure->{'level' . $i . '_file'});
                }
            }

            return $files;
        })->flatten()->join('" "');

        $pdf->save(storage_path('app/action_plan/tmp.pdf'));

        // Add measure documents to end of PDF file
        if (strlen($measureDocs) > 0) {
            $measurePdfs = '"' . $measureDocs . '"';
            $command = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile="'
                . storage_path('app/action_plan/' . $filename)
                . '" "' . storage_path('app/action_plan/tmp.pdf') . '" ' . $measurePdfs;
            exec($command);
        } else {
            Storage::disk('local')->move('action_plan/tmp.pdf', 'action_plan/' . $filename);
        }

        return Storage::disk('local')->download('action_plan/' . $filename, "Plan d'action - " . $date . '.pdf');
    }

    public function PolitiqueSecuritePdf()
    {
        $organization_id = request()->route()->parameters['organization_id'];

        $authUser = Auth::user();
        $isAdmin = $authUser && (User::ROLE_ADMIN === $authUser->role);

        $organization = Organization::with('referent')->where('id', $organization_id)
            ->first();

        $evaluation = Evaluation::where('organization_id', $organization_id)
            ->where('status', Evaluation::STATUS_DONE)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->with('dangerLevels')
            ->with('measureLevels')
            ->with('measureLevels.measure')
            ->with('organization')
            ->orderBy('updated_at', 'DESC')
            ->first();

        if (!$evaluation) {
            abort(404);
        }

        $data = [
            'organization' => $organization,
            'evaluation' => $evaluation,
            'referentiel' => $evaluation->reference,
            'date' => $organization->updated_at->format('d/m/Y'),
        ];

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pdf.politiqueSecurite.politique', $data);
        $pdf->setOption('header-html', view('pdf.politiqueSecurite.header', $data));
        $pdf->setOption('footer-html', view('pdf.politiqueSecurite.footer', $data));
        $pdf->setOption('cover', view('pdf.politiqueSecurite.cover', $data));
        $pdf->setOption('toc-header-text', 'Table des matières');
        $pdf->setOption('toc-text-size-shrink', 1);
        $pdf->setOption('toc', true);
        $pdf->setOption('margin-top', '18');
        $pdf->setOption('margin-bottom', '10');
        $pdf->setOption('margin-left', '8');
        $pdf->setOption('margin-right', '8');
        $pdf->setOption('xsl-style-sheet', view('pdf.politiqueSecurite.toc', $data));

        $filename = 'politique_securite' . date('Y-m-d') . '.pdf';

        if (file_exists(storage_path('app/politique_securite/' . $filename))) {
            unlink(storage_path('app/politique_securite/' . $filename));
        }
        $pdf->save(storage_path('app/politique_securite/' . $filename));

        return Storage::download('politique_securite/' . $filename, 'Politique de sécurité - ' . date('Y-m-d') . '.pdf');
    }

    public function DossierCyberSecuritePdf(GraphDataRepository $repository, $organization_id, $evaluation_id = null)
    {
        $authUser = Auth::user();
        $isAdmin = $authUser && (User::ROLE_ADMIN === $authUser->role);

        $organization = Organization::with('referent')->where('id', $organization_id)
            ->first();

        if ($evaluation_id) {
            $evaluation = Evaluation::where('organization_id', $organization_id)
                ->where('id', $evaluation_id)
//                ->where('status', Evaluation::STATUS_DONE)
                ->with('dangerLevels')
                ->with('measureLevels')
                ->with('measureLevels.measure')
                ->with('organization')
                ->first();
        } else {
            $evaluation = Evaluation::where('organization_id', $organization_id)
                ->where('status', Evaluation::STATUS_DONE)
                ->where('reference', env('REFERENTIEL_VERSION'))
                ->with('dangerLevels')
                ->with('measureLevels')
                ->with('measureLevels.measure')
                ->with('organization')
                ->orderBy('updated_at', 'DESC')
                ->first();
        }

        $actionSorted = $this->getActionSorted($evaluation);

        $dangerLevels = DangerLevel::orderBy('value', 'asc')->get();
        $attackData = $repository->getAttackDataForEvaluation($evaluation);

        $attackColors = $attackData['labels']->map(function ($l, $i) use ($attackData) {
            $color = GraphDataRepository::hslToRgb($i / count($attackData['labels']), 1, 0.7);
            // return $color;
            return '#' . sprintf('%x%x%x', $color[0], $color[1], $color[2]);
        });

        $data = [
            'organization' => $organization,
            'evaluation' => $evaluation,
            'measures' => Measure::all(),
            'actions' => $actionSorted,
            'referentiel' => $evaluation->reference,
            'legendLevels' => $dangerLevels,
            'residualRisks' => $evaluation->getResidualDangers(),
            'measuresData' => $repository->getMeasuresDataForEvaluation($evaluation),
            'attackData' => $attackData,
            'attackColors' => $attackColors->toArray(),
            'expositionData' => $repository->getExpositionDataForEvaluation($evaluation),
            'risksData' => $repository->getRisksDataForEvaluation($evaluation),
            'futureRisksData' => $repository->getFutureRisksDataForEvaluation($evaluation),
            'maturityData' => $repository->getMaturityDataForEvaluation($evaluation),
            'date' => $evaluation->updated_at->format('d/m/Y'),
        ];

//        return view('pdf.dossierCyberSecurite.cover', $data);

        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->loadView('pdf.dossierCyberSecurite.dossier', $data);
        $pdf->setOption('header-html', view('pdf.dossierCyberSecurite.header', $data));
        $pdf->setOption('footer-html', view('pdf.dossierCyberSecurite.footer', $data));
        $pdf->setOption('cover', view('pdf.dossierCyberSecurite.cover', $data));
        $pdf->setOption('toc-header-text', 'Table des matières');
        $pdf->setOption('toc-text-size-shrink', 1);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 1000);
        $pdf->setOption('debug-javascript', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        $pdf->setOption('toc', true);
        $pdf->setOption('margin-top', '10');
        $pdf->setOption('margin-bottom', '10');
        $pdf->setOption('margin-left', '8');
        $pdf->setOption('margin-right', '8');
        $pdf->setOption('xsl-style-sheet', view('pdf.dossierCyberSecurite.toc', $data));

        $tempName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . Str::random() . '.pdf';
        $pdf->save($tempName);

        return response()->download($tempName, 'Dossier_cyber_securite - ' . date('Y-m-d') . '.pdf');
    }

    public function getActionSorted(Evaluation|null $evaluation): array
    {
        if (!$evaluation) {
            abort(404);
        }

        $collectionAction = collect($evaluation->measureLevels)->filter(function (MeasureLevel $ml) {
            return null !== $ml->expected_level && 0 !== $ml->expected_level && $ml->actual_level < 3 && $ml->actual_level !== $ml->expected_level;
        });
        $actionSorted = $collectionAction->sortBy('end_date');

        return $actionSorted->values()->all();
    }
}
