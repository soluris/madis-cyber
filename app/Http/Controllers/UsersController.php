<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewPasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function createPassword()
    {
        $parameters = request()->route()->parameters;
        $data = [];
        $data['id'] = $parameters['id'];
        $data['reset_token'] = $parameters['reset_token'];

        $user = User::where('id', $data['id'])
            ->first();

        if ($user->reset_token !== $data['reset_token'] || (!$user)) {
            return redirect('/login'); // ->with('message', 'Votre mot de passe a été modifié.');
        }

        return view('emails/users/newpassword', $data);
    }

    public function storePassword(NewPasswordRequest $request)
    {
        $validated = $request->validated();

        $user = User::where('id', $validated['id'])
            ->where('reset_token', $validated['reset_token'])
            ->first();

        if ($user) {
            $user->password = Hash::make($validated['newpassword']);
            $user->reset_token = null;
            $user->save();
        }

        // return view('app');
        return Redirect::to('/login')->with('message', 'Votre mot de passe a été modifié.');
    }
}
