<?php

namespace App\Http\Controllers;

use App\Http\Resources\EvaluationExportResource;
use App\Http\Resources\OrganizationExportResource;
use App\Models\Evaluation;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Spatie\SimpleExcel\SimpleExcelWriter;

class ExportController extends Controller
{
    public function organizations(Request $request): Collection|null
    {
        $ext = $request->route('ext');
        $filename = "structures.$ext";
        $data = Organization::with(['address', 'referent', 'referentCyber', 'referentElu', 'territory', 'evaluations', 'evaluations.dangerLevels'])->get();
        $rows = OrganizationExportResource::collection($data)->toArray($request);
        $header = $this->_getRowsHeader($rows[0], 'exports.organization');

        if ('json' === $ext) {
            //            die('<pre>'.print_r($rows).'</pre>');
            return collect(compact('header', 'rows'));
        }

        $rows = [$header, ...$rows];

        SimpleExcelWriter::streamDownload($filename)
            ->noHeaderRow()
            ->addRows($rows)
            ->toBrowser();

        return null;
    }

    public function evaluations(Request $request): Collection|null
    {
        $ext = $request->route('ext');
        $filename = "evaluations.$ext";
        $data = Evaluation::with(['dangerLevels'])->get();
        $rows = EvaluationExportResource::collection($data)->toArray($request);
        $header = $this->_getRowsHeader($rows[0], 'exports.evaluation');

        if ('json' === $ext) {
            return collect(compact('header', 'rows'));
        }

        $rows = [$header, ...$rows];

        SimpleExcelWriter::streamDownload($filename)
            ->noHeaderRow()
            ->addRows($rows)
            ->toBrowser();

        return null;
    }

    private function _getRowsHeader(array $cols, string $prefix): array
    {
        $keys = collect($cols)->keys();

        return $keys->map(function ($item) use ($prefix) {
            return trans($prefix . '.' . $item);
        })->toArray();
    }
}
