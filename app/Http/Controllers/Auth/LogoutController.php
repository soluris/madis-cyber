<?php
/**
 * Login.php.
 *
 * Created By: jonathan
 * Date: 12/10/2020
 * Time: 10:05
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    public function logout()
    {
        $user = Auth::user();

        if ($user) {
            $user->tokens()->delete();

            Auth::guard('web')->logout();
        }

        return redirect(route('login'));
    }
}
