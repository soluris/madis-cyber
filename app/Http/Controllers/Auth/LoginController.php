<?php
/**
 * Login.php.
 *
 * Created By: jonathan
 * Date: 12/10/2020
 * Time: 10:05
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $cred = $request->only('email', 'password');

        Auth::attempt($cred);
        /**
         * @var User|null $user
         */
        $user = Auth::user();
        $userCollectivityAccess = true;

        if (!isset($user) || !isset($user->role)) {
            abort(403, 'Votre email ou mot de passe est incorrect');
        }

        // Control if collectivity of user is active
        if ($user->role > User::ROLE_ADMIN) {
            $userCollectivityAccess = ($user->organization && $user->organization->active);
        }

        if ($user->active && $userCollectivityAccess) {
            $user->last_login = Carbon::now();
            $user->save();
            $token = $user->createToken('madis-token');

            return $token->plainTextToken;
        }

        if ((!$user->active) || !$userCollectivityAccess) {
            Auth::logout();
            abort(403, "Compte inactif. Veuillez vous rapprocher d'un administrateur.");
        }

        Auth::logout();
        abort(403, 'Votre email ou mot de passe est incorrect');
    }

    public function impersonate(Request $request)
    {
        $id = (int) $request->id;
        /** @var User $currentUser */
        $currentUser = Auth::user();

        if ($currentUser->active && User::ROLE_ADMIN === $currentUser->role) {
            /** @var User $user */
            $user = User::find($id);
            if ($user->active) {
                $token = $user->createToken('madis-token');
                $currentUser->impersonate($user);

                return $token->plainTextToken;
            }
        }

        return abort(403);
    }

    public function leaveImpersonation()
    {
        /** @var User $currentUser */
        $currentUser = Auth::user();
        $currentUser->leaveImpersonation();
    }
}
