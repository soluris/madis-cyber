<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MaturityQuestion;

class MaturityController extends Controller
{
    public function all()
    {
        return MaturityQuestion::with('answers')->get();
    }
}
