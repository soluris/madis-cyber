<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MailRequest;
use App\Http\Requests\UserRequest;
use App\Mail\NotifForgotPassword;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    public function all($role = null)
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        // Have to be at least manager to get other users
        if ($user->role <= User::ROLE_MANAGER) {
            // Can only get users with our role or a role with less permissions
            if (User::ROLE_ADMIN === $user->role) {
                // If admin return all users across all organizations
                return User::where('role', '>=', $user->role)
                    ->with('organization')
                    ->get();
            }

            // Otherwise only from our organization
            return User::where('role', '>=', $user->role)
                ->where('organization_id', $user->organization_id)
                ->with('organization')
                ->get();
        }

        return abort(403);
    }

    public function me()
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->active || ($user->role > User::ROLE_ADMIN && $user->organization && !$user->organization->active)) {
            auth()->guard('web')->logout();
            abort(403, "Compte inactif. Veuillez vous rapprocher d'un administrateur.");
        }

        return User::with('organization')->find(Auth::user()->id);
    }

    public function save(UserRequest $request, $id = null)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();
        $isAdmin = User::ROLE_ADMIN === $authUser->role;

        if ($isAdmin || $authUser->id === (int) $id) {
            if ($id) {
                $user = User::find((int) $id);
            } else {
                $user = new User();
            }
            if (!$user) {
                abort(404);
            }

            // Update or create user
            $data = $request->validated();

            $user->firstname = $data['firstname'];
            $user->lastname = $data['lastname'];
            $user->email = $data['email'];
            $user->civility = $data['civility'];
            $user->organization_id = $data['organization_id'] ?? null;
            $user->active = $data['active'] ?? false;
            if ($isAdmin) {
                $user->role = $data['role'];
            }

            // Set password if it was changed
            if (isset($data['password'])) {
                $user->password = Hash::make($data['password']);
            }
            // Set random password if new user
            if (!$id && !isset($data['password'])) {
                $user->password = Hash::make(Str::random());
                $user->remember_token = Str::random(20);
            }

            $user->save();

            return User::with('organization')->find($user->id);
        }

        abort(403);
    }

    public function delete($id)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();

        if ($authUser->role <= User::ROLE_MANAGER) {
            /**
             * @var User $user
             */
            $user = User::find((int) $id);
            // Can only get users with our role or a role with less permissions
            if (User::ROLE_ADMIN === $authUser->role) {
                // If admin, he can delete every users
                $user->delete();

                return abort(204);
            }

            $organization = $authUser->organization_id;
            // Manager can delete only users of his organization and can t delete admin users
            if ($user->organization_id && ($user->organization_id === $organization) && (User::ROLE_ADMIN !== $user->role)) {
                $user->delete();

                return abort(204);
            }
        }

        return abort(403);
    }

    public function forgetpwd(MailRequest $request)
    {
        $mail = $request->validated();

        $user = User::where('email', $mail)->where('active', 1)
            ->first();

        if (null !== $user) {
            $user->reset_token = Str::random(32);
            $user->save();
            Mail::to($user->email)->send(new NotifForgotPassword($user));

            return $user;
        }

        abort(404, 'Utilisateur inconnu');
    }
}
