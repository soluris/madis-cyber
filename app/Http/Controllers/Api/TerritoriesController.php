<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TerritoryRequest;
use App\Models\Territory;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class TerritoriesController extends Controller
{
    public function all()
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        // Can only get users with our role or a role with less permissions
        if (User::ROLE_ADMIN === $user->role) {
            // If admin return all users across all organizations
            return Territory::with('organizations')
                ->get();
        }
        abort(403);
    }

    public function save(TerritoryRequest $request, $id = null)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();
        // Update or create user
        $data = json_decode($request->getContent(), true);

        if (User::ROLE_ADMIN === $authUser->role) {
            if ($id) {
                $territory = Territory::find((int) $id);
            } else {
                $territory = new Territory();
            }
            $territory->name = $data['name'];
            $territory->save();
        } else {
            abort(403);
        }

        return Territory::with('organizations')
            ->find($territory->id);
    }

    public function delete($id)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();
        $isAdmin = User::ROLE_ADMIN === $authUser->role;

        if ($isAdmin) {
            /**
             * @var Territory $territory
             */
            $territory = Territory::find((int) $id);
            $territory->delete();

            return abort(204);
        }

        return abort(403);
    }
}
