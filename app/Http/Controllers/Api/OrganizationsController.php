<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrganizationRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class OrganizationsController extends Controller
{
    public function all()
    {
        /**
         * @var User $user
         */
        $user = Auth::user();

        // Can only get users with our role or a role with less permissions
        if (User::ROLE_ADMIN === $user->role) {
            // If admin return all users across all organizations
            return Organization::with('referent')
                ->with('referentCyber')
                ->with('referentElu')
                ->with('address')
                ->with('territory')
                ->get();
        }
        // Otherwise only from our organization
        return Organization::where('id', $user->organization_id)
                ->with('referent')
                ->with('referentCyber')
                ->with('referentElu')
                ->with('address')
                ->with('territory')
                ->get();
    }

    public function save(OrganizationRequest $request, $id = null)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();
        $isAdmin = User::ROLE_ADMIN === $authUser->role;
        $isManager = User::ROLE_MANAGER === $authUser->role;

        if ($isAdmin || ($isManager && ($authUser->organization_id === (int) $id))) {
            if ($id) {
                $organization = Organization::find((int) $id);
                $address = Address::find((int) $organization->address_id);
                $referent = Contact::find((int) $organization->referent_id);
                $referent_cyber = Contact::find((int) $organization->referent_cyber_id);
                $referent_elu = Contact::find((int) $organization->referent_elu_id);

                if ($isManager && ($authUser->organization_id !== $organization->id)) {
                    abort(403);
                }
            } else {
                if ($isAdmin) {
                    $organization = new Organization();
                    $address = new Address();
                    $referent = new Contact();
                    $referent_cyber = new Contact();
                    $referent_elu = new Contact();
                } else {
                    abort(403);
                }
            }
            if (!$organization) {
                abort(404);
            }

            // Update or create user
            $data = json_decode($request->getContent(), true);

            // Address updates
            $address->address = $data['address']['address'];
            $address->moreInfos = $data['address']['moreInfos'];
            $address->cp = $data['address']['cp'];
            $address->city = $data['address']['city'];
            $address->codeInsee = $data['address']['codeInsee'];
            $address->lat = $data['address']['lat'] ?? null;
            $address->lng = $data['address']['lng'] ?? null;
            $address->save();

            $organization->referent_id = $this->UpdateReferent($referent, $data['referent']);
            $organization->referent_cyber_id = $this->UpdateReferent($referent_cyber, $data['referent_cyber']);
            $organization->referent_elu_id = $this->UpdateReferent($referent_elu, $data['referent_elu']);

            // Fix https://app.asana.com/0/1202404842967175/1202682370828220
            if ($isAdmin) {
                $organization->name = $data['name'];
                $organization->short_name = $data['short_name'];
                $organization->type = $data['type'];
                $organization->siren = $data['siren'];
                $organization->active = isset($data['active']) ? $data['active'] : false;
                $organization->website = $data['website'];
                $organization->info = $data['info'];
            }

            $organization->bilan1 = $data['bilan1'] ?? null;
            $organization->bilan2 = $data['bilan2'] ?? null;
            $organization->address_id = $address->id ?? null;
            $organization->territory_id = $data['territory_id'];
            $organization->save();

            return Organization::with('referent')
                ->with('referentCyber')
                ->with('referentElu')
                ->with('address')
                ->with('territory')
                ->find($organization->id);
        }
        abort(403);
    }

    public function delete($id)
    {
        /**
         * @var User $authUser
         */
        $authUser = Auth::user();
        $isAdmin = User::ROLE_ADMIN === $authUser->role;

        if ($isAdmin) {
            /**
             * @var Organization $organization
             */
            $organization = Organization::find((int) $id);
            $organization->delete();

            return abort(204);
        }

        return abort(403);
    }

    public function UpdateReferent($contact, $data)
    {
        $contact->civility = $data['civility'] ?? null;
        $contact->firstname = $data['firstname'] ?? null;
        $contact->lastname = $data['lastname'] ?? null;
        $contact->function = $data['function'] ?? null;
        $contact->email = $data['email'] ?? null;
        $contact->phone = $data['phone'] ?? null;
        $contact->save();

        return $contact->id;
    }
}
