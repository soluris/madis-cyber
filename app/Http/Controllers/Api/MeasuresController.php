<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MeasureRequest;
use App\Models\Measure;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MeasuresController extends Controller
{
    public function all()
    {
        return Measure::orderBy('fundamental', 'ASC')->get()->map(function (Measure $m) {
            return $m->withConvertedLevels();
        });
    }

    public function save(MeasureRequest $request, $id = null)
    {
        $authUser = Auth::user();
        $isAdmin = $authUser && (User::ROLE_ADMIN === $authUser->role);

        if ($isAdmin) {
            if ($id) {
                $measure = Measure::find((int) $id);
            } else {
                $measure = new Measure();
            }
            if (!$measure) {
                abort(404);
            }

            // Update or create user
            $data = $request->validated();

            $measure->name = $data['name'];
            $measure->short_name = $data['short_name'];

            for ($i = 0; $i < 3; ++$i) {
                $this->saveLevel($measure, $data, $i);
            }

            $measure->save();

            return $measure->withConvertedLevels();
        }

        abort(403);
    }

    private function saveLevel(Measure $measure, $data, $level)
    {
        $niveau = $level + 1;

        if ($data['levels'] && is_array($data['levels']) && count($data['levels']) > $level) {
            $measure->{'level' . $niveau . '_actual_label'} = $data['levels'][$level]['actual_label'];
            $measure->{'level' . $niveau . '_preconized_label'} = $data['levels'][$level]['preconized_label'];
//            $measure->{'level' . $niveau . '_description'} = $data['levels'][$level]['description'] ?? null;
            $measure->{'level' . $niveau . '_difficulty'} = $data['levels'][$level]['difficulty'];
            $measure->{'level' . $niveau . '_cost'} = $data['levels'][$level]['cost'];
            $measure->{'level' . $niveau . '_duration'} = $data['levels'][$level]['duration'];
            $measure->{'level' . $niveau . '_assistance'} = $data['levels'][$level]['assistance'];
            $measure->{'level' . $niveau . '_info'} = $data['levels'][$level]['info'] ?? null;

            if (isset($data['levels'][$level]['file'])) {
                $measure->{'level' . $niveau . '_file'} = '/storage' . $this->storeFileLevel($data['levels'][$level]['file'], $measure, $niveau);
            }
        }
    }

    private function storeFileLevel($file, Measure $measure, int $niveau): string
    {
        // todo mettre le bon contenu dans le fichier
        $image_parts = explode(';base64,', $file);
        $image_type_aux = explode('/', $image_parts[0]);
        $image_type = $image_type_aux[1];
        if ('pdf' !== $image_type) {
            abort(422, 'Le fichier du niveau ' . $niveau . ' doit être un fichier PDF');
        }

        $image_base64 = base64_decode($image_parts[1]);
        $path = '/documents/mesures/' . Str::slug($measure->name) . '-' . $measure->id . '/' . Str::slug($measure->name) . '-niveau-' . $niveau . '.' . $image_type;
        Storage::put('public' . $path, $image_base64);

        return $path;
    }

    public function download($id)
    {
        $authUser = Auth::user();

        if ($authUser->role <= User::ROLE_MANAGER) {
            $measure = Measure::find((int) $id);

            $zipFileName = Str::slug($measure->name) . '.zip';

            $path = storage_path('app/public/documents/mesures/' . $zipFileName);

            $zip = new \ZipArchive();

            if (true === $zip->open($path, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                for ($i = 1; $i < 4; ++$i) {
                    if (isset($measure->{'level' . $i . '_file'}) && ('' !== $measure->{'level' . $i . '_file'})) {
                        $file = public_path($measure->{'level' . $i . '_file'});
                        if (file_exists($file)) {
                            $fileName = Str::slug($measure->name) . '-niveau-' . $i . '.pdf';
                            $zip->addFile($file, $fileName);
                        }
                    }
                }

                $zip->close();
            }

            if (file_exists($path)) {
                return response()->download($path, $zipFileName, ['Content-Type' => 'application/octet-stream']);
            }

            return redirect('/mesures');
        }

        abort(403);
    }

    public function downloadAll()
    {
        $authUser = Auth::user();

        if ($authUser->role <= User::ROLE_MANAGER) {
            $zipFileName = 'Fiches-' . Carbon::now()->format('d-m-Y') . '.zip';
            $path = storage_path('app/public/documents/mesures/' . $zipFileName);

            $zip = new \ZipArchive();

            if (true === $zip->open($path, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                $measures = Measure::All();
                foreach ($measures as $measure) {
                    for ($i = 1; $i < 4; ++$i) {
                        if (isset($measure->{'level' . $i . '_file'}) && ('' !== $measure->{'level' . $i . '_file'})) {
                            $file = public_path($measure->{'level' . $i . '_file'});
                            if (file_exists($file)) {
                                $zip->addFile($file, Str::slug($measure->name) . '-' . $i . '.pdf');
                            }
                        }
                    }
                }

                $zip->close();
            }
            if (file_exists($path)) {
                return response()->download($path, $zipFileName, ['Content-Type' => 'application/octet-stream']);
            }

            return redirect('/mesures');
        }

        abort(403);
    }
}
