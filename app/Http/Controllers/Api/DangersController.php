<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Danger;
use App\Models\DangerLevel;

class DangersController extends Controller
{
    public function all()
    {
        $levels = DangerLevel::orderBy('value', 'asc')->get();
        $dangers = Danger::all()->map(function (Danger $d) use ($levels) {
            $danger = $d->toArray();
            $danger['levels'] = $levels->toArray();

            return $danger;
        });

        return $dangers;
    }
}
