<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Evaluation;
use App\Models\User;
use App\Repository\GraphDataRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class GraphDataController extends Controller
{
    private GraphDataRepository $repository;

    public function __construct(GraphDataRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id Evaluation id
     */
    public function measures(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getMeasuresDataForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function bestMeasures(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getBestMeasuresForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function risks(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getRisksDataForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function maturity(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getMaturityDataForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function futureRisks(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getFutureRisksDataForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function exposition(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getExpositionDataForEvaluation($evaluation);
    }

    /**
     * @param int $id Evaluation id
     */
    public function attack(int $id): Collection|array|\Illuminate\Support\Collection
    {
        $evaluation = $this->getEvaluation($id);

        return $this->repository->getAttackDataForEvaluation($evaluation);
    }

    /**
     * Get ActionTerritory graph data.
     */
    public function actionterritory(): Collection|array|\Illuminate\Support\Collection
    {
        return $this->repository->getActionTerritoryGraphData();
    }

    /**
     * Get organizations data.
     */
    public function organizations($territory_id = null): Collection|array|\Illuminate\Support\Collection
    {
        return $this->repository->getOrganizationData($territory_id);
    }

    private function getEvaluation($id): Evaluation
    {
        /**
         * @var User $user
         */
        $user = Auth::user();
        /**
         * @var Evaluation|null $evaluation
         */
        $evaluation = Evaluation::find($id);
        if (is_null($evaluation) || (User::ROLE_ADMIN !== $user->role && $user->organization_id !== $evaluation->organization_id)) {
            abort(404);
        }

        return $evaluation;
    }
}
