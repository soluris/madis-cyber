<?php
/**
 * DocumentTypeController.php.
 *
 * Created By: jonathan
 * Date: 15/10/2020
 * Time: 12:59
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Scenario;
use Illuminate\Database\Eloquent\Collection;

class ScenariosController extends Controller
{
    public function all(): Collection|array
    {
        return Scenario::with(['dangers', 'measures'])->get();
    }
}
