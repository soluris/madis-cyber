<?php

namespace App\Exceptions;

class MissingMaturityAnswerException extends \Exception
{
}
