<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifForgotPassword extends Mailable
{
    use Queueable;
    use SerializesModels;
    public $user;

    /**
     * /**
     * Create a new message instance.
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.users.NotifForgotPassword')
            ->subject('Mot de passe oublié')
            ->with(['user' => $this->user]);
    }
}
