<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifNewUser extends Mailable
{
    use Queueable;
    use SerializesModels;

    public User $user;
    public string $password;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->password = $user->password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): NotifNewUser
    {
        return $this->view('emails.users.NotifNewUser')->subject('Bienvenue sur Madis Cyber');
    }
}
