<?php

namespace App\Repository;

use App\Exceptions\MissingMaturityAnswerException;
use App\Models\Evaluation;
use App\Models\EvaluationMaturityAnswer;
use App\Models\MaturityQuestion;
use Illuminate\Support\Collection;

class EvaluationRepository
{
    /**
     * Save maturity answers.
     *
     * @throws MissingMaturityAnswerException
     */
    public function saveMaturity(Evaluation $evaluation, Collection $answers, int $draft = 0)
    {
        $questions = MaturityQuestion::where('auto', false)->get();
        foreach ($questions as $question) {
            // Get answer from sent values
            $answer = $answers->filter(function ($a) use ($question) {
                return $a['maturity_question_id'] === $question->id;
            })->first();
            if ((!isset($answer) || !isset($answer['maturity_answer_id'])) && 0 === $draft) {
                throw new MissingMaturityAnswerException();
            }

            $maturity = isset($answer['id']) ? EvaluationMaturityAnswer::find($answer['id']) : new EvaluationMaturityAnswer();
            $maturity->maturity_question_id = $answer['maturity_question_id'];
            $maturity->maturity_answer_id = $answer['maturity_answer_id'];
            $maturity->evaluation_id = $evaluation->id;
            $maturity->save();
        }

        $evaluation->saveAutomaticAnswers();
    }
}
