<?php

namespace App\Models;

use App\Calculator\MaturityAnswerLevelCalculator;
use Carbon\Carbon;
use Database\Factories\EvaluationFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Class Evaluation.
 *
 * @property int                                                             $id
 * @property \DateTime                                                       $createdAt
 * @property Carbon                                                          $updatedAt
 * @property Carbon                                                          $updated_at
 * @property \DateTime                                                       $created_at
 * @property DangerLevelEvaluation[]|Collection<DangerLevelEvaluation>       $dangerLevels
 * @property DangerLevelEvaluation[]|Collection<DangerLevelEvaluation>       $danger_levels
 * @property MeasureLevel[]|Collection<MeasureLevel>                         $measureLevels
 * @property MeasureLevel[]|Collection<MeasureLevel>                         $measure_levels
 * @property EvaluationMaturityAnswer[]|Collection<EvaluationMaturityAnswer> $maturityLevels
 * @property int                                                             $status
 * @property Organization                                                    $organization
 * @property int                                                             $organization_id
 * @property int                                                             $current_step
 * @property int                                                             $current_step_status
 * @property string                                                          $author
 * @property string                                                          $updated_by
 * @property float                                                           $maturityCyber
 * @property float                                                           $maturity_cyber
 * @property string                                                          $reference
 * @property int|null                                                        $danger_levels_count
 * @property int|null                                                        $maturity_levels_count
 * @property int|null                                                        $measure_levels_count
 *
 * @method static EvaluationFactory  factory(...$parameters)
 * @method static Builder|Evaluation newModelQuery()
 * @method static Builder|Evaluation newQuery()
 * @method static Builder|Evaluation query()
 * @method static Builder|Evaluation whereAuthor($value)
 * @method static Builder|Evaluation whereCreatedAt($value)
 * @method static Builder|Evaluation whereCurrentStep($value)
 * @method static Builder|Evaluation whereId($value)
 * @method static Builder|Evaluation whereOrganizationId($value)
 * @method static Builder|Evaluation whereReference($value)
 * @method static Builder|Evaluation whereStatus($value)
 * @method static Builder|Evaluation whereUpdatedAt($value)
 * @method static Builder|Evaluation whereUpdatedBy($value)
 *
 * @mixin Eloquent
 */
class Evaluation extends Model
{
    use HasFactory;
    use HasTimestamps;

    public const STATUS_ONGOING = 1;
    public const STATUS_DONE = 2;

    public const STEP_DANGERS = 1;
    public const STEP_MEASURES = 2;
    public const STEP_RECAP = 3;
    public const STEP_ACTIONS = 4;
    public const STEP_MATURITY = 5;
    public const STEP_RESULTS = 6;

    public const STATUS = [
        self::STATUS_ONGOING => 'En cours',
        self::STATUS_DONE => 'Terminé',
    ];

    protected $appends = [
        'maturity_cyber',
    ];

    protected $casts = [
        'maturity_cyber' => 'float',
    ];

    public function dangerLevels(): HasMany
    {
        return $this->hasMany(DangerLevelEvaluation::class);
    }

    public function measureLevels(): HasMany
    {
        return $this->hasMany(MeasureLevel::class);
    }

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function maturityLevels(): HasMany
    {
        return $this->hasMany(EvaluationMaturityAnswer::class);
    }

    public function getResidualDangers()
    {
        // Get all scenarios
        // For each scenario
        // For each measure
        // check if level is zero
        // if all measures are 0 for this scenario then it is a residual risk

        $scenarios = Scenario::with(['dangers', 'measures'])->get();

        $residual_scenarios = $scenarios;

        foreach ($scenarios as $scenario) {
            foreach ($scenario->measures as $measure) {
                // Get the level of this measure for this evaluation
                $level = $this->measureLevels->first(function (MeasureLevel $ml) use ($measure) {
                    return $ml->measure_id === $measure->id;
                });
                if ($level->actual_level > 0) {
                    $residual_scenarios = $residual_scenarios->reject($scenario);
                }
            }
        }

        return $residual_scenarios->map(function (Scenario $scenario) {
            return $scenario->dangers;
        })->flatten(1)->unique('id');
    }

    public function getMaturityCyberAttribute(): float
    {
        if ($this->current_step < self::STEP_RESULTS) {
            return 0.0;
        }
        $this->saveAutomaticAnswers();

        $evaluation = Evaluation::find($this->id);

        $maturityLevels = $evaluation->maturityLevels;

        return $maturityLevels->map(function (EvaluationMaturityAnswer $ml) {
            return $ml->answer->level;
        })->avg();
    }

    public function saveAutomaticAnswers()
    {
        $questions = MaturityQuestion::where('auto', true)->get();
        foreach ($questions as $question) {
            // Calculate answer based on data
            // For "execution"
            // Get completed evaluation before this one
            // If there are none then all calculated levels are 0
            // (nombre de mesure basculée en mesure en place dans l'évaluation en cours/nombre de mesure prévue dans évaluation précédente) *100
            // 0 => level 0
            // 0 < calculated_level < 20 => level 1
            // 20 < calculated_level < 80 => level 2
            // calculated_level >= 80 => level 3
            // For "fondamentaux"
            // MOYENNE.INF( niveau de la mesure en place sauvegarde, niveau de la mesure en place mot de passe, niveau de la mesure en place mise à jour , niveau de la mesure en place sensibilisation)

            $calculated_level = 0;
            if ('Fondamentaux' === $question->title) {
                $calculated_level = MaturityAnswerLevelCalculator::calculateFundamentalLevel($this);
            }

            if ('Exécution' === $question->title) {
                // Get previous completed evaluation
                /**
                 * @var Evaluation|null $previousEvaluation
                 */
                $previousEvaluation = Evaluation::where('status', Evaluation::STATUS_DONE)
                    ->where('organization_id', $this->organization_id)
                    ->whereDate('updated_at', '<', $this->updated_at->toDateString())
                    ->orderBy('updated_at', 'DESC')
                    ->first();
                $calculated_level = MaturityAnswerLevelCalculator::calculateExecutionLevel($this, $previousEvaluation);
            }

            $answer = MaturityAnswer::where('maturity_question_id', $question->id)
                ->where('level', $calculated_level)
                ->first();

            $evalAnswer = EvaluationMaturityAnswer::where('evaluation_id', $this->id)
                ->where('maturity_question_id', $question->id)
                ->first();
            if (!$evalAnswer) {
                $evalAnswer = new EvaluationMaturityAnswer();
            }

            $evalAnswer->maturity_question_id = $question->id;
            $evalAnswer->maturity_answer_id = $answer?->id;
            $evalAnswer->evaluation_id = $this->id;
            $evalAnswer->save();
        }
    }

    public static function getThreshold(): float
    {
        return env('SEUIL_ALERTE'); // TODO use config instead of direct ENV
    }

    /**
     * true if OK.
     */
    public function isThresholdNotExceeded(): bool
    {
        foreach ($this->measureLevels as $measureLevel) {
            if ($measureLevel->actual_level < self::getThreshold()) {
                return true;
            }
        }

        return false;
    }
}
