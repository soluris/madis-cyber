<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Scenario.
 *
 * @property int                  $id
 * @property string               $name
 * @property Danger[]|Collection  $dangers
 * @property Measure[]|Collection $measures
 * @property Carbon|null          $created_at
 * @property Carbon|null          $updated_at
 * @property int|null             $dangers_count
 * @property int|null             $measures_count
 *
 * @method static Builder|Scenario newModelQuery()
 * @method static Builder|Scenario newQuery()
 * @method static Builder|Scenario query()
 * @method static Builder|Scenario whereCreatedAt($value)
 * @method static Builder|Scenario whereId($value)
 * @method static Builder|Scenario whereName($value)
 * @method static Builder|Scenario whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Scenario extends Model
{
    use HasFactory;
    use HasTimestamps;

    public function dangers(): BelongsToMany
    {
        return $this->belongsToMany(Danger::class);
    }

    public function measures(): BelongsToMany
    {
        return $this->belongsToMany(Measure::class);
    }
}
