<?php

namespace App\Models;

use Database\Factories\DangerFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Danger.
 *
 * @property int                   $id
 * @property string                $name
 * @property string                $description
 * @property Scenario[]|Collection $scenarios
 * @property Carbon|null           $created_at
 * @property Carbon|null           $updated_at
 * @property int|null              $scenarios_count
 *
 * @method static DangerFactory  factory(...$parameters)
 * @method static Builder|Danger newModelQuery()
 * @method static Builder|Danger newQuery()
 * @method static Builder|Danger query()
 * @method static Builder|Danger whereCreatedAt($value)
 * @method static Builder|Danger whereDescription($value)
 * @method static Builder|Danger whereId($value)
 * @method static Builder|Danger whereName($value)
 * @method static Builder|Danger whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Danger extends Model
{
    use HasFactory;
    use HasTimestamps;

    public function scenarios(): BelongsToMany
    {
        return $this->belongsToMany(Scenario::class);
    }
}
