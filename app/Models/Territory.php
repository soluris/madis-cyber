<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Territory.
 *
 * @property int                                     $id
 * @property string                                  $name
 * @property Organization[]|Collection<Organization> $organizations
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property int|null                                $organizations_count
 *
 * @method static Builder|Territory newModelQuery()
 * @method static Builder|Territory newQuery()
 * @method static Builder|Territory query()
 * @method static Builder|Territory whereCreatedAt($value)
 * @method static Builder|Territory whereId($value)
 * @method static Builder|Territory whereName($value)
 * @method static Builder|Territory whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Territory extends Model
{
    use HasFactory;

    public function organizations(): HasMany
    {
        return $this->hasMany(Organization::class);
    }
}
