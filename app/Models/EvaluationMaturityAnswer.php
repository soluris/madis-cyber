<?php

namespace App\Models;

use Database\Factories\EvaluationMaturityAnswerFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class EvaluationMaturityAnswer.
 *
 * @property int              $id
 * @property Evaluation       $evaluation
 * @property int              $evaluation_id
 * @property MaturityQuestion $question
 * @property int              $maturity_question_id
 * @property MaturityAnswer   $answer
 * @property int              $maturity_answer_id
 * @property \DateTime        $createdAt
 * @property \DateTime        $updatedAt
 * @property Carbon|null      $created_at
 * @property Carbon|null      $updated_at
 *
 * @method static EvaluationMaturityAnswerFactory  factory(...$parameters)
 * @method static Builder|EvaluationMaturityAnswer newModelQuery()
 * @method static Builder|EvaluationMaturityAnswer newQuery()
 * @method static Builder|EvaluationMaturityAnswer query()
 * @method static Builder|EvaluationMaturityAnswer whereCreatedAt($value)
 * @method static Builder|EvaluationMaturityAnswer whereEvaluationId($value)
 * @method static Builder|EvaluationMaturityAnswer whereId($value)
 * @method static Builder|EvaluationMaturityAnswer whereMaturityAnswerId($value)
 * @method static Builder|EvaluationMaturityAnswer whereMaturityQuestionId($value)
 * @method static Builder|EvaluationMaturityAnswer whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class EvaluationMaturityAnswer extends Model
{
    use HasFactory;
    use HasTimestamps;

    public function evaluation(): BelongsTo
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function question(): BelongsTo
    {
        return $this->belongsTo(MaturityQuestion::class, 'maturity_question_id');
    }

    public function answer(): BelongsTo
    {
        return $this->belongsTo(MaturityAnswer::class, 'maturity_answer_id');
    }
}
