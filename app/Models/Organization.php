<?php

namespace App\Models;

use Database\Factories\OrganizationFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * App\Models\Organization.
 *
 * @property int                                                         $id
 * @property string                                                      $name
 * @property string                                                      $short_name
 * @property string                                                      $type
 * @property string                                                      $siren
 * @property bool                                                        $active
 * @property string                                                      $website
 * @property string                                                      $info
 * @property Address                                                     $address
 * @property ?Contact                                                    $referent
 * @property int                                                         $referent_id
 * @property ?Contact                                                    $referentCyber
 * @property int                                                         $referent_cyber_id
 * @property ?Contact                                                    $referentElu
 * @property int                                                         $referent_elu_id
 * @property Evaluation[]|Collection<Evaluation>                         $evaluations
 * @property Evaluation[]|Collection<Evaluation>                         $doneEvaluations
 * @property int                                                         $territory_id
 * @property Carbon|null                                                 $created_at
 * @property Carbon|null                                                 $updated_at
 * @property string|null                                                 $bilan1
 * @property string|null                                                 $bilan2
 * @property int|null                                                    $address_id
 * @property int|null                                                    $done_evaluations_count
 * @property int|null                                                    $evaluations_count
 * @property \App\Models\Territory|null                                  $territory
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property int|null                                                    $users_count
 *
 * @method static OrganizationFactory  factory(...$parameters)
 * @method static Builder|Organization newModelQuery()
 * @method static Builder|Organization newQuery()
 * @method static Builder|Organization query()
 * @method static Builder|Organization whereActive($value)
 * @method static Builder|Organization whereAddressId($value)
 * @method static Builder|Organization whereBilan1($value)
 * @method static Builder|Organization whereBilan2($value)
 * @method static Builder|Organization whereCreatedAt($value)
 * @method static Builder|Organization whereId($value)
 * @method static Builder|Organization whereInfo($value)
 * @method static Builder|Organization whereName($value)
 * @method static Builder|Organization whereReferentCyberId($value)
 * @method static Builder|Organization whereReferentEluId($value)
 * @method static Builder|Organization whereReferentId($value)
 * @method static Builder|Organization whereShortName($value)
 * @method static Builder|Organization whereSiren($value)
 * @method static Builder|Organization whereTerritoryId($value)
 * @method static Builder|Organization whereType($value)
 * @method static Builder|Organization whereUpdatedAt($value)
 * @method static Builder|Organization whereWebsite($value)
 *
 * @mixin Eloquent
 */
class Organization extends Model
{
    use HasFactory;
    use HasTimestamps;

    public $casts = [
        'active' => 'boolean',
    ];

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public function evaluations(): HasMany
    {
        return $this->hasMany(Evaluation::class)->orderBy('updated_at', 'desc');
    }

    public function doneEvaluations(): HasMany
    {
        return $this->evaluations()->where('status', Evaluation::STATUS_DONE);
    }

    public function address(): BelongsTo
    {
        return $this->belongsTo(Address::class);
    }

    public function territory(): BelongsTo
    {
        return $this->belongsTo(Territory::class);
    }

    public function referent(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'referent_id');
    }

    public function referentCyber(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'referent_cyber_id');
    }

    public function referentElu(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'referent_elu_id');
    }
}
