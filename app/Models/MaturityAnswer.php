<?php

namespace App\Models;

use Database\Factories\MaturityAnswerFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class MaturityAnswer.
 *
 * @property int                                  $id
 * @property string                               $name
 * @property string                               $slug
 * @property int                                  $level
 * @property MaturityQuestion                     $question
 * @property int                                  $maturity_question_id
 * @property MaturityAnswer[]|iterable|Collection $evaluationMaturityAnswers
 * @property \DateTime                            $createdAt
 * @property \DateTime                            $updatedAt
 * @property Carbon|null                          $created_at
 * @property Carbon|null                          $updated_at
 * @property int|null                             $evaluation_maturity_answers_count
 *
 * @method static MaturityAnswerFactory  factory(...$parameters)
 * @method static Builder|MaturityAnswer newModelQuery()
 * @method static Builder|MaturityAnswer newQuery()
 * @method static Builder|MaturityAnswer query()
 * @method static Builder|MaturityAnswer whereCreatedAt($value)
 * @method static Builder|MaturityAnswer whereId($value)
 * @method static Builder|MaturityAnswer whereLevel($value)
 * @method static Builder|MaturityAnswer whereMaturityQuestionId($value)
 * @method static Builder|MaturityAnswer whereName($value)
 * @method static Builder|MaturityAnswer whereSlug($value)
 * @method static Builder|MaturityAnswer whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class MaturityAnswer extends Model
{
    use HasFactory;
    use HasTimestamps;

    public function question(): BelongsTo
    {
        return $this->belongsTo(MaturityQuestion::class);
    }

    public function evaluationMaturityAnswers(): HasMany
    {
        return $this->hasMany(EvaluationMaturityAnswer::class);
    }
}
