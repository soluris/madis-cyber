<?php

namespace App\Models;

use Database\Factories\MeasureLevelFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class MeasureLevel.
 *
 * @property int            $id
 * @property Evaluation     $evaluation
 * @property int            $evaluation_id
 * @property Measure        $measure
 * @property int            $measure_id
 * @property int|null       $actual_level
 * @property int|null       $expected_level
 * @property \DateTime|null $end_date
 * @property string|null    $manager
 * @property Carbon|null    $created_at
 * @property Carbon|null    $updated_at
 *
 * @method static MeasureLevelFactory  factory(...$parameters)
 * @method static Builder|MeasureLevel newModelQuery()
 * @method static Builder|MeasureLevel newQuery()
 * @method static Builder|MeasureLevel query()
 * @method static Builder|MeasureLevel whereActualLevel($value)
 * @method static Builder|MeasureLevel whereCreatedAt($value)
 * @method static Builder|MeasureLevel whereEndDate($value)
 * @method static Builder|MeasureLevel whereEvaluationId($value)
 * @method static Builder|MeasureLevel whereExpectedLevel($value)
 * @method static Builder|MeasureLevel whereId($value)
 * @method static Builder|MeasureLevel whereManager($value)
 * @method static Builder|MeasureLevel whereMeasureId($value)
 * @method static Builder|MeasureLevel whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class MeasureLevel extends Model
{
    use HasFactory;

    public function evaluation(): BelongsTo
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function measure(): BelongsTo
    {
        return $this->belongsTo(Measure::class);
    }
}
