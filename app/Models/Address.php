<?php

namespace App\Models;

use Database\Factories\AddressFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Address.
 *
 * @property int         $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string      $address
 * @property string|null $moreInfos
 * @property string      $cp
 * @property string      $city
 * @property string      $codeInsee
 *
 * @method static AddressFactory  factory(...$parameters)
 * @method static Builder|Address newModelQuery()
 * @method static Builder|Address newQuery()
 * @method static Builder|Address query()
 * @method static Builder|Address whereAddress($value)
 * @method static Builder|Address whereCity($value)
 * @method static Builder|Address whereCodeInsee($value)
 * @method static Builder|Address whereCp($value)
 * @method static Builder|Address whereCreatedAt($value)
 * @method static Builder|Address whereId($value)
 * @method static Builder|Address whereMoreInfos($value)
 * @method static Builder|Address whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Address extends Model
{
    use HasFactory;

    protected $table = 'address';

    public function __toString(): string
    {
        return implode(' ', [$this->address, $this->cp, $this->city]);
    }
}
