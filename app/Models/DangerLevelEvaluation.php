<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class DangerLevelEvaluation.
 *
 * @property int         $id
 * @property Danger      $danger
 * @property int         $danger_id
 * @property Evaluation  $evaluation
 * @property int         $evaluation_id
 * @property DangerLevel $level
 * @property int         $danger_level_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|DangerLevelEvaluation newModelQuery()
 * @method static Builder|DangerLevelEvaluation newQuery()
 * @method static Builder|DangerLevelEvaluation query()
 * @method static Builder|DangerLevelEvaluation whereCreatedAt($value)
 * @method static Builder|DangerLevelEvaluation whereDangerId($value)
 * @method static Builder|DangerLevelEvaluation whereDangerLevelId($value)
 * @method static Builder|DangerLevelEvaluation whereEvaluationId($value)
 * @method static Builder|DangerLevelEvaluation whereId($value)
 * @method static Builder|DangerLevelEvaluation whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class DangerLevelEvaluation extends Model
{
    use HasFactory;
    use HasTimestamps;
    protected $table = 'danger_level_evaluation';

    protected $with = [
        'danger',
        'level',
    ];

    public function danger(): BelongsTo
    {
        return $this->belongsTo(Danger::class);
    }

    public function evaluation(): BelongsTo
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function level(): BelongsTo
    {
        return $this->belongsTo(DangerLevel::class, 'danger_level_id');
    }
}
