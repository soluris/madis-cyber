<?php

namespace App\Models;

use Database\Factories\ContactFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Contact.
 *
 * @property int         $id
 * @property string      $civility
 * @property string      $firstname
 * @property string      $lastname
 * @property string      $function
 * @property string      $email
 * @property string      $phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static ContactFactory  factory(...$parameters)
 * @method static Builder|Contact newModelQuery()
 * @method static Builder|Contact newQuery()
 * @method static Builder|Contact query()
 * @method static Builder|Contact whereCivility($value)
 * @method static Builder|Contact whereCreatedAt($value)
 * @method static Builder|Contact whereEmail($value)
 * @method static Builder|Contact whereFirstname($value)
 * @method static Builder|Contact whereFunction($value)
 * @method static Builder|Contact whereId($value)
 * @method static Builder|Contact whereLastname($value)
 * @method static Builder|Contact wherePhone($value)
 * @method static Builder|Contact whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Contact extends Model
{
    use HasFactory;

    public function __toString(): string
    {
        return implode(' ', [$this->civility, $this->firstname, $this->lastname]);
    }
}
