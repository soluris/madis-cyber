<?php

namespace App\Models;

use Database\Factories\MaturityQuestionFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class MaturityQuestion.
 *
 * @property int              $id
 * @property MaturityAnswer[] $answers
 * @property string           $name
 * @property string           $slug
 * @property string           $title
 * @property string           $subtitle
 * @property bool             $auto
 * @property \DateTime        $createdAt
 * @property \DateTime        $updatedAt
 * @property Carbon|null      $created_at
 * @property Carbon|null      $updated_at
 * @property int|null         $answers_count
 *
 * @method static MaturityQuestionFactory  factory(...$parameters)
 * @method static Builder|MaturityQuestion newModelQuery()
 * @method static Builder|MaturityQuestion newQuery()
 * @method static Builder|MaturityQuestion query()
 * @method static Builder|MaturityQuestion whereAuto($value)
 * @method static Builder|MaturityQuestion whereCreatedAt($value)
 * @method static Builder|MaturityQuestion whereId($value)
 * @method static Builder|MaturityQuestion whereName($value)
 * @method static Builder|MaturityQuestion whereSlug($value)
 * @method static Builder|MaturityQuestion whereSubtitle($value)
 * @method static Builder|MaturityQuestion whereTitle($value)
 * @method static Builder|MaturityQuestion whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class MaturityQuestion extends Model
{
    use HasFactory;
    use HasTimestamps;

    protected $casts = [
        'auto' => 'bool',
    ];

    public function answers(): HasMany
    {
        return $this->hasMany(MaturityAnswer::class);
    }
}
