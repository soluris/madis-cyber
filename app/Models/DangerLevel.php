<?php

namespace App\Models;

use Database\Factories\DangerLevelFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class DangerLevel.
 *
 * @property int         $id
 * @property string      $name
 * @property string      $color
 * @property Danger      $danger
 * @property int         $danger_id
 * @property string      $description
 * @property int         $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|void    $points
 *
 * @method static DangerLevelFactory  factory(...$parameters)
 * @method static Builder|DangerLevel newModelQuery()
 * @method static Builder|DangerLevel newQuery()
 * @method static Builder|DangerLevel query()
 * @method static Builder|DangerLevel whereColor($value)
 * @method static Builder|DangerLevel whereCreatedAt($value)
 * @method static Builder|DangerLevel whereDescription($value)
 * @method static Builder|DangerLevel whereId($value)
 * @method static Builder|DangerLevel whereName($value)
 * @method static Builder|DangerLevel whereUpdatedAt($value)
 * @method static Builder|DangerLevel whereValue($value)
 *
 * @mixin Eloquent
 */
class DangerLevel extends Model
{
    use HasFactory;
    use HasTimestamps;

    /**
     * Convert from value to points for calculations.
     *
     * @return int|void
     */
    public function getPointsAttribute()
    {
        return match ($this->value) {
            25 => 1,
            50 => 9,
            75 => 30,
            100 => 100,
            default => 0,
        };
    }
}
