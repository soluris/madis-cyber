<?php

namespace App\Models;

use Database\Factories\MeasureFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * App\Models\Measure.
 *
 * @property int                   $id
 * @property string                $name
 * @property string                $short_name
 * @property string                $level1_actual_label
 * @property string                $level1_preconized_label
 * @property string                $level1_file
 * @property string                $level1_description
 * @property int                   $level1_difficulty
 * @property int                   $level1_cost
 * @property int                   $level1_duration
 * @property int                   $level1_assistance
 * @property string                $level1_info
 * @property string                $level2_actual_label
 * @property string                $level2_preconized_label
 * @property string                $level2_file
 * @property string                $level2_description
 * @property int                   $level2_difficulty
 * @property int                   $level2_cost
 * @property int                   $level2_duration
 * @property int                   $level2_assistance
 * @property string                $level2_info
 * @property string                $level3_actual_label
 * @property string                $level3_preconized_label
 * @property string                $level3_description
 * @property string                $level3_info
 * @property int                   $level3_difficulty
 * @property int                   $level3_cost
 * @property int                   $level3_duration
 * @property int                   $level3_assistance
 * @property string                $level3_file
 * @property \DateTime             $created_at
 * @property \DateTime             $updated_at
 * @property bool                  $fundamental
 * @property int                   $level0_value
 * @property int                   $level1_value
 * @property int                   $level2_value
 * @property int                   $level3_value
 * @property Scenario[]|Collection $scenarios
 * @property string                $level0_actual_label
 * @property int|null              $scenarios_count
 *
 * @method static MeasureFactory  factory(...$parameters)
 * @method static Builder|Measure newModelQuery()
 * @method static Builder|Measure newQuery()
 * @method static Builder|Measure query()
 * @method static Builder|Measure whereCreatedAt($value)
 * @method static Builder|Measure whereFundamental($value)
 * @method static Builder|Measure whereId($value)
 * @method static Builder|Measure whereLevel0ActualLabel($value)
 * @method static Builder|Measure whereLevel0Value($value)
 * @method static Builder|Measure whereLevel1ActualLabel($value)
 * @method static Builder|Measure whereLevel1Assistance($value)
 * @method static Builder|Measure whereLevel1Cost($value)
 * @method static Builder|Measure whereLevel1Description($value)
 * @method static Builder|Measure whereLevel1Difficulty($value)
 * @method static Builder|Measure whereLevel1Duration($value)
 * @method static Builder|Measure whereLevel1File($value)
 * @method static Builder|Measure whereLevel1Info($value)
 * @method static Builder|Measure whereLevel1PreconizedLabel($value)
 * @method static Builder|Measure whereLevel1Value($value)
 * @method static Builder|Measure whereLevel2ActualLabel($value)
 * @method static Builder|Measure whereLevel2Assistance($value)
 * @method static Builder|Measure whereLevel2Cost($value)
 * @method static Builder|Measure whereLevel2Description($value)
 * @method static Builder|Measure whereLevel2Difficulty($value)
 * @method static Builder|Measure whereLevel2Duration($value)
 * @method static Builder|Measure whereLevel2File($value)
 * @method static Builder|Measure whereLevel2Info($value)
 * @method static Builder|Measure whereLevel2PreconizedLabel($value)
 * @method static Builder|Measure whereLevel2Value($value)
 * @method static Builder|Measure whereLevel3ActualLabel($value)
 * @method static Builder|Measure whereLevel3Assistance($value)
 * @method static Builder|Measure whereLevel3Cost($value)
 * @method static Builder|Measure whereLevel3Description($value)
 * @method static Builder|Measure whereLevel3Difficulty($value)
 * @method static Builder|Measure whereLevel3Duration($value)
 * @method static Builder|Measure whereLevel3File($value)
 * @method static Builder|Measure whereLevel3Info($value)
 * @method static Builder|Measure whereLevel3PreconizedLabel($value)
 * @method static Builder|Measure whereLevel3Value($value)
 * @method static Builder|Measure whereName($value)
 * @method static Builder|Measure whereShortName($value)
 * @method static Builder|Measure whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Measure extends Model
{
    use HasFactory;
    use HasTimestamps;

    protected $casts = [
        'level1_difficulty' => 'int',
        'level1_cost' => 'string',
        'level1_duration' => 'string',
        'level1_assistance' => 'int',
        'level2_difficulty' => 'int',
        'level2_cost' => 'string',
        'level2_duration' => 'string',
        'level2_assistance' => 'int',
        'level3_difficulty' => 'int',
        'level3_cost' => 'string',
        'level3_duration' => 'string',
        'level3_assistance' => 'int',
        'fundamental' => 'bool',
    ];

    public function scenarios(): BelongsToMany
    {
        return $this->belongsToMany(Scenario::class);
    }

    public function withConvertedLevels(): array
    {
        $m = $this->toArray();

        $m['levels'] = [];

        for ($i = 0; $i < 4; ++$i) {
            $niveau = $i;
            // Level 0 only has "actual_label"
            if (0 === $i) {
                $m['levels'][$i] = [
                    'level' => $niveau,
                    'actual_label' => $this->{'level' . $niveau . '_actual_label'},
                    'value' => $this->{'level' . $niveau . '_value'},
                ];
            } else {
                $m['levels'][$i] = [
                    'level' => $niveau,
                    'actual_label' => $this->{'level' . $niveau . '_actual_label'},
                    'preconized_label' => $this->{'level' . $niveau . '_preconized_label'},
                    'description' => $this->{'level' . $niveau . '_description'},
                    'difficulty' => $this->{'level' . $niveau . '_difficulty'},
                    'cost' => $this->{'level' . $niveau . '_cost'},
                    'duration' => $this->{'level' . $niveau . '_duration'},
                    'assistance' => $this->{'level' . $niveau . '_assistance'},
                    'info' => $this->{'level' . $niveau . '_info'},
                    'file' => $this->{'level' . $niveau . '_file'},
                    'value' => $this->{'level' . $niveau . '_value'},
                ];
            }
        }

        return $m;
    }
}
