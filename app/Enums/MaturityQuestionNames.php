<?php

namespace App\Enums;

enum MaturityQuestionNames: string
{
    case KNOWLEDGE = 'Connaissance';
    case ORGANIZATION = 'Organisation';
    case MOTIVATION = 'Motivation';
    case EXECUTION = 'Exécution';
    case FUNDAMENTALS = 'Fondamentaux';
}
