<?php

namespace App\Calculator;

use App\Models\Evaluation;
use App\Models\MeasureLevel;

class MaturityAnswerLevelCalculator
{
    public static function calculateFundamentalLevel(Evaluation $evaluation): int
    {
        $fundamentalMeasures = $evaluation->measureLevels->filter(function (MeasureLevel $ml) {
            return true === $ml->measure->fundamental;
        });

        $level = $fundamentalMeasures->average(function (MeasureLevel $ml) {
            return $ml->actual_level;
        });

        return (int) floor($level);
    }

    public static function calculateExecutionLevel(Evaluation $evaluation, Evaluation|null $previousEvaluation): int
    {
        if ($previousEvaluation) {
            // Count the number of measures that had an expected_level
            // Check how many of them are now at this actual_level
            $previousLevels = $previousEvaluation->measureLevels;
            $expectedLevelMeasures = $previousLevels->filter(function (MeasureLevel $ml) {
                return null !== $ml->expected_level;
            });
            // Check how many of them are now at this actual_level
            $attainedLevelMeasures = $evaluation->measureLevels->filter(function (MeasureLevel $ml, $i) use ($previousLevels) {
                $previousLevel = $previousLevels->first(function (MeasureLevel $pml) use ($ml) {
                    return $ml->measure_id === $pml->measure_id;
                });

                return $ml->actual_level === $previousLevel->expected_level;
            });

            // Calculate score
            $atc = $attainedLevelMeasures->count();
            $etc = $expectedLevelMeasures->count();

            if (0 === $etc) {
                return 0;
            }

            $score = $atc / $etc * 100;

            // Return level from score
            if ($score > 80) {
                return 3;
            }
            if ($score > 20) {
                return 2;
            }
            if ($score > 0) {
                return 1;
            }
        }

        return 0;
    }
}
