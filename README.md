# Madis Cyber

## Installation sur serveur linux

Installe PHP 8, nginx, git, ghostscript et mysql comme recommandé pour votre variante de linux

Installer wkhtmltopdf en suivant cette procédure : 

- Récupérer la version 0.12.6-1 depuis [https://github.com/wkhtmltopdf/packaging/releases/tag/0.12.6-1]() pour votre système d'exploitation
- Installer avec `sudo apt install ./wkhtmltox_0.12.6-1.<OS_VERSION>.deb` (Remplacez `<OS_VERSION>` par la version de votre système d'exploitation utilisé à l'étape 1 )

Installer les extensions PHP suivantes (à l'aide d'une commande du type `apt-get install php8.1-zip`) :

 - BCMath
 - Ctype
 - cURL
 - DOM
 - Fileinfo
 - JSON
 - Mbstring
 - OpenSSL
 - PCRE
 - PDO
 - Tokenizer
 - XML
 - PDOMySQL
 - Zip
 - Intl

Configurez nginx comme indiqué dans la [documentation laravel](https://laravel.com/docs/9.x/deployment)

Créez une base de données mysql pour le projet et associez-y un utilisateur. Conservez ces informations car nous en aurons besoin plus tard.

Installez [composer](https://getcomposer.org/download/)
Installez [npm et nodejs](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

Placez-vous dans le dossier `/var/www` puis récupérez le code depuis le dépôt gitlab avec la commande suivante : 

`git clone https://gitlab.adullact.net/soluris/madis-cyber.git`

Cela créera un dossier `madis-cyber` avec le contenu du projet.

Entrez dans le dossier (`cd madis-cyber`) puis effectuez les opérations suivantes :

Copiez le fichier `.env.example` vers `.env` puis modifiez-y les variables suivantes:
- `APP_URL` : l'URL de l'application (ex: https://madis-cyber.com)
- `DB_DATABASE` : nom de la base de données
- `DB_USERNAME` : utilisateur de la base de données
- `DB_PASSWORD` : mot de passe de la base de données
- Configurez l'envoi d'emails via la section `MAIL_...`
- `SANCTUM_STATEFUL_DOMAINS` : ajoutez une virgule à la fin de la ligne, puis le nom de domaine. Ex : `SANCTUM_STATEFUL_DOMAINS=localhost,127.0.0.1,127.0.0.1:8000,::1,madis-cyber.com`
- `ADMIN_EMAIL` : cela sera l'adresse email de l'utilisateur administrateur que nous allons créer par la suite
- `ADMIN_PASSWORD` : cela sera le mot de passe de l'utilisateur administrateur que nous allons créer par la suite
- `ADMIN_FIRSTNAME` : cela sera le prénom de l'utilisateur administrateur que nous allons créer par la suite
- `ADMIN_LASTNAME` : cela sera le nom de l'utilisateur administrateur que nous allons créer par la suite
- `APP_FAVICON_PATH` : (_optionnel_) le chemin vers l'image à utiliser comme icone d'onglet (image à placer dans le dossier public/images)
- `REFERENTIEL_VERSION` : La version du référentiel à utiliser pour les évaluations
- `SEUIL_ALERTE` : permet la comparaison du niveau des measures fondamentales avec le seuil d'alerte. Sa valeur peut aller de -1 (ne pas prendre en compte) à 3.

- `MIX_FOOTER_NAME`: nom du pied de page en bas à gauche
- `MIX_FOOTER_LINK`: lien du nom de pied de page en bas à gauche
- `MIX_FOOTER_YEAR`: année du copyright en bas à gauche

Installez les dépendances php avec composer en lançant la commande suivante :
`composer install --optimize-autoloader --no-dev`

Créez une clé d'appication avec la commande suivante :
`php artisan key:generate`

Ensuite, lancez les migrations avec `php artisan migrate`

Lancez la commande `php artisan storage:link` pour faire en sorte que les documents envoyés soient téléchargeables

Nous pouvons installer les données et créer un premier utilisateur en lançant la commande `php artisan db:seed --class=InstanceSeeder`

Installer nodejs en version >= 14.

npm doit-être en version >= 6.

Installez les dépendances javascript en lançant la commande `npm install && npm run prod`

Vous devriez maintenant pouvoir vous connecter au site que vous avez configuré dans `APP_URL` et vous connecter avec les identifiants administrateur définis précédemment.

Pour les mises à jour il suffit de lancer le script `deploy` dans le répertoire racine avec la commande `./deploy`. Cela mettra à jour a version de l'application dans le pied de page automatiquement avec le dernier tag git.
