<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(MeasureSeeder::class);
        $this->call(DangerLevelSeeder::class);
        $this->call(DangerSeeder::class);
        $this->call(ScenarioSeeder::class);
        $this->call(MaturityQuestionSeeder::class);
        $this->call(MaturityAnswerSeeder::class);
        $this->call(TerritorySeeder::class);
        $this->call(OrganizationSeeder::class);
    }
}
