<?php

namespace Database\Seeders;

use App\Models\Danger;
use App\Models\DangerLevel;
use App\Models\DangerLevelEvaluation;
use App\Models\Evaluation;
use App\Models\EvaluationMaturityAnswer;
use App\Models\MaturityAnswer;
use App\Models\MaturityQuestion;
use App\Models\Measure;
use App\Models\MeasureLevel;
use App\Models\Organization;
use App\Models\User;
use App\Repository\EvaluationRepository;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{
    protected EvaluationRepository $repository;

    public function __construct(EvaluationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::factory(5)->create()->each(function (Organization $org) {
            $org->users()->saveMany(User::factory(5)->make());
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_MANAGER]));
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_USER]));
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_USER, 'active' => false]));
            $org->evaluations()->saveMany(Evaluation::factory(5)->make());
            $org->evaluations()->saveMany(Evaluation::factory(2)->make(['current_step' => 4]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make(['current_step' => 5]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make([
                'current_step' => Evaluation::STEP_MATURITY,
                'reference' => env('REFERENTIEL_VERSION'),
            ]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make([
                'current_step' => Evaluation::STEP_RESULTS,
                'status' => Evaluation::STATUS_DONE,
                'reference' => env('REFERENTIEL_VERSION'),
            ]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make([
                'current_step' => Evaluation::STEP_MEASURES,
                'reference' => env('REFERENTIEL_VERSION'),
            ]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make(['current_step' => 1]));
            $org->evaluations()->saveMany(Evaluation::factory(2)->make(['current_step' => 2]));

            $this->setEvaluationsData($org);
        });

        // Create inactive org with active user
        Organization::factory(1)->create(['active' => false])->each(function (Organization $org) {
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_MANAGER]));
            $org->users()->saveMany(User::factory(5)->make(['active' => true, 'role' => User::ROLE_USER]));
        });
        // Create active org with inactive user
        Organization::factory(1)->create(['active' => true])->each(function (Organization $org) {
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_MANAGER]));
            $org->users()->saveMany(User::factory(5)->make(['active' => false, 'role' => User::ROLE_USER]));
        });

        // Create active org single completed evaluation
        Organization::factory(1)->create(['active' => true])->each(function (Organization $org) {
            $org->users()->saveMany(User::factory(1)->make(['role' => User::ROLE_MANAGER]));
            $org->users()->saveMany(User::factory(5)->make(['active' => false, 'role' => User::ROLE_USER]));
            $org->evaluations()->saveMany(Evaluation::factory(1)->make([
                'current_step' => Evaluation::STEP_RESULTS,
                'status' => Evaluation::STATUS_DONE,
                'reference' => env('REFERENTIEL_VERSION'),
            ]));

            $this->setEvaluationsData($org);
        });
    }

    private function setEvaluationsData(Organization $org)
    {
        foreach ($org->evaluations as $eval) {
            if ($eval->current_step < Evaluation::STEP_RESULTS) {
                $eval->status = Evaluation::STATUS_ONGOING;
            } else {
                $eval->status = Evaluation::STATUS_DONE;
            }
            $eval->save();
            if ($eval->current_step >= Evaluation::STEP_DANGERS) {
                $dangers = Danger::all();

                $dangerLevels = $dangers->map(function (Danger $danger) {
                    $level = DangerLevel::inRandomOrder()->first();
                    $dangerLevel = new DangerLevelEvaluation();
                    $dangerLevel->danger_level_id = $level->id;
                    $dangerLevel->danger_id = $danger->id;

                    return $dangerLevel;
                });

                $eval->dangerLevels()->saveMany($dangerLevels);
            }

            if ($eval->current_step >= Evaluation::STEP_MEASURES) {
                $measures = Measure::all();
                $evalMeasures = $measures->map(function (Measure $m) {
                    return MeasureLevel::factory()->make([
                        'measure_id' => $m->id,
                    ]);
                });
                $eval->measureLevels()->saveMany($evalMeasures);
            }

            if ($eval->current_step >= Evaluation::STEP_MATURITY) {
                $questions = MaturityQuestion::where('auto', false)->get();
                $answers = $questions->map(function (MaturityQuestion $m) {
                    $a = new EvaluationMaturityAnswer();
                    $a->maturity_question_id = $m->id;
                    $a->maturity_answer_id = MaturityAnswer::where('maturity_question_id', $m->id)
                        ->inRandomOrder()
                        ->first()->id;

                    return $a;
                });
                $eval->maturityLevels()->saveMany($answers);

                $eval->saveAutomaticAnswers();
            }
        }
    }
}
