<?php

namespace Database\Seeders;

use App\Models\MaturityQuestion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MaturityQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $q1 = MaturityQuestion::firstOrCreate([
            'id' => 1,
            'name' => 'Le référent dispose-t-il des connaissances pour mettre en œuvre des mesures de sécurité cyber ?',
            'slug' => Str::slug('Le référent dispose-t-il des connaissances pour mettre en œuvre des mesures de sécurité cyber ?'),
        ]);

        $q1->update([
            'title' => 'Connaissance',
            'subtitle' => '(sait/connaît)',
        ]);

        $q2 = MaturityQuestion::firstOrCreate([
            'id' => 2,
            'name' => "Le référent peut-il s'appuyer sur l'organisation de sa structure pour mettre en œuvre des mesures de sécurité cyber ?",
            'slug' => Str::slug("Le référent peut-il s'appuyer sur l'organisation de sa structure pour mettre en œuvre des mesures de sécurité cyber ?"),
        ]);
        $q2->update([
            'title' => 'Organisation',
            'subtitle' => '(peut faire)',
        ]);

        $q3 = MaturityQuestion::firstOrCreate([
            'id' => 3,
            'name' => 'Le référent est-il convaincu par la méthode CMT ?',
            'slug' => Str::slug('Le référent est-il convaincu par la méthode CMT ?'),
        ]);
        $q3->update([
            'title' => 'Motivation',
            'subtitle' => '(veut faire)',
        ]);

        $q4 = MaturityQuestion::firstOrCreate([
            'id' => 4,
            'name' => 'Le plan de traitement a-t-il été mis en œuvre conformément aux engagements de l\'année précédente ?',
            'slug' => Str::slug('Le plan de traitement a-t-il été mis en œuvre conformément aux engagements de l\'année précédente ?'),
        ]);
        $q4->update([
            'title' => 'Exécution',
            'subtitle' => '',
            'auto' => true,
        ]);

        $q5 = MaturityQuestion::firstOrCreate([
            'id' => 5,
            'name' => 'Quelle est votre maturité sur les fondamentaux de la sécurité ?',
            'slug' => Str::slug('Quelle est votre maturité sur les fondamentaux de la sécurité ?'),
        ]);

        $q5->update([
            'title' => 'Fondamentaux',
            'subtitle' => '',
            'auto' => true,
        ]);
    }
}
