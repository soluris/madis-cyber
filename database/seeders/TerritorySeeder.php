<?php

namespace Database\Seeders;

use App\Models\Territory;
use Illuminate\Database\Seeder;

class TerritorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Territory::create(['name' => 'Territoire 1']);
        Territory::create(['name' => 'Territoire 2']);
        Territory::create(['name' => 'Territoire 3']);
        Territory::create(['name' => 'Territoire 4']);
        Territory::create(['name' => 'Territoire 5']);
    }
}
