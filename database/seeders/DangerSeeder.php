<?php

namespace Database\Seeders;

use App\Models\Danger;
use Illuminate\Database\Seeder;

class DangerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Danger::firstOrCreate([
            'name' => 'Sabotage, demande de rançon',
            'description' => "La collectivité est victime d'un chiffrement ou de la captation de ses données par un cybercriminel qui demande le paiement d'une rançon.",
        ]);

        Danger::firstOrCreate([
            'name' => 'Vol, recel de données, publication de données des citoyens',
            'description' => 'Les données de la collectivité ont été exfiltrées et sont revendues par les cybercriminels ou publiées sur internet.',
        ]);

        Danger::firstOrCreate([
            'name' => 'Détournement de fonds ',
            'description' => "La collectivité est victime d'une escroquerie qui lui fait payer des services usurpés ou de l'usurpation d'identité d'un partenaire afin de remplacer le RIB pour le paiement.",
        ]);

        Danger::firstOrCreate([
            'name' => 'Détournement de ressources informatiques',
            'description' => 'La collectivité héberge des services illicites à son insu pouvant entrainer des surfacturations, des ralentissements ou des poursuites judiciaires.',
        ]);

        Danger::firstOrCreate([
            'name' => 'Défiguration du site internet',
            'description' => "La collectivité est victime d'une défiguration (défacement) de son site internet pour promouvoir l'apologie d'une idéologie",
        ]);

        Danger::firstOrCreate([
            'name' => 'Piratage de compte de messagerie',
            'description' => 'La messagerie de la collectivité est la cible de mails malveillants (spam, phishing) ou elle est accessible aux cybercriminels qui en espionnent le contenu.',
        ]);
    }
}
