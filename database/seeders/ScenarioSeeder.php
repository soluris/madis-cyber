<?php

namespace Database\Seeders;

use App\Models\Danger;
use App\Models\Measure;
use App\Models\Scenario;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScenarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Scenario::firstOrCreate([
            'name' => 'Hameçonnage',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Installation non maitrisée',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Exploitation de vulnérabilités, défaut de mise à jour',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Accès distant non sécurisée',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Mot de passe faible',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Ingénerie sociale',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Défaut de paramétrage site web, outils de sécurité ',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Usage d\'outils pro à titre privé',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Réseau WIFI, VOIP non sécurisée',
        ]);
        Scenario::firstOrCreate([
            'name' => 'Intrusion via un prestataire ou éditeurs',
        ]);

        $scenarios = Scenario::all();

        // DANGERS
        $danger_scenario_matrix = [
            1, 1, 1, 1, 0, 1,   // S
            1, 0, 0, 1, 1, 0,   // C
            1, 1, 0, 1, 1, 0,   // E
            1, 1, 0, 1, 0, 0,   // N
            1, 0, 0, 1, 1, 1,   // A
            0, 0, 1, 0, 0, 1,   // R
            1, 1, 1, 1, 1, 1,   // I
            1, 0, 0, 1, 0, 1,   // O
            0, 1, 0, 0, 0, 1,   // S
            1, 1, 0, 1, 1, 0,
        ];

        // MEASURES
        $measure_scenario_matrix = [
            // MEASURES
            0, 1, 1, 1, 1, 0, 0, 1, 0, 0,  // S
            1, 1, 1, 1, 1, 0, 1, 1, 1, 1,  // C
            0, 1, 1, 1, 0, 0, 1, 1, 1, 0,  // E
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,  // N
            1, 1, 0, 1, 1, 0, 0, 0, 1, 0,  // A
            0, 1, 0, 1, 1, 0, 0, 0, 1, 0,  // R
            1, 1, 1, 1, 0, 1, 0, 1, 1, 1,  // I
            1, 1, 1, 1, 1, 0, 0, 1, 0, 0,  // O
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1,  // S
            1, 1, 0, 0, 1, 1, 1, 1, 1, 1,
        ];

        $measureOrder = [
            'Mots de passe',
            'Sauvegardes',
            'MAJ',
            'Sensibilisation',
            "Droits d'accès logiques",
            'Cloisonnement réseaux',
            'Modes dégradés',
            'AV et outils complémentaires',
            'Contrats',
            'Plan informatique',
        ];

        $measures = Measure::all()->sortBy(function (Measure $m) use ($measureOrder) {
            return array_search($m->short_name, $measureOrder);
        })->values();

        foreach ($scenarios as $k => $scenario) {
            // Insert danger_scenario
            $dangers = Danger::all();
            foreach ($dangers as $l => $danger) {
                if (1 === $danger_scenario_matrix[$k * 6 + $l]) {
                    DB::table('danger_scenario')->updateOrInsert([
                        'scenario_id' => $scenario->id,
                        'danger_id' => $danger->id,
                    ]);
                }
            }
            // Insert measure_scenario
            foreach ($measures as $l => $measure) {
                if (1 === $measure_scenario_matrix[$k * 10 + $l]) {
                    DB::table('measure_scenario')->updateOrInsert([
                        'scenario_id' => $scenario->id,
                        'measure_id' => $measure->id,
                    ]);
                }
            }
        }
    }
}
