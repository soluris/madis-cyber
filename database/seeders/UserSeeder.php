<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'email' => env('ADMIN_EMAIL', 'admin@datakode.fr'),
            'firstname' => env('ADMIN_FIRSTNAME', 'Admin'),
            'lastname' => env('ADMIN_LASTNAME', 'Admin'),
            'role' => User::ROLE_ADMIN,
            'active' => true,
        ], [
            'password' => Hash::make(env('ADMIN_PASSWORD', 'secret')),
        ]);
    }
}
