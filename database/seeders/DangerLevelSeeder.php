<?php

namespace Database\Seeders;

use App\Models\DangerLevel;
use Illuminate\Database\Seeder;

class DangerLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DangerLevel::factory()->create([
            'name' => 'Négligeable',
            'value' => 25,
            'color' => '#468355',
            'description' => 'La structure sera faiblement impactée, la résolution prendra quelques jours, aucune séquelle ne sera perceptible.',
        ]);
        DangerLevel::factory()->create([
            'name' => 'Limité',
            'value' => 50,
            'color' => '#FFC107',
            'description' => 'La structure sera faiblement impactée, la résolution prendra plusieurs semaines, aucune séquelle ne sera perceptible.',
        ]);
        DangerLevel::factory()->create([
            'name' => 'Important',
            'value' => 75,
            'color' => '#D63F49',
            'description' => 'La structure sera fortement impactée, la résolution prendra plusieurs semaines, les séquelles seront perceptibles sur plusieurs mois.',
        ]);
        DangerLevel::factory()->create([
            'name' => 'Critique',
            'value' => 100,
            'color' => '#454545',
            'description' => 'La structure sera fortement impactée, la résolution prendra plusieurs mois, les séquelles seront perceptibles sur plusieurs années.',
        ]);
    }
}
