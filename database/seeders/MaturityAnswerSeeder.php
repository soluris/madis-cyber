<?php

namespace Database\Seeders;

use App\Models\MaturityAnswer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class MaturityAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent ne comprend pas ou ne se sent pas concerné par les notions de dangers cyber.',
            'slug' => Str::slug('Le référent ne comprend pas ou ne se sent pas concerné par les notions de dangers cyber.'),
            'maturity_question_id' => 1,
            'level' => 0,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent comprend les dangers auxquels est exposée sa collectivité.',
            'slug' => Str::slug('Le référent comprend les dangers auxquels est exposée sa collectivité.'),
            'maturity_question_id' => 1,
            'level' => 1,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent comprend les dangers et a une bonne connaisance des mesures mises en œuvre dans sa collectivité.',
            'slug' => Str::slug('Le référent comprend les dangers et a une bonne connaisance des mesures mises en œuvre dans sa collectivité.'),
            'maturity_question_id' => 1,
            'level' => 2,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent comprend les dangers, connait les mesures mises en œuvre et a une bonne connaissance des projets de de la collectivité.',
            'slug' => Str::slug('Le référent comprend les dangers, connait les mesures mises en œuvre et a une bonne connaissance des projets de de la collectivité.'),
            'maturity_question_id' => 1,
            'level' => 3,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => "Le référent n'est pas légitime à mettre en œuvre le plan de traitement cyber.",
            'slug' => Str::slug("Le référent n'est pas légitime à mettre en œuvre le plan de traitement cyber."),
            'maturity_question_id' => 2,
            'level' => 0,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent dispose de la légitimité pour mettre en œuvre le plan de traitement cyber.',
            'slug' => Str::slug('Le référent dispose de la légitimité pour mettre en œuvre le plan de traitement cyber.'),
            'maturity_question_id' => 2,
            'level' => 1,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent dispose de la légitimité et du budget nécessaire pour mettre en œuvre le plan de traitement cyber.',
            'slug' => Str::slug('Le référent dispose de la légitimité et du budget nécessaire pour mettre en œuvre le plan de traitement cyber.'),
            'maturity_question_id' => 2,
            'level' => 2,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent cyber dispose de la légitimité, du budget et du temps nécessaire pour suivre la mise en œuvre du plan de traitement cyber.',
            'slug' => Str::slug('Le référent cyber dispose de la légitimité, du budget et du temps nécessaire pour suivre la mise en œuvre du plan de traitement cyber.'),
            'maturity_question_id' => 2,
            'level' => 3,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent ne voit pas quel bénéfice il peut retirer de la démarche CMT.',
            'slug' => Str::slug('Le référent ne voit pas quel bénéfice il peut retirer de la démarche CMT.'),
            'maturity_question_id' => 3,
            'level' => 0,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent comprend les bénéfices mais ne souhaite pas traiter le sujet pour le moment / la démarche ne fait pas partie des sujets prioritaires.',
            'slug' => Str::slug('Le référent comprend les bénéfices mais ne souhaite pas traiter le sujet pour le moment / la démarche ne fait pas partie des sujets prioritaires.'),
            'maturity_question_id' => 3,
            'level' => 1,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Le référent comprend les bénéfices mais ne souhaite pas suivre la démarche, il préfère la déléguer à un tiers.',
            'slug' => Str::slug('Le référent comprend les bénéfices mais ne souhaite pas suivre la démarche, il préfère la déléguer à un tiers.'),
            'maturity_question_id' => 3,
            'level' => 2,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => "Le référent perçoit les bénéfices qu'il peut retirer de la démarche CMT pour lui, les équipes de sa collectivité et les citoyens et va s'impliquer dans la mise en œuvre.",
            'slug' => Str::slug("Le référent perçoit les bénéfices qu'il peut retirer de la démarche CMT pour lui, les équipes de sa collectivité et les citoyens et va s'impliquer dans la mise en œuvre."),
            'maturity_question_id' => 3,
            'level' => 3,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Exécution niveau 0',
            'slug' => Str::slug('Exécution niveau 0'),
            'maturity_question_id' => 4,
            'level' => 0,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Exécution niveau 1',
            'slug' => Str::slug('Exécution niveau 1'),
            'maturity_question_id' => 4,
            'level' => 1,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Exécution niveau 2',
            'slug' => Str::slug('Exécution niveau 2'),
            'maturity_question_id' => 4,
            'level' => 2,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Exécution niveau 3',
            'slug' => Str::slug('Exécution niveau 3'),
            'maturity_question_id' => 4,
            'level' => 3,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Fondamentaux niveau 0',
            'slug' => Str::slug('Fondamentaux niveau 0'),
            'maturity_question_id' => 5,
            'level' => 0,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Fondamentaux niveau 1',
            'slug' => Str::slug('Fondamentaux niveau 1'),
            'maturity_question_id' => 5,
            'level' => 1,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Fondamentaux niveau 2',
            'slug' => Str::slug('Fondamentaux niveau 2'),
            'maturity_question_id' => 5,
            'level' => 2,
        ]);

        MaturityAnswer::firstOrCreate([
            'name' => 'Fondamentaux niveau 3',
            'slug' => Str::slug('Fondamentaux niveau 3'),
            'maturity_question_id' => 5,
            'level' => 3,
        ]);
    }
}
