<?php

namespace Database\Factories;

use App\Models\MaturityQuestion;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MaturityAnswer>
 */
class MaturityAnswerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $n = $this->faker->text;

        return [
            'name' => $n,
            'slug' => $this->faker->randomElement([Str::slug($n), null, '']),
            'level' => $this->faker->numberBetween(0, 3),
            'maturity_question_id' => MaturityQuestion::inRandomOrder()->first()->id,
        ];
    }
}
