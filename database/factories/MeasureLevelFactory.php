<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MeasureLevel>
 */
class MeasureLevelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $level = $this->faker->numberBetween(0, 3);
        $expected_level = min(3, $level + $this->faker->numberBetween(0, 2));

        if (3 === $level) {
            $expected_level = null;
        }

        return [
            'expected_level' => $expected_level,
            'actual_level' => $level,
            'end_date' => Carbon::now()->addDays($this->faker->numberBetween(0, 365)),
        ];
    }
}
