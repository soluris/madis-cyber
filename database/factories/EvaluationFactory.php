<?php

namespace Database\Factories;

use App\Models\Evaluation;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Evaluation>
 */
class EvaluationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $status = $this->faker->numberBetween(Evaluation::STATUS_ONGOING, Evaluation::STATUS_DONE);
        if (Evaluation::STATUS_DONE !== $status) {
            $current_step = $this->faker->numberBetween(Evaluation::STEP_DANGERS, Evaluation::STEP_MATURITY);
        } else {
            $current_step = Evaluation::STEP_RESULTS;
        }

        return [
            'status' => $status,
            'current_step' => $current_step,
            'updated_by' => $this->faker->name(),
            'author' => $this->faker->name(),
            'reference' => $this->faker->boolean(80) ? env('REFERENTIEL_VERSION') : '0.2',
        ];
    }
}
