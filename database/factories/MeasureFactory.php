<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Measure>
 */
class MeasureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $n = $this->faker->text(50);

        return [
            'name' => $n,
            'short_name' => Str::slug($n),
            'level0_actual_label' => $this->faker->randomElement(['Niveau 0', 'Niveau zero']),
            'level1_actual_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level1_preconized_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level1_description' => $this->faker->randomElement([$this->faker->text]),
            'level1_difficulty' => $this->faker->numberBetween(1, 3),
            'level1_cost' => $this->faker->text(5),
            'level1_duration' => $this->faker->text(5),
            'level1_assistance' => $this->faker->numberBetween(1, 3),
            'level1_info' => $this->faker->randomElement([$this->faker->text, null]),
            'level2_actual_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level2_preconized_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level2_description' => $this->faker->randomElement([$this->faker->text]),
            'level2_difficulty' => $this->faker->numberBetween(1, 3),
            'level2_cost' => $this->faker->text(5),
            'level2_duration' => $this->faker->text(5),
            'level2_assistance' => $this->faker->numberBetween(1, 3),
            'level2_info' => $this->faker->randomElement([$this->faker->text, null]),
            'level3_actual_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level3_preconized_label' => $this->faker->randomElement(['Niveau 1', 'Premier niveau']),
            'level3_description' => $this->faker->randomElement([$this->faker->text]),
            'level3_difficulty' => $this->faker->numberBetween(1, 3),
            'level3_cost' => $this->faker->text(5),
            'level3_duration' => $this->faker->text(5),
            'level3_assistance' => $this->faker->numberBetween(1, 3),
            'level3_info' => $this->faker->randomElement([$this->faker->text, null]),
        ];
    }
}
