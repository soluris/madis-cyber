<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Address>
 */
class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'address' => $this->faker->text(10),
            'moreInfos' => $this->faker->text(10),
            'cp' => $this->faker->numberBetween(10000, 99999),
            'city' => $this->faker->text(8),
            'codeInsee' => $this->faker->text(15),
        ];
    }
}
