<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MaturityQuestion>
 */
class MaturityQuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $n = $this->faker->text;

        return [
            'name' => $n,
            'slug' => $this->faker->randomElement([Str::slug($n), null, '']),
        ];
    }
}
