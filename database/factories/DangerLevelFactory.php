<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DangerLevel>
 */
class DangerLevelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(['critique', 'limité', 'important', 'Négligeable']),
            'color' => $this->faker->hexColor,
            'description' => $this->faker->text(300),
            'value' => $this->faker->numberBetween(10, 100),
        ];
    }
}
