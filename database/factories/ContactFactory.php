<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contact>
 */
class ContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'civility' => $this->faker->randomElement(['Monsieur', 'Madame']),
            'firstname' => $this->faker->firstName(),
            'lastname' => $this->faker->lastName(),
            'function' => $this->faker->text(8),
            'email' => $this->faker->unique()->safeEmail(),
            'phone' => '0' . $this->faker->numerify('#########'),
        ];
    }
}
