<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Organization>
 */
class OrganizationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $n = $this->faker->company();

        $types = [
            'Commune',
            'CCAS',
            'EPCI',
            'CIAS',
            'Syndicat départemental',
            'Autre',
        ];

        return [
            'name' => $n,
            'short_name' => Str::slug($n),
            'type' => $this->faker->randomElement($types),
            'siren' => $this->faker->imei(),
            'active' => $this->faker->boolean(75),
            'website' => $this->faker->url(),
            'info' => $this->faker->text(500),
            'address_id' => Address::factory()->create()->id,
            'referent_id' => Contact::factory()->create()->id,
            'referent_cyber_id' => Contact::factory()->create()->id,
            'referent_elu_id' => Contact::factory()->create()->id,
            'territory_id' => $this->faker->numberBetween(1, 5),
        ];
    }
}
