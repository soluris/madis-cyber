<?php

namespace Database\Factories;

use App\Models\Evaluation;
use App\Models\MaturityAnswer;
use App\Models\MaturityQuestion;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EvaluationMaturityAnswer>
 */
class EvaluationMaturityAnswerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $mqid = MaturityQuestion::inRandomOrder()->first()->id;

        return [
            'maturity_question_id' => MaturityQuestion::inRandomOrder()->first()->id,
            'maturity_answer_id' => MaturityAnswer::where('maturity_question_id', $mqid)->inRandomOrder()->first()->id,
            'evaluation_id' => Evaluation::inRandomOrder()->first()->id,
        ];
    }
}
