<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maturity_answers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('name')->comment('Texte de la réponse');
            $table->integer('level');
            $table->string('slug')->nullable();
            $table->unsignedBigInteger('maturity_question_id');
            $table->foreign('maturity_question_id')
                ->references('id')
                ->on('maturity_questions')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maturity_answers');
    }
};
