<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('maturity_questions', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->boolean('auto')->default(false);
        });

        // Add automatic questions
        // Call seeder
        Artisan::call('db:seed', [
            '--class' => 'MaturityQuestionSeeder',
            '--force' => true,
        ]);
        Artisan::call('db:seed', [
            '--class' => 'MaturityAnswerSeeder',
            '--force' => true,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maturity_questions', function (Blueprint $table) {
            $table->dropColumn(['title', 'subtitle', 'auto']);
        });
    }
};
