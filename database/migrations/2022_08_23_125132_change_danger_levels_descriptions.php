<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('danger_levels')->where('name', 'Négligeable')
            ->update([
                'color' => '#468355',
                'description' => 'La structure sera faiblement impactée, la résolution prendra quelques jours, aucune séquelle ne sera perceptible.',
            ]);
        DB::table('danger_levels')->where('name', 'Limité')
            ->update([
                'color' => '#FFC107',
                'description' => 'La structure sera faiblement impactée, la résolution prendra plusieurs semaines, aucune séquelle ne sera perceptible.',
            ]);
        DB::table('danger_levels')->where('name', 'Important')
            ->update([
                'color' => '#D63F49',
                'description' => 'La structure sera fortement impactée, la résolution prendra plusieurs semaines, les séquelles seront perceptibles sur plusieurs mois.',
            ]);
        DB::table('danger_levels')->where('name', 'Critique')
            ->update([
                'color' => '#454545',
                'description' => 'La structure sera fortement impactée, la résolution prendra plusieurs mois, les séquelles seront perceptibles sur plusieurs années.',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
