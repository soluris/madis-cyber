<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_maturity_answers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('maturity_question_id');
            $table->foreign('maturity_question_id')
                ->references('id')
                ->on('maturity_questions')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('maturity_answer_id');
            $table->foreign('maturity_answer_id')
                ->references('id')
                ->on('maturity_answers')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('evaluation_id');
            $table->foreign('evaluation_id')
                ->references('id')
                ->on('evaluations')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_maturity_answers');
    }
};
