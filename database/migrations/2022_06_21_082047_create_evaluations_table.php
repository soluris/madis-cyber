<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('organization_id');
            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade')
            ;

            $table->integer('status')->comment('Statut global de l\'evaluation');
            $table->integer('current_step')->comment('Etape en cours')->default(1);
            $table->string('updated_by')->comment('Nom de la personne qui a effectué la dernière modification');
            $table->string('author')->comment('Nom de la personne qui a créé l\'évaluation');
            $table->string('reference')->comment('Version du référentiel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
};
