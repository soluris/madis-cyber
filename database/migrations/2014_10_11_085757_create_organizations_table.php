<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('short_name');
            $table->string('type');
            $table->string('siren');
            $table->boolean('active');
            $table->string('website')->nullable();
            $table->text('info')->nullable();
            $table->longText('bilan1')->nullable();
            $table->longText('bilan2')->nullable();

            $table->unsignedBigInteger('address_id')->nullable();
            $table->unsignedBigInteger('referent_id')->nullable();
            $table->unsignedBigInteger('referent_cyber_id')->nullable();
            $table->unsignedBigInteger('referent_elu_id')->nullable();

            $table->foreign('address_id')->references('id')->on('address')->onDelete('cascade');
            $table->foreign('referent_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->foreign('referent_cyber_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->foreign('referent_elu_id')->references('id')->on('contacts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
};
