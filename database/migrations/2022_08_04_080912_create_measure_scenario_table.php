<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_scenario', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('scenario_id');
            $table->foreign('scenario_id')
                ->references('id')
                ->on('scenarios')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('measure_id');
            $table->foreign('measure_id')
                ->references('id')
                ->on('measures')
                ->onDelete('cascade')
            ;

            $table->unique(['scenario_id', 'measure_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_scenario');
    }
};
