<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $driver = env('DB_CONNECTION');

        if ('sqlite' === $driver) {
            DB::table('measures')->update(['level0_actual_label' => DB::raw("`level0_actual_label` || ' ou ne sais pas'")]);
        } else {
            DB::table('measures')->update(['level0_actual_label' => DB::raw("CONCAT(`level0_actual_label`, ' ou ne sais pas')")]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
