<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('maturity_answers')->where('name', 'le référent ne comprend pas ou ne se sent pas concerné par les notions de dangers cyber')
            ->update([
                'name' => 'Le référent ne comprend pas ou ne se sent pas concerné par les notions de dangers cyber.',
                'slug' => Str::slug('Le référent ne comprend pas ou ne se sent pas concerné par les notions de dangers cyber.'),
        ]);

        DB::table('maturity_answers')->where('name', 'le référent comprend les dangers auxquels est exposée sa collectivité')
            ->update([
                'name' => 'Le référent comprend les dangers auxquels est exposée sa collectivité.',
                'slug' => Str::slug('Le référent comprend les dangers auxquels est exposée sa collectivité.'),
        ]);

        DB::table('maturity_answers')->where('name', 'le référent comprend les dangers et a une bonne connaisance des mesures mises en œuvre dans sa collectivité')
            ->update([
                'name' => 'Le référent comprend les dangers et a une bonne connaisance des mesures mises en œuvre dans sa collectivité.',
                'slug' => Str::slug('Le référent comprend les dangers et a une bonne connaisance des mesures mises en œuvre dans sa collectivité.'),
        ]);

        DB::table('maturity_answers')->where('name', 'le référent comprend les dangers, connait les mesures mises en œuvre et a une bonne connaissance des projets de de la collectivité')
            ->update([
                'name' => 'Le référent comprend les dangers, connait les mesures mises en œuvre et a une bonne connaissance des projets de de la collectivité.',
                'slug' => Str::slug('Le référent comprend les dangers, connait les mesures mises en œuvre et a une bonne connaissance des projets de de la collectivité.'),
        ]);

        DB::table('maturity_answers')->where('name', "le référent n'est pas légitime à mettre en œuvre le plan de traitement cyber ")
            ->update([
                'name' => "Le référent n'est pas légitime à mettre en œuvre le plan de traitement cyber.",
                'slug' => Str::slug("Le référent n'est pas légitime à mettre en œuvre le plan de traitement cyber."),
        ]);

        DB::table('maturity_answers')->where('name', 'le référent dispose de la légitimité pour mettre en œuvre le plan de traitement cyber')
            ->update([
                'name' => 'Le référent dispose de la légitimité pour mettre en œuvre le plan de traitement cyber.',
                'slug' => Str::slug('Le référent dispose de la légitimité pour mettre en œuvre le plan de traitement cyber.'),
            ]);

        DB::table('maturity_answers')->where('name', 'le référent dispose de la légitimité et du budget nécessaire pour mettre en œuvre le plan de traitement cyber')
            ->update([
                'name' => 'Le référent dispose de la légitimité et du budget nécessaire pour mettre en œuvre le plan de traitement cyber.',
                'slug' => Str::slug('Le référent dispose de la légitimité et du budget nécessaire pour mettre en œuvre le plan de traitement cyber.'),
            ]);

        DB::table('maturity_answers')->where('name', 'le référent cyber dispose de la légitimité, du budget et du temps nécessaire pour suivre la mise en œuvre du plan de traitement cyber')
            ->update([
                'name' => 'Le référent cyber dispose de la légitimité, du budget et du temps nécessaire pour suivre la mise en œuvre du plan de traitement cyber.',
                'slug' => Str::slug('Le référent cyber dispose de la légitimité, du budget et du temps nécessaire pour suivre la mise en œuvre du plan de traitement cyber.'),
            ]);

        DB::table('maturity_answers')->where('name', 'le référent ne voit pas quel bénéfice il peut retirer de la démarche CMT')
            ->update([
                'name' => 'Le référent ne voit pas quel bénéfice il peut retirer de la démarche CMT.',
                'slug' => Str::slug('Le référent ne voit pas quel bénéfice il peut retirer de la démarche CMT.'),
            ]);

        DB::table('maturity_answers')->where('name', 'Le référent comprend les bénéfices mais ne souhaite pas traiter le sujet pour le moment / la démarche ne fait pas partie des sujets prioritaires')
            ->update([
                'name' => 'Le référent comprend les bénéfices mais ne souhaite pas traiter le sujet pour le moment / la démarche ne fait pas partie des sujets prioritaires.',
                'slug' => Str::slug('Le référent comprend les bénéfices mais ne souhaite pas traiter le sujet pour le moment / la démarche ne fait pas partie des sujets prioritaires.'),
            ]);

        DB::table('maturity_answers')->where('name', "le référent perçoit les bénéfices qu'il peut retirer de la démarche CMT pour lui, les équipes de sa collectivité et les citoyens et va s'impliquer dans la mise en œuvre")
            ->update([
                'name' => "Le référent perçoit les bénéfices qu'il peut retirer de la démarche CMT pour lui, les équipes de sa collectivité et les citoyens et va s'impliquer dans la mise en œuvre.",
                'slug' => Str::slug("Le référent perçoit les bénéfices qu'il peut retirer de la démarche CMT pour lui, les équipes de sa collectivité et les citoyens et va s'impliquer dans la mise en œuvre."),
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
