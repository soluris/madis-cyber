<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('measures')->where('short_name', 'AV et outils complémentaires')
            ->update([
            'level1_description' => 'La collectivité possède : <ul><li>un antivirus professionnel à jour sur chaque poste</li></ul>',
            'level2_description' => 'La collectivité possède : <ul><li>un antivirus professionnel (avec module complémentaires filtrage web, anti-hameçonnage, renforcement des transactions) à jour sur chaque poste</li></ul>',
            'level3_description' => "La collectivité s'assure :  <ul><li>que les alertes sont vérifiées annuellement, </li><li>qu'elle a son propre serveur DNS.</li></ul>",
        ]);
        DB::table('measures')->where('short_name', 'Cloisonnement réseaux')
            ->update([
                'level1_description' => 'La collectivité possède : <ul><li>un parefeu activé qui bloque les connexions entrantes</li></ul>',
                'level2_description' => 'La collectivité possède : <ul><li>un Wifi dédié et cloisonné pour les personnes extérieures</li></ul>',
                'level3_description' => 'La collectivité possède : <ul><li>un réseau dédié et isolé pour les copieurs,</li><li>un accès VPN pour les personnes à distance</li></ul>',
        ]);
        DB::table('measures')->where('short_name', 'Contrats')
            ->update([
                'level1_description' => 'La collectivité possède : <ul><li>Un inventaire des contrats mis à jour une fois par an</li></ul>',
                'level2_description' => "La collectivité s'assure : <ul><li>que les contrats les plus sensibles au niveau de la sécurité du système d’information (SSI) sont identifiés.</li></ul>",
                'level3_description' => "La collectivité s'assure : <ul><li>que les règles de sécurités minimales sont imposées aux prestataires sensibles.</li></ul>",
        ]);
        DB::table('measures')->where('short_name', 'Droits d\'accès logiques')
            ->update([
                'level1_description' => "La collectivité s'assure : <ul><li>qu'un inventaire des droits d'accès Windows est disponible et mis à jour une fois par an.</li><li>que les comptes de messagerie et des session Windows sont nominatifs.</li></ul>",
                'level2_description' => "La collectivité s'assure : <ul><li>qu'un inventaire des droits d'accès des applications métiers est disponible et mis à jour une fois par an.</li><li>que le départ des collaborateurs est géré (retrait des droits d’accès).</li></ul>",
                'level3_description' => "La collectivité s'assure : <ul><li>qu'un contrôle est réalisé une fois par an sur la base des inventaires.</li>",
            ]);
        DB::table('measures')->where('short_name', 'MAJ')
            ->update([
                'level1_description' => "La collectivité s'assure : <ul><li>que chaque poste est configuré pour que Windows se mette à jour automatiquement et régulièrement.</li><li>que les postes obsolètes sont remplacés.</li></ul>",
                'level2_description' => 'Chaque poste est configuré pour que les navigateurs et les suites bureautiques se mettent à jour automatiquement et régulièrement',
                'level3_description' => "La collectivité s'assure : <ul><li>que la bonne mise à jour des postes, des navigateurs et des suites bureautiques est vérifiée au moins une fois par an.</li></ul>",
                'level3_actual_label' => 'Un contrôle annuel des mises à jour des postes, navigateurs et suites bureautiques est réalisé.',
                'level1_actual_label' => "Un inventaire des droits d'accès Windows, comptes de messagerie et sessions Windows est disponible",
                'level1_preconized_label' => "Inventorier les droits d'accès Windows, comptes de messagerie et sessions Windows.",
            ]);
        DB::table('measures')->where('short_name', 'Modes dégradés')
            ->update([
                'level1_description' => "La collectivité s'assure :<ul><li>de contacter Cybermalveillance, la gendarmerie ou la police et/ou son OPSN en cas d'incidents cyber.</li></ul>",
                'level2_description' => 'La collectivité possède : <ul><li>un document expliquant comment réagir en cas de ransomware/rançongiciel et affiché pour les agents dans la collectivité</li></ul>',
                'level3_description' => 'La collectivité possède : <ul><li>des documents sur les opérations nécessaires à la reconstruction de son système d’information (SI).</li></ul>',
            ]);
        DB::table('measures')->where('short_name', 'Plan informatique')
            ->update([
                'level1_description' => 'La collectivité possède : <ul><li>un inventaire des équipements informatiques mis à jour une fois par an.</li></ul>',
                'level2_description' => "La collectivité possède : <ul><li>un schéma d'implantation par bâtiment mis à jour une fois par an.</li></ul>",
                'level3_description' => 'La collectivité possède : <ul><li>un schéma réseau mis à jour une fois par an</li></ul>',
            ]);
        DB::table('measures')->where('short_name', 'Mots de passe')
            ->update([
                'level1_description' => "La collectivité s'assure : <ul><li>que chaque utilisateur dispose d'un mot de passe de connexion Windows personnel comprenant 9 caractères (chiffre, minuscule, majuscule, caractères spéciaux).</li></ul>",
                'level2_description' => "La collectivité s'assure : <ul><li>que chaque utilisateur dispose d'un mot de passe Windows personnel et de messagerie distinct comprenant 12 caractères (chiffre, minuscule, majuscule, caractères spéciaux) et changement tous les 2 ans.</li></ul>",
                'level3_description' => "La collectivité s'assure : <ul><li>que chaque utilisateur dispose d'un mot de passe personnel pour la session Windows avec un compte sans privilèges (utilisateur).</li><li>que le mot de passe de la session doit comprendre au moins 12 caractères (chiffres, minuscules, majuscules, caractères spéciaux) et changement tous les 2 ans.</li></ul>",
            ]);
        DB::table('measures')->where('short_name', 'Sauvegardes')
            ->update([
                'level1_description' => 'La collectivité possède : <ul><li>Une sauvegarde régulière sur un support externe et une copie réalisée sur un autre support.</li></ul>',
                'level2_description' => 'La collectivité possède : <ul><li>une sauvegarde manuelle régulière sur deux supports différents. - une des sauvegardes est externalisée.</li></ul>',
                'level3_description' => "La collectivité s'assure : <ul><li>qu'une fois par an un test de restauration technique est réalisé.</li></ul>",
            ]);
        DB::table('measures')->where('short_name', 'Sensibilisation')
            ->update([
                'level1_description' => "La collectivité s'assure : <ul><li>que les fiches pratiques Cybermalveillance sont diffusées aux agents et élus de la collectivité.</li></ul>",
                'level2_description' => "La collectivité s'assure : <ul><li>que les agents et élus suivent régulièrement des événementiels locaux ou nationaux ayant trait à la cybersécurité.</li></ul>",
                'level3_description' => "La collectivité s'assure : <ul><li>qu'une réunion de sensibilisation annuelle sur les risques du numérique et les bonnes pratiques est organisée avec l’ensemble des agents et des élus.</li></ul>",
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
