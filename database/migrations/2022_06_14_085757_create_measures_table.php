<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measures', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->string('short_name');
            $table->string('level0_actual_label');
            $table->string('level1_actual_label');
            $table->string('level1_preconized_label');
            $table->longText('level1_file')->nullable();
            $table->text('level1_description');
            $table->integer('level1_difficulty');
            $table->string('level1_cost');
            $table->string('level1_duration');
            $table->integer('level1_assistance');
            $table->text('level1_info')->nullable();
            $table->string('level2_actual_label');
            $table->string('level2_preconized_label');
            $table->text('level2_file')->nullable();
            $table->text('level2_description');
            $table->integer('level2_difficulty');
            $table->string('level2_cost');
            $table->string('level2_duration');
            $table->integer('level2_assistance');
            $table->text('level2_info')->nullable();
            $table->string('level3_actual_label');
            $table->string('level3_preconized_label');
            $table->text('level3_file')->nullable();
            $table->text('level3_description');
            $table->integer('level3_difficulty');
            $table->string('level3_cost');
            $table->string('level3_duration');
            $table->integer('level3_assistance');
            $table->text('level3_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measures');
    }
};
