<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measure_levels', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('evaluation_id');
            $table->foreign('evaluation_id')
                ->references('id')
                ->on('evaluations')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('measure_id');
            $table->foreign('measure_id')
                ->references('id')
                ->on('measures')
                ->onDelete('cascade')
            ;
            $table->date('end_date')->nullable();
            $table->integer('expected_level')->nullable();
            $table->integer('actual_level')->nullable();
            $table->string('manager')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measure_levels');
    }
};
