<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danger_scenario', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('scenario_id');
            $table->foreign('scenario_id')
                ->references('id')
                ->on('scenarios')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('danger_id');
            $table->foreign('danger_id')
                ->references('id')
                ->on('measures')
                ->onDelete('cascade')
            ;

            $table->unique(['scenario_id', 'danger_id']);
        });

        // Call seeder
        Artisan::call('db:seed', [
            '--class' => 'ScenarioSeeder',
            '--force' => true,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danger_scenario');
    }
};
