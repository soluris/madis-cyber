<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('measures', function (Blueprint $table) {
            $table->integer('level0_value')->default(0);
            $table->integer('level1_value')->default(45);
            $table->integer('level2_value')->default(70);
            $table->integer('level3_value')->default(100);
            $table->boolean('fundamental')->default(false);
        });

        // Set double values for fundamental measures

        \Illuminate\Support\Facades\DB::table('measures')
            ->where('short_name', 'Mots de passe')
            ->orWhere('short_name', 'Sauvegardes')
            ->orWhere('short_name', 'MAJ')
            ->orWhere('short_name', 'Sensibilisation')
            ->update([
                'level1_value' => 90,
                'level2_value' => 140,
                'level3_value' => 200,
                'fundamental' => true,
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('measures', function (Blueprint $table) {
            $table->dropColumn(['level0_value', 'level1_value', 'level2_value', 'level3_value', 'fundamental']);
        });
    }
};
