<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danger_level_evaluation', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('danger_level_id')->nullable();
            $table->foreign('danger_level_id')
                ->references('id')
                ->on('danger_levels')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('evaluation_id')->nullable();
            $table->foreign('evaluation_id')
                ->references('id')
                ->on('evaluations')
                ->onDelete('cascade')
            ;
            $table->unsignedBigInteger('danger_id')->nullable();
            $table->foreign('danger_id')
                ->references('id')
                ->on('dangers')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danger_level_evaluation');
    }
};
