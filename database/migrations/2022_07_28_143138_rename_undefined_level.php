<?php

use Illuminate\Database\Migrations\Migration;

return new class() extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('danger_levels')->where('name', 'Non défini')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('danger_levels')->insert(['name' => 'Non défini', 'color' => '#09759B', 'description' => '', 'value' => 0]);
    }
};
