/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue'

import VueRouter from 'vue-router'
import Vuex from 'vuex'
import { ClientTable } from 'vue-tables-2'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted'
import CKEditor from '@ckeditor/ckeditor5-vue2'

import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'
import Pagination from './components/Pagination'

import routes from './routes'
import stores from './stores'

require('./bootstrap')

Vue.prototype.$reference = window.referentielVersion
Vue.prototype.$seuilAlerte = window.seuilAlerte
Vue.prototype.$footerLink = window.footerLink
Vue.prototype.$logoSidebar = window.logoSidebar
Vue.prototype.$appVersion = window.appVersion

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(ClientTable,
    {}, false,
    'bootstrap3',
    {
        pagination: Pagination
    }
)
Vue.use(Vuelidate)
Vue.use(Toasted, {
    iconPack: 'fontawesome'
})
Vue.use(CKEditor)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

export const router = new VueRouter({
    routes,
    mode: 'history'
})

const store = new Vuex.Store(stores)

store.dispatch('getUser')

const vm = new Vue({
    store,
    router
})

vm.$mount('#app')
