import App from '../components/App.vue'
import Credits from '../components/Credits'
import Login from '../components/Auth/Login.vue'
import Users from '../components/Users/index'
import Profile from '../components/Users/Profile'
import UsersList from '../components/Users/List'
import SingleUser from '../components/Users/Single'
import Organizations from '../components/Organizations/index'
import OrganizationsList from '../components/Organizations/List'
import SingleOrganization from '../components/Organizations/Single/index'
import Dashboard from '../components/Dashboard'
import Measures from '../components/Measures'
import MeasuresList from '../components/Measures/List'
import SingleMeasure from '../components/Measures/Single/index'
import Evaluations from '../components/Evaluations'
import EvaluationsList from '../components/Evaluations/List'
import SingleEvaluation from '../components/Evaluations/Single'
import EditEvaluation from '../components/Evaluations/Single/Edit'
import EvaluationStep from '../components/Evaluations/Single/Steps/Step'
import ViewEvaluation from '../components/Evaluations/Single/View'
import NotFound from "../components/Errors/NotFound";
import Territories from '../components/Territories/index'
import TerritoriesList from '../components/Territories/List'
import SingleTerritory from '../components/Territories/Single/index'

export default [
    {
        path: '/',
        name: 'Tableau de bord',
        component: App,
        meta: {
            icon: 'fa-tachometer-alt',
            title: 'Tableau de bord'
        },
        children: [
            {
                path: '',
                component: Dashboard,
                meta: {
                    icon: 'fa-tachometer-alt',
                    title: 'Tableau de bord'
                }
            },
            {
                path: 'login',
                component: Login
            },
            {
                path: 'credits',
                component: Credits,
                name: 'Crédits',
                meta: {
                    title: 'Crédits'
                }
            },
            {
                path: 'profil',
                component: Profile,
                children: [
                    {
                        path: '',
                        name: 'Profil',
                        component: SingleUser,
                        meta: {
                        // icon: 'fa-user',
                            title: 'Modifier mon profil',
                            type: 'profile'
                        }
                    }
                ]
            },
            {
                path: 'utilisateurs',
                component: Users,
                name: 'Liste des utilisateurs',
                meta: {
                // icon: 'fa-users',
                    title: 'Liste des utilisateurs'
                },
                children: [
                    {
                        path: '',
                        component: UsersList,
                        meta: {
                        // icon: 'fa-users',
                            title: 'Liste des utilisateurs'
                        }
                    },
                    {
                        name: 'Créer un utilisateur',
                        path: 'nouveau',
                        component: SingleUser,
                        meta: {
                        // icon: 'fa-user',
                            title: 'Créer un utilisateur',
                            type: 'user'
                        }
                    },
                    {
                        name: 'Modifier',
                        path: ':id',
                        component: SingleUser,
                        meta: {
                        // icon: 'fa-user',
                            title: 'Modifier un utilisateur',
                            type: 'user'
                        }
                    }
                ]
            },
            {
                path: 'evaluations',
                name: 'Liste des évaluations',
                meta: {
                // icon: 'fa-search',
                    title: 'Liste des évaluations'
                },
                component: Evaluations,
                children: [
                    {
                        path: '',
                        component: EvaluationsList,
                        meta: {
                            title: 'Liste des évaluations'
                        }
                    },
                    {
                        path: ':id',
                        component: SingleEvaluation,
                        meta: { title: 'Éditer une évaluation', type: 'evaluation' },
                        children: [
                            {
                                name: "Résultats de l'évaluation",
                                path: 'resultats',
                                component: ViewEvaluation,
                                meta: {
                                    title: "Résultats de l'évaluation",
                                    type: 'evaluation'
                                }
                            },
                            {
                                path: '',
                                component: EditEvaluation,
                                children: [
                                    {
                                        name: 'Éditer une évaluation',
                                        path: ':step',
                                        component: EvaluationStep,
                                        meta: { title: 'Éditer une évaluation', type: 'evaluation', edit:true }
                                    },
                                ]
                            }
                        ]

                    }
                ]
            },
            {
                path: 'mesures',
                name: 'Liste des mesures de sécurité',
                meta: {
                // icon: 'fa-list'
                },
                component: Measures,
                children: [
                    {
                        path: '',
                        component: MeasuresList,
                        meta: {
                            title: 'Liste des mesures',
                            type: 'measure'
                        }
                    },
                    {
                        name: 'Modifier',
                        path: ':id',
                        component: SingleMeasure,
                        meta: {
                        // icon: 'fa-user',
                            title: 'Modifier une mesure',
                            type: 'measure'
                        }
                    }
                ]
            },
            {
                path: 'territoires',
                name: 'Liste des territoires',
                meta: {
                    // icon: 'fa-home',
                    title: 'Liste des territoires'
                },
                component: Territories,
                children: [
                    {
                        path: '',
                        component: TerritoriesList,
                        meta: {
                            // icon: 'fa-home',
                            title: 'Liste des territoires'
                        }
                    },
                    {
                        name: 'Créer un territoire',
                        path: 'nouveau',
                        component: SingleTerritory,
                        meta: {
                            // icon: 'fa-home',
                            title: 'Créer un territoire',
                            type: 'territory'
                        }
                    },
                    {
                        name: 'Modifier',
                        path: ':id',
                        component: SingleTerritory,
                        meta: {
                            // icon: 'fa-home',
                            title: 'Modifier un territoire',
                            type: 'territory'
                        }
                    }
                ]
            },
            {
                path: 'structures',
                name: 'Liste des structures',
                meta: {
                // icon: 'fa-home',
                    title: 'Liste des structures'
                },
                component: Organizations,
                children: [
                    {
                        path: '',
                        component: OrganizationsList,
                        meta: {
                        // icon: 'fa-home',
                            title: 'Liste des structures'
                        }
                    },
                    {
                        name: 'Créer une structure',
                        path: 'nouveau',
                        component: SingleOrganization,
                        meta: {
                        // icon: 'fa-home',
                            title: 'Créer une structure',
                            type: 'organization'
                        }
                    },
                    {
                        name: 'Modifier',
                        path: ':id',
                        component: SingleOrganization,
                        meta: {
                        // icon: 'fa-home',
                            title: 'Modifier une structure',
                            type: 'organization'
                        }
                    }
                ]
            },
            { path: "*", component: NotFound },
        ]
    }
]
