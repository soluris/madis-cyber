export default function toaster (that, text, bg, icon) {
    const setIcon = icon ? { name: icon } : null
    that = that || this
    that.$toasted.show(`<b>${text}</b>`, {
        theme: 'toasted-primary',
        position: 'bottom-center',
        duration: 4000,
        closeOnSwipe: true,
        className: bg,
        action: {
            icon: 'times',
            onClick: (e, toastObject) => {
                toastObject.goAway(0)
            }
        },
        icon: setIcon
    })
}
