export default function diacritic(type,typeName,value) {
    if (!value) {
        return true;
    }
    if (!type) {
        return false
    }
    const pattern = value
        .toLowerCase()
        .replace(/[aáàãâä]/g, 'a')
        .replace(/[eéèêë]/g, 'e')
        .replace(/[iíìîï]/g, 'i')
        .replace(/[oóòõôö]/g, 'o')
        .replace(/[uúùûü]/g, 'u')
        .replace(/[cç]/g, 'c')
    const name = typeName
        .toLowerCase()
        .replace(/[aáàãâä]/g, 'a')
        .replace(/[eéèêë]/g, 'e')
        .replace(/[iíìîï]/g, 'i')
        .replace(/[oóòõôö]/g, 'o')
        .replace(/[uúùûü]/g, 'u')
        .replace(/[cç]/g, 'c')

    return name.includes(pattern);
}