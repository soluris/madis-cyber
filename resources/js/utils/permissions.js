export function isAdmin () {
    if (this && this.$store) {
        const perm = this.$store.state.user.roles
        const user = this.$store.state.user.data
        return user ? parseInt(user.role) === perm.ROLE_ADMIN : false
    }
    return false
}
export function isManager () {
    if (this && this.$store) {
        const perm = this.$store.state.user.roles
        const user = this.$store.state.user.data
        return user ? parseInt(user.role) === perm.ROLE_MANAGER : false
    }
    return false
}
export function isUser () {
    if (this && this.$store) {
        const perm = this.$store.state.user.roles
        const user = this.$store.state.user.data
        return user ? parseInt(user.role) === perm.ROLE_USER : false
    }
    return false
}