import axios from 'axios'
import toaster from '../utils/toaster.js'

export default {
    state: { all: [], errors: null },
    mutations: {
        setOrganizations (state, items) {
            state.all = items
        },
        updateOrganization (state, item) {
            const index = state.all.findIndex(s => s.id === item.id)
            state.all = state.all.filter(s => s.id !== item.id)
            state.all.splice(index, 0, item)
        },
        deleteOrganization (state, item) {
            state.all = state.all.filter(u => u.id !== item.id)
        },
        setErrors (state, errors) {
            state.errors = errors
        }
    },
    actions: {
        getOrganizations ({ commit }) {
            const req = axios.get('/api/organizations')
            req.then(res => {
                commit('setOrganizations', res.data)
            }).catch(err => {
                console.log('could not get users', err)
            })
            return req
        },
        updateOrganization ({ commit, dispatch }, item) {
            const req = item.id ? axios.put(`/api/organizations/${item.id}`, item) : axios.post('/api/organizations', item)
            req.then(res => {
                commit('updateOrganization', res.data)
                item.id ? toaster(this._vm, `La structure ${item.name} a été modifiée`, 'bg-success', 'check') : toaster(this._vm, `La structure ${item.name} a été créée`, 'bg-success', 'check')
                // Update users
                dispatch('getUsers')
                // Update evaluations
                dispatch('getEvaluations')
            }).catch(err => {
                console.log('could not update org', err)
                commit('setErrors', { errors: err.response.data, status: err.response.status })
            })
            return req
        },
        deleteOrganization ({ commit, dispatch }, item) {
            console.log('deleteOrganization', item)
            const req = axios.delete(`/api/organizations/${item.id}`, item)
            req.then(res => {
                commit('deleteOrganization', item)
                // Refresh users to show that some have been deleted
                dispatch('getUsers')
                // Refresh evaluations to show that some have been deleted
                dispatch('getEvaluations')
                toaster(this._vm, `La structure ${item.name} a été supprimée`, 'bg-info', 'trash')
            }).catch(err => {
                console.log('could not delete org', err)
            })
            return req
        }
    }

}
