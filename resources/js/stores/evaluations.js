import axios from 'axios'
import toaster from '../utils/toaster.js'

export default {
    state: { all: [], stepView: 1, errors: null, loading: false },
    mutations: {
        setEvaluations (state, items) {
            state.all = items
        },
        updateEvaluation (state, item) {
            const index = state.all.findIndex(s => s.id === item.id)
            state.all = state.all.filter(s => s.id !== item.id)
            state.all.splice(index, 0, item)
        },
        deleteEvaluation (state, item) {
            state.all = state.all.filter(u => u.id !== item.id)
        },
        setErrors (state, errors) {
            state.errors = errors
        },
        setStepView (state, number) {
            // Side effect : scroll to top when changing page
            console.log('setting stepview', number)
            setTimeout(() => {
                window.scrollTo(0,0);
            }, 100)

            state.stepView = number
        },
        setLoading(state, loading) {
            state.loading = loading
        }
    },
    actions: {
        getEvaluations ({ commit }) {
            commit('setLoading', true)
            const req = axios.get('/api/evaluations')
            req.then(res => {
                commit('setEvaluations', res.data)
                commit('setLoading', false)
            }).catch(err => {
                console.log('could not get evaluations', err)
                commit('setLoading', false)
            })
            return req
        },
        updateEvaluation ({ commit }, item) {
            const toast = item.toast
            delete item.toast
            const req = item.id ? axios.put(`/api/evaluations/${item.id}`, item) : axios.post('/api/evaluations', item)
            req.then(res => {
                commit('updateEvaluation', res.data)
                item.id && toast && toaster(this._vm, 'Mise à jour effectuée', 'bg-success', 'check')
            }).catch(err => {
                console.log('could not update evaluation', err)
                commit('setErrors', { errors: err.response.data, status: err.response.status })
            })
            return req
        },
        saveEvaluation ({ commit, dispatch }, item) {
            return dispatch('updateEvaluation', { ...item, draft: 0 })
        },
        saveDraftEvaluation ({ commit, dispatch }, item) {
            return dispatch('updateEvaluation', { ...item, draft: 1 })
        },
        deleteEvaluation ({ commit }, item) {
            console.log('deleteEvaluation', item)
            const req = axios.delete(`/api/evaluations/${item.id}`, item)
            req.then(res => {
                commit('deleteEvaluation', item)
                toaster(this._vm, `La suppression de l'évaluation a été effectuée`, 'bg-success', 'check')
            }).catch(err => {
                console.log('could not delete evaluation', err)
            })
            return req
        }
    }

}
