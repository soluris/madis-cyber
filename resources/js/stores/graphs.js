import axios from 'axios'
import Vue from "vue";

export default {
    state: { },
    mutations: {
        setGraphData (state, {name, data, id}) {
            if (typeof state[id] === 'undefined') {
                Vue.set(state, id, {})
            }

            const newData = {...state[id], [name]: data}
            Vue.set(state, id, newData)
            state[id] = newData
        },
        removeGraphData (state, {id}) {
            state[id] = {}
            Vue.delete(state, id)
        }
    },
    actions: {
        async getOrganizationData({commit}, {territoryId}) {
            const req = await axios.get(`/api/graphs/organizations/${territoryId || ''}`)
            commit('setGraphData', { name: 'organization', data: req.data, id: 'organization' })
            return req.data
        },
        async getBestMeasures({commit}, {id}) {
            const req = await axios.get(`/api/evaluations/${id}/graphs/best_measures/`)
            commit('setGraphData', { name: 'bestMeasures', data: req.data, id })
            return req.data
        },
        async getGraphForEvaluation ({ commit }, {name, id}) {
            const req = await axios.get(`/api/evaluations/${id}/graphs/${name}`)
            commit('setGraphData', { name, data: req.data, id })
            return req.data
        },
        async getAllGraphsForEvaluation ({ commit }, id) {
            const graphs = [
                'risks',
                'measures',
                'futurerisks',
                'exposition',
                'attack',
            ]
            graphs.forEach((name) => {
                axios.get(`/api/evaluations/${id}/graphs/${name}`).then(res => {
                    commit('setGraphData', { name, data: res.data, id })
                })
            })
        }
    }
}
