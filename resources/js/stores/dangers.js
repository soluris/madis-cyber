import axios from 'axios'

export default {
    state: { all: [] },
    mutations: {
        setDangers (state, items) {
            state.all = items
        }
    },
    actions: {
        getDangers ({ commit }) {
            const req = axios.get('/api/dangers')
            req.then(res => {
                commit('setDangers', res.data)
            }).catch(err => {
                console.log('could not get dangers', err)
            })
            return req
        }
    }

}
