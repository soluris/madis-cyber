import axios from 'axios'

export default {
    state: { all: [] },
    mutations: {
        setQuestions(state, items) {
            state.all = items
        }
    },
    actions: {
        getQuestions ({ commit }) {
            const req = axios.get('/api/questions')
            req.then(res => {
                commit('setQuestions', res.data)
            }).catch(err => {
                console.log('could not get questions', err)
            })
            return req
        }
    }

}
