import axios from 'axios'
import toaster from '../utils/toaster.js'

export default {
    state: { all: [], errors: null },
    mutations: {
        setTerritories (state, items) {
            state.all = items
        },
        updateTerritory (state, item) {
            const index = state.all.findIndex(s => s.id === item.id)
            state.all = state.all.filter(s => s.id !== item.id)
            state.all.splice(index, 0, item)
        },
        deleteTerritory (state, item) {
            state.all = state.all.filter(u => u.id !== item.id)
        },
        setErrors (state, errors) {
            state.errors = errors
        }
    },
    actions: {
        getTerritories ({ commit }) {
            const req = axios.get('/api/territories')
            req.then(res => {
                commit('setTerritories', res.data)
            }).catch(err => {
                console.log('could not get users', err)
            })
            return req
        },
        updateTerritory ({ commit, dispatch }, item) {
            const req = item.id ? axios.put(`/api/territories/${item.id}`, item) : axios.post('/api/territories', item)
            req.then(res => {
                commit('updateTerritory', res.data)
                dispatch('getOrganizations')
                dispatch('getTerritories')
                item.id ? toaster(this._vm, `Le territoire ${item.name} a été modifié`, 'bg-success', 'check') : toaster(this._vm, `Le territoire ${item.name} a été créé`, 'bg-success', 'check')
            }).catch(err => {
                console.log('could not update ter', err)
                commit('setErrors', { errors: err.response.data, status: err.response.status })
            })
            return req
        },
        deleteTerritory ({ commit, dispatch }, item) {
            console.log('deleteTerritory', item)
            const req = axios.delete(`/api/territories/${item.id}`, item)
            req.then(res => {
                commit('deleteTerritory', item)
                // Refresh users to show that some have been deleted
                dispatch('getTerritories')
                toaster(this._vm, `Le territoire ${item.name} a été supprimé`, 'bg-info', 'trash')
            }).catch(err => {
                console.log('could not delete ter', err)
            })
            return req
        }
    }

}
