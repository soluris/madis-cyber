import axios from 'axios'
import toaster from '../utils/toaster.js'

export default {
    state: {
        all: [],
        difficulty: {
            Simple: 1,
            Moyen: 2,
            Compliqué: 3
        },
        assistance: {
            Simple: 1,
            Moyen: 2,
            Élevé: 3
        },
        errors: null
    },
    mutations: {
        setMeasures (state, items) {
            state.all = items
        },
        updateMeasure (state, item) {
            const index = state.all.findIndex(s => s.id === item.id)
            state.all = state.all.filter(s => s.id !== item.id)
            state.all.splice(index, 0, item)
        },
        setErrors (state, errors) {
            state.errors = errors
        }
    },
    actions: {
        getMeasures ({ commit }) {
            const req = axios.get('/api/measures')
            req.then(res => {
                commit('setMeasures', res.data)
            }).catch(err => {
                console.log('could not get measures', err)
            })
            return req
        },
        updateMeasure ({ commit }, item) {
            const req = axios.put(`/api/measures/${item.id}`, item)
            req.then(res => {
                commit('updateMeasure', res.data)
                toaster(this._vm, `La mesure ${item.name} a été modifiée`, 'bg-success', 'check')
            }).catch(err => {
                console.log('could not update measures', err)
                commit('setErrors', { errors: err.response.data, status: err.response.status })
            })
            return req
        }
    }

}
