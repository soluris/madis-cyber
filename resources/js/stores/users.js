import axios from 'axios'
import toaster from '../utils/toaster.js'

export default {
    state: { all: [], errors: null },
    mutations: {
        setUsers (state, items) {
            console.log('all users', items)
            state.all = items
        },
        updateUser (state, item) {
            const index = state.all.findIndex(s => s.id === item.id)
            state.all = state.all.filter(s => s.id !== item.id)
            state.all.splice(index, 0, item)
        },
        deleteUser (state, item) {
            state.all = state.all.filter(u => u.id !== item.id)
        },
        setErrors (state, errors) {
            state.errors = errors
        }
    },
    actions: {
        getUsers ({ commit }) {
            const req = axios.get('/api/users')
            req.then(res => {
                commit('setUsers', res.data)
            }).catch(err => {
                console.log('could not get users', err)
            })
            return req
        },
        updateUser ({ commit }, user) {
            commit('setErrors', null)
            const req = user.id ? axios.put(`/api/users/${user.id}`, user) : axios.post('/api/users', user)
            req.then(res => {
                commit('updateUser', res.data)
                user.id ? toaster(this._vm, `L'utilisateur ${user.firstname} ${user.lastname} a été modifié`, 'bg-success', 'check') : toaster(this._vm, `L'utilisateur ${user.firstname} ${user.lastname} a été créé`, 'bg-success', 'check')
            }).catch(err => {
                console.log('could not update users', err.response)
                commit('setErrors', { errors: err.response.data, status: err.response.status })
            })
            return req
        },
        deleteUser ({ commit }, user) {
            console.log('deleteUser', user)
            const req = axios.delete(`/api/users/${user.id}`, user)
            req.then(res => {
                commit('deleteUser', user)
                toaster(this._vm, `L'utilisateur ${user.firstname} ${user.lastname} a été supprimé`, 'bg-info', 'trash')
            }).catch(err => {
                console.log('could not delete users', err)
            })
            return req
        }
    }

}
