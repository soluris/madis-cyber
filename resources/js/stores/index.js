import user from './user.js'
import users from './users.js'
import organizations from './organizations.js'
import loading from './loading.js'
import evaluations from './evaluations.js'
import measures from './measures.js'
import dangers from './dangers.js'
import questions from './questions.js'
import graphs from './graphs.js'
import territories from './territories.js'

export default {
    modules: {
        user,
        users,
        organizations,
        loading,
        evaluations,
        measures,
        dangers,
        questions,
        graphs,
        territories,
    }
}
