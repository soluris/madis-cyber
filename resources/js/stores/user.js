import axios from 'axios'
import { router } from '../app'

export default {
    state: {
        data: null,
        roles: {
            ROLE_ADMIN: 0,
            ROLE_MANAGER: 10,
            ROLE_USER: 20
        },
        previousUser: null,
        previousToken: null
    },
    mutations: {
        setUser (state, user) {
            state.data = user
        },
        setPreviousUser (state, user) {
            if (user) {
                state.previousUser = user
            } else {
                state.previousUser = state.data
            }

            localStorage.setItem('previousUser', JSON.stringify(state.previousUser))
        },
        unsetPreviousUser (state) {
            state.previousUser = null
            state = {...state, previousUser:null}
            localStorage.removeItem('previousUser')
        }
    },
    actions: {
        async getUser ({ commit, dispatch }) {
            // try to get previous User from localstorage
            const pu = localStorage.getItem('previousUser')
            commit('setLoading', true)
            if (pu) {
                commit('setPreviousUser', JSON.parse(pu))
            }

            try {
                const r = await axios.get('/api/users/me')
                commit('setUser', r.data)
                dispatch('getUsers')
                dispatch('getOrganizations')
                commit('setLoading', false)
                dispatch('getEvaluations')
                dispatch('getMeasures')
                dispatch('getDangers')
                dispatch('getQuestions')
                dispatch('getTerritories')
                return r.data
            } catch (e) {
                router.push('/login')
                commit('setLoading', false)
                throw e
            }
        },
        async changeUser ({ commit, dispatch }, id) {
            try {
                console.log('logging in as user id ', id)

                await axios.post('/api/impersonate', { id })
                commit('unsetPreviousUser')
                commit('setPreviousUser')
            } catch (e) {
                console.log('could not impersonate user with id ' + id, e)
                throw e
            }

            return await dispatch('getUser')
        },
        revertUser ({ commit, dispatch }, id) {
            const req = axios.post('/api/leave')
            req.then(r => {
                commit('unsetPreviousUser')
                dispatch('getUser')
            }).catch(err => {
                console.log('could not leave impersonation', err)
            })

            return req
        },
        logout ({ commit }) {
            commit('setLoading', true)
            setTimeout(() => {
                axios.get('/logout').then(r => {
                    commit('setUser', null)
                    commit('setLoading', false)
                    router.push('/login')
                }).catch(e => {
                    window.location = '/logout'
                })
            }, 50)


        }
    }
}
