<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Madis Cyber</title>
    <link href="{{ mix('css/app.css')}}" rel="stylesheet">
    <link rel="icon" href="{{mix('images/' . env('APP_FAVICON_PATH', 'logo_madis_2020_favicon.png'))}}" type="image/x-icon">
</head>
<body class=" layout-fixed {{\Illuminate\Support\Facades\Auth::user() ? '' :'layout-top-nav' }} bg-light">

    @yield('content')

    @yield('footer')

    @stack('js')
</body>
</html>
