@extends('layouts.page_with_header')

@section('content')

    <div id="app">
        <router-view></router-view>
    </div>
@stop

@section('footer')
@stop

@push('js')
    <script>
        window.referentielVersion = '{{ env('REFERENTIEL_VERSION') }}';
        window.seuilAlerte = '{{ env('SEUIL_ALERTE') }}';
        window.footerLink = '{{ env('MIX_FOOTER_LINK') }}';
        window.logoSidebar = '{{ mix('images/' . env('LOGO_SIDEBAR', 'soluris-logo-white.png')) }}';
        window.appVersion = '';
    </script>
    <script src="{{ mix('js/app.js') }}"></script>

@endpush
