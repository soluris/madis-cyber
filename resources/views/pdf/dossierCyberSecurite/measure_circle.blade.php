<?php
if (!function_exists('getProgressFromLevel')) {
    function getProgressFromLevel($level)
    {
        return match ($level) {
            0 => 15,
            1 => 33,
            2 => 66,
            3 => 100,
            default => 0,
        };
    }
}
$actualColor = 'transparent';
switch ($action->actual_level) {
    case 0:
        $actualColor = '#454545';
        break;
    case 1:
        $actualColor = '#D63F49';
        break;
    case 2:
        $actualColor = '#FFC107';
        break;
    case 3:
        $actualColor = '#468355';
        break;
}
$expectedColor = 'transparent';
switch ($action->expected_level) {
    case 0:
        $expectedColor = '#454545';
        break;
    case 1:
        $expectedColor = '#D63F49';
        break;
    case 2:
        $expectedColor = '#FFC107';
        break;
    case 3:
        $expectedColor = '#468355';
        break;
}
$expectedDashOffset = (new \App\Helpers\SVG(getProgressFromLevel($action->expected_level), 100))->dashArray();
$actualDashOffset = (new \App\Helpers\SVG(getProgressFromLevel($action->actual_level), 100))->dashArray();
$svgSize = isset($size) ? $size : 150;
$globalSize = isset($size) ? $size + $size / 8 : 190;
?>



<div style="height: {{$globalSize}}px;position:relative">
    <p style="text-align: center;z-index: 1;position: absolute;top: 25%;left: 0;right: 0;" class="@if($action->actual_level === 3) custom-crown @endif">
        @if ($action->actual_level === 3)
            <i class="fas fa-crown"></i>
        @endif
        @if($svgSize >= 150)
        <span class="@if($action->actual_level === 3) text-white @else text-muted @endif text-uppercase small">Niveau</span>  <br/>
        @endif
        <span class="h1" @if($svgSize < 150) style="margin: 0; padding: 0;line-height:1.8em;font-size: 1.8em" @endif>
            {{$action->actual_level}}
        </span>
    </p>
    @if($action->actual_level === 3)
{{--        Show crown for level 3--}}
        <div style="position: relative">
            <svg xmlns="http://www.w3.org/2000/svg" width="{{$svgSize}}" height="{{$svgSize}}" fill="#468355" viewBox="0 0 16 16">
                <path d="m8 1.288 6.842 5.56L12.267 15H3.733L1.158 6.847 8 1.288zM16 6.5 8 0 0 6.5 3 16h10l3-9.5z"/>
            </svg>

            <svg
                xmlns="http://www.w3.org/2000/svg" width="{{$svgSize-40}}"
                height="{{$svgSize-40}}" fill="#468355"
                viewBox="0 0 16 16"
                style="top: 23px">
                <path d="m8 0 8 6.5-3 9.5H3L0 6.5 8 0z"/>
            </svg>
        </div>
    @else
{{--        Show circle for other levels--}}
        <div style="position: relative;width:{{$svgSize+10}}px;height:{{$svgSize+10}}px;margin: 0 auto;">
            <!-- BASE CIRCLE -->
            <svg data-v-4d45322b="" height="{{$svgSize+10}}" width="{{$svgSize+10}}">
                <circle data-v-4d45322b="" stroke="lightgray" stroke-dasharray="414.6902302738527 414.6902302738527"
                        stroke-width="7" fill="transparent" r="{{($svgSize-18) / 2}}" cx="{{($svgSize+10) / 2}}" cy="{{($svgSize+10) / 2}}" style="stroke-dashoffset: 0px;"></circle>
            </svg>
        <!-- ACTUAL LEVEL -->
            <svg
                height="{{$svgSize+10}}"
                width="{{$svgSize+10}}"
            >
                <circle
                    stroke="{{$actualColor}}"
                    stroke-dasharray="{{$actualDashOffset}}"
{{--                    style="stroke-dashoffset: {{$actualDashOffset}};"--}}
                    stroke-width="7"
                    fill="transparent"
                    r="{{($svgSize-18) / 2}}"
                    cx="{{($svgSize+10) / 2}}"
                    cy="{{($svgSize+10) / 2}}"
                />
            </svg>
        </div>
    @endif
</div>
