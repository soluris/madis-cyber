<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dossier cybersécurité</title>

    <!-- Latest compiled and minified CSS -->
    <!-- CSS only -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">    <!-- JavaScript Bundle with Popper -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/solid.min.css"/>
    <script>
        @include('pdf.dossierCyberSecurite.chart-bundle')
    </script>

    <script>
        // wkhtmltopdf 0.12.5 crash fix.
        // https://github.com/wkhtmltopdf/wkhtmltopdf/issues/3242#issuecomment-518099192
        'use strict';
        (function(setLineDash) {
            CanvasRenderingContext2D.prototype.setLineDash = function() {
                if(!arguments[0].length){
                    arguments[0] = [1,0];
                }
                // Now, call the original method
                return setLineDash.apply(this, arguments);
            };
        })(CanvasRenderingContext2D.prototype.setLineDash);
        Function.prototype.bind = Function.prototype.bind || function (thisp) {
            var fn = this;
            return function () {
                return fn.apply(thisp, arguments);
            };
        };
    </script>


    <style>
        body {
            padding: 4em;
            text-align: justify;
        }
        .table_actions {
            border-left: 0.01em solid #ccc;
            border-right: 0;
            border-top: 0.01em solid #ccc;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table_actions td,
        .table_actions th {
            border-left: 0;
            border-right: 0.01em solid #ccc;
            border-top: 0;
            border-bottom: 0.01em solid #ccc;
        }

        .page-break {
            page-break-before: always;
            height: 3em;
        }

        .card-primary {
            background-color: #395B64;
            color: white;
        }
        ol {
            margin-right: 0;
            padding-right: 0;
        }
        ol li {
            margin-right: 0;
            padding-right: 0;
            clear:both;
        }

        header {
            position: fixed;
            top: -40px;
            height: 40px;
            font-size: small;

            /** Extra personal styles **/
            color: darkgrey;
            text-align: left;
        }

        footer {
            position: absolute;
            bottom: 0px;
            height: 40px;
            font-size: small;
            color: darkgrey;
        }
        .value-icon {
            font-size: 1.5em;
            color: #666666;
        }
        svg {
            position: absolute;
            left: 0;
            right: 0;
            margin: auto;
        }
        .reportGraph {
            width:900px;
            clear:both;
            page-break-inside: avoid;
            margin: 0 auto;
            padding-top: 2em;
        }
         .custom-crown {
             top: 30%;
         }
        .custom-crown .fa-crown {
            font-size: 80px;
            color: rgba(255,255,255, 0.1);
            z-index: 1;
            position: absolute;
            left:0;
            width:100%;
        }
        .custom-crown span {
            color: white !important;
        }
    </style>
</head>
<body>

<div class="WordSection1">
    {{-- 1.	Méthodologie --}}
    <h3>1. Méthodologie</h3><br>
    <p>Le dossier cybersécurité de {{ $organization['name'] }} (nom de la collectivité) utilise la méthode Cybersécurité Mutualisée des Territoires (CMT) qui permet aux communes de moins de cinq mille habitants d’évaluer et de développer leur maturité en matière de cybersécurité.<br>
        La méthode CMT s’adresse aux élus et décideurs des collectivités territoriales avec pour objectif de leur permettre de mieux appréhender le phénomène des cybermenaces et de leur permettre d’agir simplement et efficacement pour contrer les dangers du cyberespace.
    </p>
    <p>La méthodologie CMT est adossée/couplée au logiciel&nbsp;<i>&nbsp;Madis cyber</i> qui permet de garantir le cadre méthodologique défini en quatre ateliers :</p>
    <p style="margin-left: 80px;">
        Atelier A : Votre perception du danger cyber<br>
        Atelier B : Vos mesures de protection<br>
        Atelier C : Votre plan d’action priorisé<br>
        Atelier D : Votre maturité à la cybersécurité<br><br>
    </p>
    <p>Ce dossier est produit par le logiciel&nbsp;<i>&nbsp;Madis cyber</i>.</p>
    <div class="page-break"></div>

    {{-- 2.	Analyse des risques --}}
    <h3>2. Analyse des risques</h3>
    <h5 style="margin-left: 25px">2.1 - Atelier A: Votre perception du danger cyber</h5>
    <p>A travers le premier atelier, le référent sécurité de {{ $organization['name'] }} prend connaissance des dangers auxquels sont exposés les collectivités de moins de 5 000 habitants puis il est guidé par l’outil&nbsp;<i>&nbsp;Madis cyber</i> pour transformer sa vision du sujet en une évaluation utilisable dans une analyse des risques.<br>
        En synthèse, la perception du danger cyber qui pèse sur {{ $organization['name'] }} est la suivante :
    </p>
    <div class="card card-info mt-4">
        <div class="card-header py-1">
            <div class="card-title">
                Niveau d'exposition aux dangers
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Dangers</th>
                    <th>Niveau de dangers</th>
                </tr>
                </thead>
                @if ($evaluation->dangerLevels)
                    <tbody>
                    @foreach($evaluation->dangerLevels->sortBy('value') as $dangerLevel )
                        <tr>
                            <td> {{$dangerLevel->danger['name']}} {{$dangerLevel->level['value']}}</td>
                            <td><span class="badge" style="background-color: {{$dangerLevel->level['color']}};padding: 0.5em 1em;border-radius: 0.25em; @if ($dangerLevel->level['name'] === 'Limité') color: black; @else color: white; @endif">{{$dangerLevel->level['name']}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
    {{-- todo reprendre le graphique généré dans la synthèse --}}
    <div class="card card-info mt-4" style="font-size: small">
        <div class="card-header py-1">
            <div class="card-title">
                Niveau de danger
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Niveau de danger</th>
                    <th>Description</th>
                </tr>
                </thead>
                @if ($legendLevels)
                    <tbody>
                    @foreach($legendLevels as $level )
                        <tr>
                            <td><span class="badge" style="background-color: {{$level['color']}}; @if($level['name'] === 'Limité') color:black @else color:white; @endif;padding: 0.5em 1em;border-radius: 0.25em">{{$level['name']}}</span></td>
                            <td> {{$level['description']}} </td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
    <div class="page-break"></div>

    <h4 style="margin-left: 25px">2.2 - Analyse des risques cyber</h4>
    <br>
    <p>A travers le deuxième atelier, le référent sécurité de {{$organization['name']}} est guidé afin d’identifier l’ensemble des mesures de sécurité déjà mises en œuvre au sein de sa collectivité. Cela lui permet ensuite d’obtenir une évaluation de la vraisemblance des scénarios d’attaques les plus fréquents dans les collectivités de moins de 5 000 habitants et de connaître son niveau d’exposition.<br>
        En synthèse, l’analyse des risques cyber donne les résultats illustrés ci-dessous :
    </p>
    <p>On peut évaluer la maturité des mesures de sécurité mises en place actuellement telle que : </p>

    {{-- graphe mesures de securité --}}
    @foreach ($evaluation->measureLevels as $k => $action)
        <div style="padding-top: 2em;padding-left: 10px;border: none;width: 25%;float:left">
            <div style="border: 1px solid #4281A4;padding:0.5em 1em;">
                <p style="text-align: center;font-weight: bolder;height: 50px">
                    {{$action->measure['short_name']}}
                </p>
                @include('pdf.dossierCyberSecurite.measure_circle')
            </div>
        </div>
    @endforeach
    <br>


    {{-- todo mettre les graphes : surface de mise en œuvre des mesures de protection, mesures de sécurité mises en place--}}
    <div class="reportGraph">
        <canvas id="measuresGraph" style="width: 900px; height: 500px" width="1800" height="1000"></canvas>
    </div>
    <div class="page-break"></div>

    <p>Au regard des mesures de sécurité déjà en place, nous estimons que les attaquants peuvent exploiter les scénarios d’attaques suivants :</p>
    {{-- todo mettre graphe + legende couleur--}}
    <div class="reportGraph">
        <canvas id="attackGraph" style="width: 900px; height: 500px" width="1800" height="1000"></canvas>
    </div>
    <br>
    <p>L’exposition aux principaux dangers identifiés dans le domaine de la cybersécurité concernant les collectivités est donc le suivant (cf. niveau d’exposition au risque cyber)</p>
    {{-- todo mettre graphe + legende --}}
    <div class="reportGraph">
        <canvas id="expositionGraph" style="width: 900px; height: 500px" width="1800" height="1000"></canvas>
    </div>
    <div class="page-break"></div>

    <h4 style="margin-left: 25px">2.3 - Maturité à la cybersécurité</h4>
    <br>
    <p>L’évolution de la maturité cyber permet d’évaluer non seulement la capacité de la collectivité à mettre en œuvre le plan d’action cyber mais aussi d’assurer que l’utilisation de Madis cyber apporte un réel bénéfice aux acteurs du plan d’action.<br>
        La maturité en matière cybersécurité est la suivante.
    </p>
    {{-- todo mettre graphe maturité cyber + legende + indice de maturité --}}
    <div class="reportGraph">
        <canvas id="maturityGraph" style="width: 900px; height: 500px" width="1800" height="1000"></canvas>
    </div>
    <p>L’indice synthétique de maturité en date du {{ $date }} est de&nbsp;<b>&nbsp;{{ number_format(collect($maturityData['data'])->avg(), 1, ',') }}. (moyenne arrondie inf 0,1 des axes)</b></p>
    <div class="page-break"></div>

    <h3>3. Plan d'action</h3>
    <br>
    <p>{{ $organization['name'] }} a arbitré sur la base des préconisations de cybersécurité un plan d’action réaliste et pragmatiques adapté à son contexte. Chaque action est assignée et planifiée afin de faciliter le suivi et la mise en œuvre. </p>
    <h5 style="margin-left: 25px">3.1 - Liste des actions planifiées.</h5>
    <p>{{ $organization['name'] }} s’engage à mettre en œuvre les actions suivantes :</p>
    <div style="font-size: 0.8em">
        <table class="table table_actions table-bordered" style="border:1px solid #DDD; border-collapse: collapse">
            <thead>
            <tr>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Nom court</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Titre du niveau</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Date planifiée</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Personne en charge</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($actions as $action)
                <tr>
                    <td style="vertical-align: center;border:1px solid #DDD">
                        {{$action->measure['short_name']}}
                    </td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{$action->measure['level'.$action->expected_level.'_preconized_label']}}</td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{date('d/m/Y',strtotime($action->end_date))}}</td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{$action->manager}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <p>Les actions identifiées doivent permettre de réduire significativement le niveau d’exposition aux risques cyber.<br><br>
        En conclusion, voici les gains attendus sur les risques présents avec la réalisation du plan d’action.
    </p>
    {{-- todo mettre le graphe niveau d'expo au risque cyber--}}
    <div class="reportGraph">
        <canvas id="evolutionGraph" style="width: 900px; height: 500px" width="1800" height="1000"></canvas>
    </div>

    <div class="page-break"></div>
    <h5 style="margin-left: 25px">3.2 - Liste des risques cyber résiduels.</h5>
    <br>
    <p>Les risques cyber résiduels sont les risques pour lesquels vous n’avez pas mis en place de mesures de protection :</p>
    @foreach($residualRisks as $residual)
    <p style="margin-left: 25px">•	{{$residual->name}}</p>
    @endforeach
    @if (count($residualRisks) === 0)
        <p>Vous n'avez aucun risque résiduel</p>
    @endif
    <div class="page-break"></div>


    {{-- 4.	Politique de cybersécurité --}}
    <h3>4. Politique de cybersécurité</h3>
    <p>La politique de sécurité permet de visualiser quelles sont les mesures de sécurité mises en œuvre au sein de {{ $organization['name'] }}.</p>
    @foreach ($evaluation->measureLevels as $k => $measure)
        <div style="page-break-inside: avoid;padding-top: 2em;margin-bottom:-1em">
        @if ($measure->expected_level === null || $measure->expected_level === 0)
            @if($measure->actual_level === 0 )
                <h5 style="margin-left: 25px">4.{{ $k+1 }} - {{$measure->measure['short_name']}} [0]</h5>
                <p><i>La collectivité ne dispose pas d’une politique de sécurité établie dans ce domaine.</i></p>
            @else
                <h5 style="margin-left: 25px">4.{{ $k+1 }} - {{$measure->measure['short_name']}} [{{ $measure->actual_level }}]</h5>
                <p>
                    @for ($i = 1; $i < ($measure->actual_level + 1); $i++)
                        {{$measure->measure['level'.$i.'_actual_label']}}<br>
                    @endfor
                </p>
                <p style="margin-left: 50px;"><i><u>Statut</u> : Appliqué</i></p><br>
            @endif
        @else
            <h5 style="margin-left: 25px">4.{{ $k+1 }} - {{$measure->measure['short_name']}} [{{ $measure->expected_level }}]</h5>
            <p>
                @for ($i = 1; $i < ($measure->expected_level + 1); $i++)
                    {{$measure->measure['level'.$i.'_actual_label']}}<br>
                @endfor
            </p>
                <p style="margin-left: 50px;"><i><u>Statut</u> : Applicable {{ \Carbon\Carbon::parse($measure->end_date)->format('d/m/Y') }}</i></p><br>
        @endif
        </div>
    @endforeach
    <div class="page-break"></div>

    {{-- Lettre de mission --}}
    <h3>5. Lettre de mission du référent cybersécurité</h3>
    <p>Le référent cybersécurité est la personne désignée par le représentant de {{ $organization['name'] }} comme étant responsable de la démarche d’amélioration de la cybersécurité.</p>
    {{-- Mettre le texte correspondant à la mission  --}}
    <p><b>Missions du référent cybersécurité :</b></p>
    <p>{!! $organization['bilan1'] !!}</p>

    <p><b>Ressources annuelles recommandées</b><br/>
        Le référent cybersécurité dispose au moins de 10 jours alloués annuellement.</p>
    <div class="page-break"></div>

    <h3>6.	Responsabilités du RSSI mutualisé</h3>
    <p>Le RSSI mutualisé est le dispositif mis en œuvre par votre OPSN pour vous assister dans la démarche d’amélioration de votre cybersécurité.</p>

    <p><b>Missions du RSSI mutualisé :</b></p>
    <p>{!! $organization['bilan2'] !!}</p>
    <br>
    <div class="page-break"></div>

    <h3>Annexe 1 – Propriétés et exigences attachées la méthode</h3>
    <p><i>« Par méthode, j’entends des règles certaines et aisées, grâce auxquelles tous ceux qui les auront exactement observées, n’admettront jamais rien de faux pour vrai, et sans se fatiguer l’esprit en efforts inutiles, mais en augmentant toujours [comme] par degrés leur science, parviendront à la connaissance vraie de toutes les choses dont [leur esprit] sera capable » René DESCARTES</i></p>
    <br>
    <p><b>Elle repose sur des fondamentaux :</b></p>
    <p style="margin-left: 60px">Transparence de la démarche produisant un résultat incontestable (Story board)<br>
       Adhérence aux standards de sécurité des acteurs de référence ACYMA, ANSSI, CNIL (Cross référence avec EBIOS RM)
    </p>
    <p><b>Elle a pour but d’augmenter massivement et rapidement la protection des petites collectivités contre les cyberattaques et, pour ces raisons, elle est fondée sur 2 principes dont on ne peut s’écarter :</b></p>
    <p style="margin-left: 60px">Contextualisation aux communes de moins de 5000 habitants et pas plus<br>
        Dynamique collective au travers d’ateliers territoriaux regroupant des décideurs
    </p>
    <p><b>Elle répond à trois objectifs opérationnels de ces collectivités :</b></p>
    <p style="margin-left: 60px">Comprendre la cybersécurité et agir<br>
        Maintenir la vigilance<br>
        Se positionner en comparaison avec les autres collectivités
    </p>
    <p><b>Elle est ancrée dans une logique d’aide à la décision</b></p>
    <p style="margin-left: 60px">Production d’indicateurs visuels simple à comprendre<br>
        Le décideur est en maîtrise de ses choix
    </p>
    <p><b>Elle est définie dans un système de management qui se décompose en 5 processus</b></p>
    <p style="margin-left: 80px">•	Processus Comprendre et agir<br>
        •	Processus Maintenir la vigilance<br>
        •	Processus Gérer les demandes<br>
        •	Processus Informer<br>
        •	Processus Piloter<br>
    </p>
    <p><b>Elle s’appuie sur un logiciel qui garantit la conformité à la méthode, optimise le déroulement des sessions collectives, gère les cycles opérationnels pour permettre aux collectivités de s’améliorer dans le temps.</b></p>
    <div class="page-break"></div>

    <h3>Annexe 2 – Résultats des ateliers</h3>
    <br>
    <h5 style="margin-left: 25px">2.1 - Atelier A : Perception du danger cyber</h5>
    <br>
    <p>En synthèse, la perception du danger cyber qui pèse sur la collectivité est la suivante :</p>
    <br>
    <div class="card card-info mt-4">
        <div class="card-header py-1">
            <div class="card-title">
                Niveau d'exposition aux dangers
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Dangers</th>
                    <th>Niveau de dangers</th>
                </tr>
                </thead>
                @if ($evaluation->dangerLevels)
                    <tbody>
                    @foreach($evaluation->dangerLevels as $dangerLevel )
                        <tr>
                            <td> {{$dangerLevel->danger['name']}} </td>
                            <td><span class="badge" style="background-color: {{$dangerLevel->level['color']}};padding: 0.5em 1em;border-radius: 0.25em; @if ($dangerLevel->level['value'] == 50) color: black; @else color: white; @endif">{{$dangerLevel->level['name']}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>
    <div class="page-break"></div>

    <h5 style="margin-left: 25px">2.2 - Atelier B : Mesures de protection</h5>
    <br>
    <p>En synthèse, les mesures de sécurité opérationnelles dans la collectivité sont les suivantes :</p>

    <table style="width:100%;border:none">
        <?php $size = 80; ?>
        @foreach($evaluation->measureLevels as $action)
            <tr>
                <td>
                    <table style="page-break-inside: avoid;width:100%;border: 1px solid #4281A4;border-collapse: collapse">
                        <tr>
                            <td style="padding: 0 20px;border: 1px solid #4281A4;width:80%">
                                <b>{{$action->measure->short_name}}</b>
                            </td>
                            <td style="padding: 0 20px;border: 1px solid #4281A4;width:20%">
                                @include('pdf.dossierCyberSecurite.measure_circle')
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 10px 20px;border: 1px solid #4281A4;">
                                {{$action->measure->name}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        @endforeach
    </table>

    <div class="page-break"></div>

    <h3>Annexe 3 – Métriques</h3>
    <br>
    <table class="table table-bordered" border="1px solid blue" style="text-align: center;">
        <thead style="background-color: #2a63b6;color:white;">
        <tr>
            <th colspan="3">PERCEPTION DU DANGER</th>
        </tr>
        <tr>
            <th>Echelle</th>
            <th>Poids</th>
            <th>Critères d'évaluation</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><b>Critique</b></td>
            <td>10</td>
            <td>La collectivité sera fortement impactée; la résolution prendra plusieurs mois; les séquelles seront perceptibles sur plusieurs années.</td>
        </tr>
        <tr>
            <td><b>Important</b></td>
            <td>3</td>
            <td>La collectivité sera fortement impactée; la résolution prendra plusieurs mois; les séquelles seront perceptibles sur plusieurs mois.</td>
        </tr>
        <tr>
            <td><b>Limité</b></td>
            <td>0,9</td>
            <td>La collectivité sera faiblement impactée; la résolution prendra plusieurs mois; aucunes séquelles ne seront perceptibles.</td>
        </tr>
        <tr>
            <td><b>Négligeable</b></td>
            <td>0,1</td>
            <td>La collectivité sera faiblement impactée; la résolution prendra quelques jours; aucunes séquelles ne seront perceptibles.</td>
        </tr>



        </tbody>
    </table>

    <div class="page-break"></div>

    <h3>Annexe 4 – Glossaire</h3>
    <p><b>Danger</b><br>
    Elément, personne, groupe de personne ou organisation susceptible d’engendrer un risque (assimilé au terme « source de risque » de la méthode EBIOS RM).
    </p>
    <p><b>Cyberespace</b><br>
    Espace de communication constitué par l’interconnexion mondiale d’équipements de traitement automatisé de données numériques. (ANSSI)
    </p><p><b>Cybersécurité</b><br>
    État recherché pour un système d’information lui permettant de résister à des événements issus du cyberespace susceptibles de compromettre la disponibilité, l’intégrité ou la confidentialité des données stockées, traitées ou transmises et des services connexes que ces systèmes offrent ou qu’ils rendent accessibles. La cybersécurité fait appel à des techniques de sécurité des systèmes d’information et s’appuie sur la lutte contre la cybercriminalité et sur la mise en place d’une cyberdéfense. (ANSSI)
    </p><p><b>Risque</b><br>
    Danger ou péril dans lequel l’idée de hasard est accusée mais avec la perspective d’un quelconque avantage possible (dictionnaire de la langue philosophique)
    Un risque est associé à la possibilité qu’un danger connu exploite un scénario d’attaque connu sur un composant et nuise à l’organisme (Inspiré de la norme ISO 27000).
    </p><p><b>Risque cyber</b><br>
    Risque portant sur le système d’information ayant pour origine le cyberespace.
    </p><p><b>Système d’Information</b><br>
    Ensemble des ressources destinées à collecter, classifier, stocker, gérer, diffuser les informations au sein d’une organisation.
    </p>

</div>
@include('pdf.dossierCyberSecurite.graphs-js')
</body>
</html>
