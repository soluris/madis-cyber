<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<style>
    body {
        font-family: sans-serif;
    }
    hr {
        color: black;
        background: black;
        height: 1px;
        border:none;
    }

</style>
<body>
<header>
    <p style="margin-bottom: 0px">Dossier cybersécurité - {{ $organization['name'] }} {{ $evaluation->updated_at->format('d/m/Y') }}</p>
    <hr>
</header>
</body>
</html>
