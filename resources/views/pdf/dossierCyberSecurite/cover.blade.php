<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>

<style>
    body {
        font-family: sans-serif;
        background: url({{ asset('/images/rectangle.png') }});
        background-repeat: repeat-y;
        margin:0;
        padding:0;
        height: 950px;
        padding-top: 300px;
    }
</style>
<body>
{{-- Page de garde --}}

    <h1 style="text-align:center;"><b>{{ $organization['name'] }}</b><br></h1>
    <div style="text-align:center;margin-top: 120px">
        <img
            src="{{ asset('/images/logo_madis_cyber.png')}}"
            width="25%"
            alt="Logo Madis"
        >
    </div>

    <h3 style="text-align:center;margin-top: 50px;margin-left: 3%;">Dossier cybersécurité - {{ $date }}</h3>
<p style="margin-left: 160px;margin-top:20%">Responsable de l'organisation : {{ $organization->referent->firstname }} {{ $organization->referent->lastname }}</p>
<p style="margin-left: 160px;">Responsable cybersécurité : {{ $organization->referentCyber->firstname }} {{ $organization->referentCyber->lastname }}</p>
<p style="margin-left: 160px;">Responsable élu cybersécurité : {{ $organization->referentElu->firstname }} {{ $organization->referentElu->lastname }}</p>

<p style="margin-left: 160px">Version du référentiel :  {{ $referentiel }}</p>
</body>
</html>
