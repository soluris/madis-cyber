
<script type="text/javascript">
    const measuresData = {!! json_encode($measuresData) !!};
    const attackData = {!! json_encode($attackData) !!};
    const expositionData = {!! json_encode($expositionData) !!};

    expositionData.labels = expositionData.labels.map(function(l) {
        return l.match(/.{1,40}(\s|$)/g)
    })

    const maturityData = {!! json_encode($maturityData) !!};
    const risksData = {!! json_encode($risksData) !!};
    risksData.labels = risksData.labels.map(function(l) {
        return l.match(/.{1,40}(\s|$)/g)
    })
    const futureRisksData = {!! json_encode($futureRisksData) !!};
    futureRisksData.labels = futureRisksData.labels.map(function(l) {
        return l.match(/.{1,40}(\s|$)/g)
    })

    const radarOptions = {
        animation: false,
        responsive: false,
        legend: {
            display: false,
        },
        scale: {
            angleLines: {
                lineWidth: 2,
            },
            gridLines: {
                lineWidth: 2,
            },
            pointLabels: {
                display: true,
                fontColor: 'black',
                fontSize: 25,
            },
            beginAtZero: true,
            animate: false,
            ticks: {
                color: '#000',
                backdropColor: 'rgba(255,255,255, 0.8)',
                fontSize: 25,
                z: 10,
                min:-1,
                max: 3,
                beginAtZero: false,
                stepSize: 1,
            },
            grid: {
                z: 9,
            }
        },
    }

    const barOptions = {
        legend: {
            display:false,
        },
        animation:false,
        responsive: false,
        scales: {
            xAxes: [{
                gridLines: {
                    lineWidth: 2,
                },
                beginAtZero: true,
                animate: false,
                ticks: {
                    color: '#000',
                    backdropColor: 'rgba(255,255,255, 0.8)',
                    fontSize: 25,
                    z: 10,
                },
                grid: {
                    z: 9,
                }
            }],
            yAxes: [{
                gridLines: {
                    lineWidth: 2,
                },
                beginAtZero: true,
                animate: false,
                ticks: {
                    min: 0,
                    max: 100,
                    color: '#000',
                    backdropColor: 'rgba(255,255,255, 0.8)',
                    fontSize: 25,
                    z: 10,
                },
                grid: {
                    z: 9,
                }
            }],
        }
    };

    window.onload=function() {
        // Chart.defaults.global.defaultFontSize = 20;
        const maturityctx = document.getElementById('maturityGraph').getContext('2d');
        const maturityChart = new Chart(maturityctx, {
            type: 'radar',
            data: {
                labels: maturityData.labels,
                datasets: [{
                    label: '',
                    data: maturityData.data,
                    backgroundColor: 'rgba(66,129,164,0.6)',
                    borderColor: '#4281A4',
                    pointBackgroundColor: '#4281A4',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#4281A4',
                    pointRadius: 0,
                }]
            },
            options: radarOptions,
        });

        const ctx = document.getElementById('measuresGraph').getContext('2d');
        const measureOptions = radarOptions;

        measureOptions.scale.ticks.max = 3
        const measuresChart = new Chart(ctx, {
            type: 'radar',
            data: {
                labels: attackData.labels,
                datasets: [{
                    label: 'Mesures',
                    data: measuresData.data,
                    backgroundColor: 'rgba(66,129,164,0.6)',
                    borderColor: '#4281A4',
                    pointBackgroundColor: '#4281A4',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: '#4281A4',
                    pointRadius: 0,
                }]
            },
            options: measureOptions,
        });

        const polarOptions = radarOptions;
        polarOptions.scale.ticks.max = 100
        polarOptions.scale.ticks.stepSize = 10

        polarOptions.startAngle = -125*Math.PI/180

        const attackctx = document.getElementById('attackGraph').getContext('2d');
        // const colors = attackData.labels.map((c, i) => {
        //     return 'hsla('+ 360*i/attackData.labels.length +', 100%, 60%, 0.6)'
        // })
        const attackChart = new Chart(attackctx, {
            type: 'polarArea',
            data: {
                labels: attackData.labels,
                datasets: [{
                    label: '',
                    data: attackData.data,
                    backgroundColor: {!!json_encode($attackColors)!!}
                }]
            },
            options: polarOptions,
        });



        const expoctx = document.getElementById('expositionGraph').getContext('2d');
        const expoChart = new Chart(expoctx, {
            type: 'bar',
            data: {
                labels: expositionData.labels,
                datasets: [{
                    label: '',
                    data: expositionData.data,
                    backgroundColor: '#4281A4',
                    borderColor: '#4281A4',
                }]
            },
            options: barOptions,
        });

        const evolctx = document.getElementById('evolutionGraph').getContext('2d');
        const evolChart = new Chart(evolctx, {
            type: 'bar',
            data: {
                labels: risksData.labels,
                datasets: [
                    {
                        label: 'Risques actuels',
                        data: risksData.data,
                        backgroundColor: 'black',
                        borderColor: 'black',
                    },
                    {
                        label: 'Risques prévus',
                        data: futureRisksData.data,
                        backgroundColor: '#4281A4',
                        borderColor: '#4281A4',
                    },
                ]
            },
            options: barOptions,
        });
    }


</script>
