<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Politique de sécurité</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/solid.min.css">

    <style>
        body {
            padding: 4em;
            text-align: justify;
        }
        .table_actions {
            border-left: 0.01em solid #ccc;
            border-right: 0;
            border-top: 0.01em solid #ccc;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table_actions td,
        .table_actions th {
            border-left: 0;
            border-right: 0.01em solid #ccc;
            border-top: 0;
            border-bottom: 0.01em solid #ccc;
        }

        .page-break {
            page-break-before: always;
            height: 3em;
        }

        .card-primary {
            background-color: #395B64;
            color: white;
        }
        ol {
            margin-right: 0;
            padding-right: 0;
        }
        ol li {
            margin-right: 0;
            padding-right: 0;
            clear:both;
        }

        header {
            position: fixed;
            top: -40px;
            height: 40px;
            font-size: small;

            /** Extra personal styles **/
            color: darkgrey;
            text-align: left;
        }

        footer {
            position: absolute;
            bottom: 0px;
            height: 40px;
            font-size: small;
            color: darkgrey;
        }
        .value-icon {
            font-size: 1.5em;
            color: #666666;
        }
        svg {
            position: absolute;
            left: 0;
            right: 0;
            margin: auto;
        }

    </style>
</head>
<body>

<div class="WordSection1">
    {{-- Préambule --}}
    <h3>1. Préambule</h3><br>
    <p>Au travers de la méthode CMT (cybersécurité Mutualisée des Territoires) avec le logiciel&nbsp;<i>&nbsp;Madis cyber</i> proposé par OPSN, {{ $organization['name'] }} a réalisé une analyse des risques cyber puis s’est dotée d’un plan d’action visant à réduire l’exposition aux risques cyber.<br/><br/>
        L’ensemble de la démarche est synthétisé dans un document intitulé&nbsp;<i>&nbsp;Dossier cybersécurité</i>. Dans ce dossier, une politique de sécurité est aussi proposée à {{ $organization['name'] }}.<br/><br/>
        Le présent document fait partie du&nbsp;<i>&nbsp;Dossier cybersécurité</i> et décrit la politique de sécurité cyber que {{ $organization['name'] }} souhaite appliquer.<br/><br/>
        La politique de sécurité comprend deux types de mesures :</p>
    <p style="margin-left: 25px;">
        •	Statut appliqué : les mesures sont déjà en place;<br>
        •	Statut applicable date : les mesures font partie du plan d’action en cours.<br>
    </p>
    <p>
        Cette politique peut être transmise aux prestataires afin qu’ils s’engagent à respecter au moins les mêmes mesures pour ne pas dégrader le niveau de sécurité de {{ $organization['name'] }}.<br><br>
        Cette politique est produit par le logiciel&nbsp;<i>&nbsp;Madis cyber</i>.
    </p>
    <div class="page-break"></div>

    {{-- 2.	Politique de cybersécurité --}}
    <h3>2. Politique de cybersécurité</h3>
    <p>La politique de sécurité permet de visualiser quelles sont les mesures de sécurité mises en œuvre au sein de {{ $organization['name'] }}.</p>
    @foreach ($evaluation->measureLevels as $k => $measure)
        <div style="page-break-inside: avoid">
        @if ($measure->expected_level === null || $measure->expected_level === 0)
                @if($measure->actual_level === 0 )
                    <h5 style="margin-left: 25px">2.{{ $k+1 }} - {{$measure->measure['short_name']}}</h5>

                    <p><i>La collectivité ne dispose pas d’une politique de sécurité établie dans ce domaine.</i></p>
                @else
                    <h5 style="margin-left: 25px">2.{{ $k+1 }} - {{$measure->measure['short_name']}} [{{ $measure->actual_level }}]</h5>
                    <p>
                    @for ($i = 1; $i < ($measure->actual_level + 1); $i++)
                        {{$measure->measure['level'.$i.'_actual_label']}}<br>
                    @endfor
                </p>
                <p style="margin-left: 50px;"><i><u>Statut</u> : Appliqué</i></p><br>
            @endif
        @else
            <h5 style="margin-left: 25px">2.{{ $k+1 }} - {{$measure->measure['short_name']}} [{{ $measure->expected_level }}]</h5>
            <p>
                @for ($i = 1; $i < ($measure->expected_level + 1); $i++)
                    {{$measure->measure['level'.$i.'_actual_label']}}<br>
                @endfor
            </p>
                <p style="margin-left: 50px;"><i><u>Statut</u> : Applicable {{ \Carbon\Carbon::parse($measure->end_date)->format('d/m/Y') }}</i></p><br>
        @endif
        </div>
    @endforeach
    <div class="page-break"></div>

    {{-- Lettre de mission --}}
    <h3>3.	Lettre de mission du référent cybersécurité</h3>
    <p>Le référent cybersécurité est la personne désignée par le représentant de {{ $organization['name'] }} comme étant responsable de la démarche d’amélioration de la cybersécurité.</p>
    {{-- Mettre le texte correspondant à la mission  --}}
    <p><b>Missions du référent cybersécurité :</b></p>
    <p>{!! $organization['bilan1'] !!}</p>

    <p><b>Ressources annuelles recommandées</b><br/>
        Le référent cybersécurité dispose au moins de 10 jours alloués annuellement.</p>

    <div class="page-break"></div>

    <h3>4.	Responsabilités du RSSI mutualisé</h3>
    <p>Le RSSI mutualisé est le dispositif mis en œuvre par votre OPSN pour vous assister dans la démarche d’amélioration de votre cybersécurité.</p>
    <p><b>Missions du RSSI mutualisé :</b></p>
    <p>{!! $organization['bilan2'] !!}</p>
    <br>
</div>

</body>
</html>
