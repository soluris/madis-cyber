<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script>
        function substitutePdfVariables() {

            function getParameterByName(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }

            function substitute(name) {
                var value = getParameterByName(name);
                var elements = document.getElementsByClassName(name);

                for (var i = 0; elements && i < elements.length; i++) {
                    elements[i].textContent = value;
                }
            }

            ['frompage', 'topage', 'page', 'webpage', 'section', 'subsection', 'subsubsection']
                .forEach(function(param) {
                    substitute(param);
                });
        }
    </script>
    <title></title>
</head>

<style>
    body {
        font-family: sans-serif;
    }
    hr {
        color: black;
        background: black;
        height: 1px;
        border:none;
    }

</style>
<body onload="substitutePdfVariables()">
<footer>
    <hr>
    <p style="margin-bottom: 0;text-align: center">
        <b><span class="page"></span> | <span class="topage"></span></b>
    </p>

</footer>
<div style="height: 25px"></div>
</body>
</html>
