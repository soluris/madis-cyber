<?php
if (!function_exists('getProgressFromLevel')) {
    function getProgressFromLevel($level)
    {
        return match ($level) {
            0 => 15,
            1 => 33,
            2 => 66,
            3 => 100,
            default => 0,
        };
    }
}
$actualColor = 'transparent';
switch ($action->actual_level) {
    case 0:
        $actualColor = '#454545';
        break;
    case 1:
        $actualColor = '#D63F49';
        break;
    case 2:
        $actualColor = '#FFC107';
        break;
    case 3:
        $actualColor = '#468355';
        break;
}
$expectedColor = 'transparent';
switch ($action->expected_level) {
    case 0:
        $expectedColor = '#454545';
        break;
    case 1:
        $expectedColor = '#D63F49';
        break;
    case 2:
        $expectedColor = '#FFC107';
        break;
    case 3:
        $expectedColor = '#468355';
        break;
}
$expectedDashOffset = (new \App\Helpers\SVG(getProgressFromLevel($action->expected_level), 100))->dashArray();
$actualDashOffset = (new \App\Helpers\SVG(getProgressFromLevel($action->actual_level), 100))->dashArray();
?>



<div style="height: 180px;position:relative">
    <p style="text-align: center;z-index: 1;position: absolute;top: 25%;left: 0;right: 0;" class="@if($action->actual_level === 3) custom-crown @endif">
        @if ($action->actual_level === 3)
            <i class="fas fa-crown"></i>
        @endif

        <span class="@if($action->actual_level === 3) text-white @else text-muted @endif  text-uppercase small">Niveau</span>  <br/>
        <span class="h1">{{$action->actual_level}}</span>
        @if ($action->expected_level > $action->actual_level)
            <span>
                <i id="circle-arrow" class="fas fa-angle-double-right"></i>
                <span class="h1">{{$action->expected_level}}</span>
            </span>
        @endif
    </p>
    @if($action->actual_level === 3)
{{--        Show crown for level 3--}}
        <div style="position: relative">
            <svg xmlns="http://www.w3.org/2000/svg" width="150" height="150" fill="#468355" viewBox="0 0 16 16">
                <path d="m8 1.288 6.842 5.56L12.267 15H3.733L1.158 6.847 8 1.288zM16 6.5 8 0 0 6.5 3 16h10l3-9.5z"/>
            </svg>

            <svg
                xmlns="http://www.w3.org/2000/svg" width="110"
                height="110" fill="#468355"
                viewBox="0 0 16 16"
                style="top: 23px">
                <path d="m8 0 8 6.5-3 9.5H3L0 6.5 8 0z"/>
            </svg>
        </div>
    @else
{{--        Show circle for other levels--}}
        <div style="position: relative;width:160px;height:160px;margin: 0 auto;">
            <!-- BASE CIRCLE -->
            <svg data-v-4d45322b="" height="160" width="160">
                <circle data-v-4d45322b="" stroke="lightgray" stroke-dasharray="414.6902302738527 414.6902302738527"
                        stroke-width="7" fill="transparent" r="66" cx="80" cy="80" style="stroke-dashoffset: 0px;"></circle>
            </svg>
            <!-- EXPECTED LEVEL -->
            <!--  -->
            @if ($action->expected_level)
                <svg
                    height="160"
                    width="160"
                >
                    <circle
                        stroke="{{$expectedColor}}"
                        stroke-dasharray="{{$expectedDashOffset}}"
{{--                        style="stroke-dashoffset: {{$expectedDashOffset}};"--}}
                        stroke-width="7"
                        fill="transparent"
                        r="66"
                        cx="80"
                        cy="80"
                    />
                </svg>
            @endif
        <!-- ACTUAL LEVEL -->
            <svg
                height="160"
                width="160"
            >
                <circle
                    stroke="{{$actualColor}}"
                    stroke-dasharray="{{$actualDashOffset}}"
{{--                    style="stroke-dashoffset: {{$actualDashOffset}};"--}}
                    stroke-width="7"
                    fill="transparent"
                    r="66"
                    cx="80"
                    cy="80"
                />
            </svg>
        </div>
    @endif
</div>
