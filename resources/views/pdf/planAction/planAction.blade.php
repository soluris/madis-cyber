<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Plan d'action</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/solid.min.css">

    <style>
        body {
            padding: 1em;
            font-family: sans-serif;
            line-height: 1.4em;
            text-align: justify;
        }
        .table_actions {
            border-left: 0.01em solid #ccc;
            border-right: 0;
            border-top: 0.01em solid #ccc;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table_actions td,
        .table_actions th {
            border-left: 0;
            border-right: 0.01em solid #ccc;
            border-top: 0;
            border-bottom: 0.01em solid #ccc;
        }

        .page-break {
            page-break-before: always;
            height: 3em;
        }

        .card-primary {
            background-color: #395B64;
            color: white;
        }
        ol {
            margin-right: 0;
            padding-right: 0;
        }
        ol li {
            margin-right: 0;
            padding-right: 0;
            clear:both;
        }

        header {
            position: fixed;
            top: -40px;
            height: 40px;
            font-size: small;

            /** Extra personal styles **/
            color: darkgrey;
            text-align: left;
        }

        footer {
            position: absolute;
            bottom: 0px;
            height: 40px;
            font-size: small;
            color: darkgrey;
        }
        .value-icon {
            font-size: 1.5em;
            color: #666666;
        }
        svg {
            position: absolute;
            left: 0;
            right: 0;
            margin: auto;
        }

    </style>
</head>
<body>

<div class="WordSection1">
    <div style="height: 3em;"></div>
    {{-- Préambule --}}
    <h3>1. Préambule</h3>
    <p>À travers la méthode CMT (cybersécurité Mutualisée des Territoires) avec le logiciel&nbsp;<i>&nbsp;Madis cyber</i> proposé par OPSN, {{ $organization['name'] }} a réalisé une analyse des risques cyber puis s’est dotée d’un plan d’action visant à réduire l’exposition aux risques cyber.<br/><br/>
        L’ensemble de la démarche que s’engage à réaliser {{ $organization['name'] }} est consolidé dans un document intitulé&nbsp;<i>&nbsp;Dossier cybersécurité</i> dans lequel apparaît le plan d’action issu de l’analyse des risques.<br/><br/>
        Le présent document décrit le plan d’action présenté dans le&nbsp;<i>&nbsp;Dossier cybersécurité</i>.<br/><br/>
        Ce plan d’action est produit par le logiciel&nbsp;<i>&nbsp;Madis cyber</i>.
    </p>
    <div class="page-break"></div>

    {{-- Plan d'action --}}
    <h3>2. Plan d'action</h3>
    <p> {{ $organization['name'] }} a arbitré sur la base des préconisations de cybersécurité un plan d’action réaliste et pragmatique adapté à son contexte. Chaque action est assignée et planifiée afin d’en faciliter la mise en œuvre et le suivi.</p>
    <h4>2.1	– Synthèse des actions planifiées</h4>
    <p>Les actions identifiées doivent permettre de réduire significativement le niveau d’exposition aux risques cyber.</p>
    <div style="font-size: 0.8em">
        <table class="table table_actions table-bordered" style="border:1px solid #DDD; border-collapse: collapse">
            <thead>
            <tr>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Nom court</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Titre du niveau</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Date planifiée</th>
                <th scope="col" style="vertical-align: center;border:1px solid #DDD">Personne en charge</th>
            </tr>
            </thead>
            <tbody>
            {{-- todo loop on actions --}}
            @foreach ($actions as $action)
                <tr>
                    <td style="vertical-align: center;border:1px solid #DDD">
                        {{$action->measure['short_name']}}
                    </td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{$action->measure['level'.$action->expected_level.'_preconized_label']}}</td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{date('d/m/Y',strtotime($action->end_date))}}</td>
                    <td style="vertical-align: center;border:1px solid #DDD">{{$action->manager}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="page-break"></div>

    <h4>2.2	– Actions détaillées</h4>
    <p>Afin d’aider à la priorisation des actions, quatre critères permettent de les décrire :</p>
    <ul>
        <li><b>Difficulté :</b> Evaluation du niveau de difficulté pour la mise en œuvre de l’action.</li>
    	<li><b>Coût :</b> Estimation budgétaire pour la mise en place de l’action.</li>
    	<li><b>Temps :</b> Estimation du temps nécessaire pour la mise en œuvre de l’action.</li>
    	<li><b>Accompagnement :</b> Besoin d’accompagnement pour assurer la mise en place de l’action</li>
    </ul>
    @foreach ($actions as $action)
        <div style="page-break-inside: avoid;padding-top: 3em;border: none;">
            <div style="border: 1px solid #4281A4;padding:0.5em 1em">
                <h5 style="text-align: center;font-weight: bolder">{{$action->measure['level'.$action->expected_level.'_preconized_label']}}</h5>

                <table class="table" style="border:none">
                    <tr>

                        <td style="width:50%;border: none;text-align: center">
                            @include('pdf.planAction.measure_circle')

                            <div style="background-color: #e3e4e7;border-radius: 6px; ">
                                <table class="table" style="border: none">
                                    <tr style="border: none">
                                        <td style="width:22%;text-align: center;border: none">
                                            <div class="value-icon">
                                                <i class="fas fa-tachometer-alt"></i>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.6em;color:#999">Difficulté :</p>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.8em;font-weight: bold;margin-bottom:0px;">
                                                    @switch(intval($action->measure['level'.$action->expected_level.'_difficulty']))
                                                        @case(1)
                                                            Simple
                                                            @break
                                                        @case(2)
                                                            Moyen
                                                            @break
                                                        @case(3)
                                                            Compliqué
                                                            @break
                                                    @endswitch
                                                </p>
                                            </div>
                                        </td>
                                        <td style="width:20%;text-align: center;border: none">
                                            <div class="value-icon">
                                                <i class="fas fa-euro-sign"></i>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.6em;color:#999">Coût :</p>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.8em;font-weight: bold;margin-bottom:0px;">{{$action->measure['level'.$action->expected_level.'_cost'] }}</p>
                                            </div>
                                        </td>
                                        <td style="width:20%;text-align: center;border: none">
                                            <div class="value-icon">
                                                <i class="fas fa-calendar-alt"></i>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.6em;color:#999">Temps :</p>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.8em;font-weight: bold;margin-bottom:0px;">{{$action->measure['level'.$action->expected_level.'_duration'] }}</p>
                                            </div>
                                        </td>
                                        <td style="width:38%;text-align: center;border: none">
                                            <div class="value-icon">
                                                <i class="fas fa-hands-helping"></i>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.6em;color:#999">Accompagnement :</p>
                                            </div>
                                            <div>
                                                <p style="font-size: 0.8em;font-weight: bold;margin-bottom:0px;">
                                                    @switch(intval($action->measure['level'.$action->expected_level.'_assistance']))
                                                        @case(1)
                                                            Simple
                                                            @break

                                                        @case(2)
                                                            Moyen
                                                            @break

                                                        @case(3)
                                                            Élevé
                                                            @break
                                                    @endswitch
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td style="font-size: 0.8em;width:50%;border:none;"><p style="text-decoration: underline;">Description :</p>
                            <p style="margin-right: 35px">{!!$action->measure['level'.$action->expected_level.'_description']!!}</p>
                            <p style="text-decoration: underline;">Préconisation :</p>
                            <p style="margin-right: 35px">{{$action->measure['level'.$action->expected_level.'_preconized_label']}}</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
    @endforeach
    <div class="page-break"></div>

    {{-- Lettre de mission --}}

    <h3>3.	Lettre de mission du référent cybersécurité</h3>
    <p>Le référent cybersécurité est la personne désignée par le représentant de {{ $organization['name'] }} comme étant responsable de la démarche d’amélioration de la cybersécurité.</p>
    {{-- Mettre le texte correspondant à la mission  --}}
    <p><b>Missions du référent cybersécurité :</b></p>
    <p>{!! $organization['bilan1'] !!}</p>

    <p><b>Ressources annuelles recommandées</b><br/>
        Le référent cybersécurité dispose au moins de 10 jours alloués annuellement.</p>

    <div class="page-break"></div>

    <h3>4.	Responsabilités du RSSI mutualisé</h3>
    <p>Le RSSI mutualisé est le dispositif mis en œuvre par votre OPSN pour vous assister dans la démarche d’amélioration de votre cybersécurité.</p>
    <p><b>Missions du RSSI mutualisé :</b></p>
    <p>{!! $organization['bilan2'] !!}</p>
    <br>

    <h3>5.	Annexe 1 : Fiches pratiques</h3>
</div>

</body>
</html>
