@extends('layouts.page_with_header')

@section('content')
    <div class="login-page">
        <form method="post" action="{{route('password_reset.send')}}">
            {!! csrf_field() !!}
            <div hidden>
                <input id="id" name="id" type="hidden" value="{{intval($id)}}">
                <input id="remember_token" name="reset_token" type="hidden" value="{{$reset_token}}">
            </div>
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    <span class="h3" style="font-weight: 300">Nouveau mot de passe</span>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="input-group mb-3" data-children-count="1">
                            <input class="form-control @error('newpassword') is-invalid @enderror" type="password" name="newpassword" id="newpassword" placeholder="Mot de passe">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                            <p style="color: red">
                                @foreach ($errors->all() as $error)
                                    <b>{{ $error }}</b><br/>
                                @endforeach
                            </p>
                        @else
                            <p style="font-size:70%;">Le mot de passe doit contenir au moins un chiffre, une lettre minuscule, une lettre majuscule, un caractère spécial et doit avoir une longueur minimale de 12 caractères.</p>
                        @endif
                    </div>
                    <div class="text-center mt-4">
                        <button class="btn btn-primary btn-flat" type="submit">Valider</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

