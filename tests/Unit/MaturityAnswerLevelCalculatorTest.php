<?php

namespace Tests\Unit;

use App\Calculator\MaturityAnswerLevelCalculator;
use App\Models\Evaluation;
use App\Models\Measure;
use App\Models\MeasureLevel;
use Carbon\Carbon;
use Tests\TestCase;

class MaturityAnswerLevelCalculatorTest extends TestCase
{
    public function testCalculateFundamentalLevelWithOneLevel()
    {
        $evaluation = new Evaluation();

        $measure = new Measure();
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 3;

        $evaluation->measureLevels->add($measureLevel);

        $res = MaturityAnswerLevelCalculator::calculateFundamentalLevel($evaluation);

        $this->assertEquals(3, $res);
    }

    public function testCalculateFundamentalLevelWithSeveralLevels()
    {
        $evaluation = new Evaluation();

        $measure = new Measure();
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 3;

        $evaluation->measureLevels->add($measureLevel);

        $measure = new Measure();
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 1;

        $evaluation->measureLevels->add($measureLevel);

        $res = MaturityAnswerLevelCalculator::calculateFundamentalLevel($evaluation);

        $this->assertEquals(2, $res);
    }

    public function testCalculateFundamentalLevelWithNonFundamentalLevels()
    {
        $evaluation = new Evaluation();

        $measure = new Measure();
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 3;

        $evaluation->measureLevels->add($measureLevel);

        $measure = new Measure();
        $measure->fundamental = false;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 1;

        $evaluation->measureLevels->add($measureLevel);

        $res = MaturityAnswerLevelCalculator::calculateFundamentalLevel($evaluation);

        $this->assertEquals(3, $res);
    }

    public function testCalculateExecutionLevelWithEverythingOK()
    {
        $evaluation = new Evaluation();
        $evaluation->updated_at = Carbon::now();

        $previousEvaluation = new Evaluation();
        $previousEvaluation->updated_at = Carbon::now()->subDays(5);

        $measure = new Measure();
        $measure->id = 1;
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->measure_id = $measure->id;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->expected_level = 3;
        $measureLevel->actual_level = 1;

        $previousEvaluation->measureLevels->add($measureLevel);

        $measureLevel->actual_level = 3;
        $evaluation->measureLevels->add($measureLevel);

        $measure = new Measure();
        $measure->id = 2;
        $measure->fundamental = false;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->measure_id = $measure->id;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 1;
        $measureLevel->expected_level = 2;

        $previousEvaluation->measureLevels->add($measureLevel);

        $measureLevel->actual_level = 2;
        $evaluation->measureLevels->add($measureLevel);

        $res = MaturityAnswerLevelCalculator::calculateExecutionLevel($evaluation, $previousEvaluation);

        $this->assertEquals(3, $res);
    }

    public function testCalculateExecutionLevelWithHalfOK()
    {
        $evaluation = new Evaluation();
        $evaluation->updated_at = Carbon::now();

        $previousEvaluation = new Evaluation();
        $previousEvaluation->updated_at = Carbon::now()->subDays(5);

        $measure = new Measure();
        $measure->id = 1;
        $measure->fundamental = true;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->measure_id = $measure->id;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->expected_level = 3;
        $measureLevel->actual_level = 1;

        $previousEvaluation->measureLevels->add($measureLevel);

        $measureLevel->actual_level = 2;
        $evaluation->measureLevels->add($measureLevel);

        $measure = new Measure();
        $measure->fundamental = false;
        $measure->id = 2;

        $measureLevel = new MeasureLevel();
        $measureLevel->measure = $measure;
        $measureLevel->measure_id = $measure->id;
        $measureLevel->evaluation = $evaluation;
        $measureLevel->actual_level = 1;
        $measureLevel->expected_level = 2;

        $previousEvaluation->measureLevels->add($measureLevel);

        $measureLevel->actual_level = 2;
        $evaluation->measureLevels->add($measureLevel);

        $res = MaturityAnswerLevelCalculator::calculateExecutionLevel($evaluation, $previousEvaluation);

        $this->assertEquals(2, $res);
    }
}
