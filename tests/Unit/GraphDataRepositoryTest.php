<?php

namespace Tests\Unit;

use App\Models\Evaluation;
use App\Models\Measure;
use App\Models\MeasureLevel;
use App\Models\Organization;
use App\Models\Territory;
use App\Repository\EvaluationRepository;
use App\Repository\GraphDataRepository;
use Carbon\Carbon;
use Tests\TestCase;

class GraphDataRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate');
    }

    public function testGetActionTerritoryGraphDataWithNoDataShouldReturnZero()
    {
        $territory = new Territory();
        $territory->name = 'test';
        $territory->save();

        $organisation = new Organization();
        $organisation->territory_id = $territory->id;
        $organisation->name = 'test org';
        $organisation->short_name = 'testorg';
        $organisation->type = 'CCAS';
        $organisation->siren = '123456';
        $organisation->active = true;
        $organisation->save();

        $evalution = new Evaluation();
        $evalution->organization_id = $organisation->id;
        $evalution->status = 2;
        $evalution->current_step = 6;
        $evalution->created_at = Carbon::now()->subDays(10);
        $evalution->updated_at = Carbon::now();
        $evalution->updated_by = 'test update';
        $evalution->author = 'test author';
        $evalution->reference = env('REFERENTIEL_VERSION');
        $evalution->save();

        $evalution = new Evaluation();
        $evalution->organization_id = $organisation->id;
        $evalution->status = 2;
        $evalution->current_step = 6;
        $evalution->created_at = Carbon::now()->subDays(10);
        $evalution->updated_at = Carbon::now()->subDays(2);
        $evalution->updated_by = 'test update 2';
        $evalution->author = 'test author2';
        $evalution->reference = env('REFERENTIEL_VERSION');
        $evalution->save();

        $evalRepo = new EvaluationRepository();
        $repo = new GraphDataRepository($evalRepo);

        $data = $repo->getActionTerritoryGraphData();

        $expected = [
            'labels' => ['test'],
            'data' => [[0], [0]],
        ];

        $this->assertEquals($expected, $data);
    }

    public function testGetActionTerritoryGraphDataWithDataShouldReturnCorrectData()
    {
        $territory = new Territory();
        $territory->name = 'test';
        $territory->save();

        $organisation = new Organization();
        $organisation->territory_id = $territory->id;
        $organisation->name = 'test org';
        $organisation->short_name = 'testorg';
        $organisation->type = 'CCAS';
        $organisation->siren = '123456';
        $organisation->active = true;
        $organisation->save();

        $measures = Measure::factory(5)->create();

        $oldevalution = new Evaluation();
        $oldevalution->organization_id = $organisation->id;
        $oldevalution->status = 2;
        $oldevalution->current_step = 6;
        $oldevalution->created_at = Carbon::now()->subDays(10);
        $oldevalution->updated_at = Carbon::now()->subDays(2);
        $oldevalution->updated_by = 'test update 2';
        $oldevalution->author = 'test author2';
        $oldevalution->reference = env('REFERENTIEL_VERSION');
        $oldevalution->save();

        foreach ($measures as $measure) {
            $ml = new MeasureLevel();
            $ml->measure_id = $measure->id;
            $ml->evaluation_id = $oldevalution->id;
            $ml->expected_level = 2;
            $ml->end_date = Carbon::now()->addMonths(3);
            $ml->manager = 'test manager';
            $ml->actual_level = 1;
            $ml->save();
        }

        $evalution = new Evaluation();
        $evalution->organization_id = $organisation->id;
        $evalution->status = 2;
        $evalution->current_step = 6;
        $evalution->created_at = Carbon::now()->subDays(10);
        $evalution->updated_at = Carbon::now();
        $evalution->updated_by = 'test update';
        $evalution->author = 'test author';
        $evalution->reference = env('REFERENTIEL_VERSION');
        $evalution->save();

        foreach ($measures as $k => $measure) {
            $ml = new MeasureLevel();
            $ml->measure_id = $measure->id;
            $ml->evaluation_id = $evalution->id;
            $ml->expected_level = 3;
            $ml->actual_level = $k % 4;
            $ml->end_date = Carbon::now()->addMonths(14);
            $ml->manager = 'test manager';
            $ml->save();
        }

        $evalRepo = new EvaluationRepository();
        $repo = new GraphDataRepository($evalRepo);

        $data = $repo->getActionTerritoryGraphData();

        $expected = [
            'labels' => ['test'],
            'data' => [[5], [2]],
        ];

        $this->assertEquals($expected, $data);
    }

    public function testGetOrganizationDataWithDataShouldReturnCorrectData()
    {
        $territory = new Territory();
        $territory->name = 'test';
        $territory->save();

        $organisation = new Organization();
        $organisation->territory_id = $territory->id;
        $organisation->name = 'test org';
        $organisation->short_name = 'testorg';
        $organisation->type = 'CCAS';
        $organisation->siren = '123456';
        $organisation->active = true;
        $organisation->save();

        $measures = Measure::factory(5)->create();

        $oldevalution = new Evaluation();
        $oldevalution->organization_id = $organisation->id;
        $oldevalution->status = 2;
        $oldevalution->current_step = 6;
        $oldevalution->created_at = Carbon::now()->subDays(10);
        $oldevalution->updated_at = Carbon::now()->subDays(2);
        $oldevalution->updated_by = 'test update 2';
        $oldevalution->author = 'test author2';
        $oldevalution->reference = env('REFERENTIEL_VERSION');
        $oldevalution->save();

        foreach ($measures as $k => $measure) {
            $ml = new MeasureLevel();
            $ml->measure_id = $measure->id;
            $ml->evaluation_id = $oldevalution->id;
            $ml->expected_level = $k % 4;
            $ml->end_date = Carbon::now()->addMonths(3);
            $ml->manager = 'test manager';
            $ml->actual_level = 1;
            $ml->save();
        }

        $evalution = new Evaluation();
        $evalution->organization_id = $organisation->id;
        $evalution->status = 2;
        $evalution->current_step = 6;
        $evalution->created_at = Carbon::now()->subDays(10);
        $evalution->updated_at = Carbon::now();
        $evalution->updated_by = 'test update';
        $evalution->author = 'test author';
        $evalution->reference = env('REFERENTIEL_VERSION');
        $evalution->save();

        foreach ($measures as $k => $measure) {
            $ml = new MeasureLevel();
            $ml->measure_id = $measure->id;
            $ml->evaluation_id = $evalution->id;
            $ml->expected_level = $k % 4;
            $ml->actual_level = $k % 4;
            $ml->end_date = Carbon::now()->addMonths(14);
            $ml->manager = 'test manager';
            $ml->save();
        }

        $evalRepo = new EvaluationRepository();
        $repo = new GraphDataRepository($evalRepo);

        $data = $repo->getOrganizationData();

        $expected = [
            'maturity' => 1.5,
            'count' => 1,
            'top_measures' => [
                    0 => [
                            'short_name' => $measures[1]->short_name,
                            'organizations' => 1,
                            'max_levels' => [
                                    0 => 1,
                                ],
                            'max_value' => 1,
                            'percentage' => 100,
                        ],
                    1 => [
                            'short_name' => $measures[2]->short_name,
                            'organizations' => 1,
                            'max_levels' => [
                                    0 => 2,
                                ],
                            'max_value' => 1,
                            'percentage' => 100,
                        ],
                    2 => [
                            'short_name' => $measures[3]->short_name,
                            'organizations' => 1,
                            'max_levels' => [
                                    0 => 3,
                                ],
                            'max_value' => 1,
                            'percentage' => 100,
                        ],
                ],
        ];

        $this->assertEquals($expected, $data);
    }
}
