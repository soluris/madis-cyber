<?php

namespace Tests\Feature;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testLoginWithRealActiveUserSucceeds()
    {
        $user = User::with('organization')
            ->where('active', 1)
            ->whereHas('organization', function ($q) {
                $q->where('active', 1);
            })
            ->where('role', User::ROLE_USER)
            ->first();

        $response = $this->postJson('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertOk();
    }

    public function testLoginUpdatesLastLogin()
    {
        $user = User::with('organization')
            ->where('active', 1)
            ->whereHas('organization', function ($q) {
                $q->where('active', 1);
            })
            ->where('role', User::ROLE_USER)
            ->first();

        $user->last_login = null;
        $user->save();

        $response = $this->postJson('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertOk();

        $response = $this->actingAs($user)->getJson(route('api.users.me'));

        $data = $response->json();

        $this->assertNotNull($data['last_login']);

        $this->assertEquals(
            Carbon::now()->format('Y-m-d H:i'),
            Carbon::parse($data['last_login'])->format('Y-m-d H:i')
        );
    }

    public function testLoginWithWrongPasswordFails()
    {
        $user = User::with('organization')
            ->where('active', 1)
            ->whereHas('organization', function ($q) {
                $q->where('active', 1);
            })
            ->where('role', User::ROLE_USER)
            ->first();

        $response = $this->postJson('/login', [
            'email' => $user->email,
            'password' => 'oops',
        ]);

        $response->assertStatus(403);
        $response = $this->getJson('/users/me');

        $response->assertStatus(401);
    }

    public function testLoginWithFakeUserFails()
    {
        $response = $this->postJson('/login', [
            'email' => 'fake@fake.com',
            'password' => 'notarealpassword',
        ]);

        $response->assertStatus(403);
        $response = $this->getJson('/users/me');

        $response->assertStatus(401);
    }

    public function testLoginWithInactiveUserFails()
    {
        $user = User::with('organization')
            ->where('active', 0)
            ->whereHas('organization', function ($q) {
                $q->where('active', 1);
            })
            ->where('role', User::ROLE_USER)
            ->first();

        $response = $this->postJson('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(403);

        $response = $this->getJson('/users/me');

        $response->assertStatus(401);
    }

    public function testLoginWithInactiveOrganisationFails()
    {
        $user = User::with('organization')
            ->where('active', 1)
            ->whereHas('organization', function ($q) {
                $q->where('active', 0);
            })
            ->where('role', User::ROLE_USER)
            ->first();

        if (!$user) {
            echo 'No active user exists with inactive organisation, skipping test';
            $this->markTestSkipped();
        }

        $response = $this->postJson('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(403);

        $response = $this->getJson('/users/me');

        $response->assertStatus(401);
    }

    public function testLoginAs()
    {
        $user = User::where('role', User::ROLE_ADMIN)->where('active', 1)->with('organization')->first();
        $loginAsUser = User::where('role', User::ROLE_USER)->where('active', 1)->first();

        $response = $this->actingAs($user)->postJson(route('api.impersonate'), ['id' => $loginAsUser->id]);

        $response->assertStatus(200);
        [$id, $token] = explode('|', $response->content(), 2);

        $tokenUser = DB::table('personal_access_tokens')
            ->where('token', hash('sha256', $token))
            ->where('tokenable_id', $loginAsUser->id)
            ->first();

        $lu = User::find($tokenUser->tokenable_id);

        $this->assertEquals($loginAsUser->id, $lu->id);
    }
}
