<?php

namespace Tests\Feature;

use App\Models\Measure;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MeasuresControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testAdminCanGetAll()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();
        $response = $this->actingAs($user)->getJson(route('api.measures.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $m = Measure::orderBy('fundamental', 'ASC')->get()->map(function (Measure $m) {
            return $m->withConvertedLevels();
        });

        $this->assertEquals($m->toArray(), $response->json());
    }

    /**
     * Test get all.
     *
     * @return void
     */
    public function testUserCanGetAll()
    {
        $user = User::where('role', '>', User::ROLE_ADMIN)->first();
        $response = $this->actingAs($user)->getJson(route('api.measures.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $m = Measure::orderBy('fundamental', 'ASC')->get()->map(function (Measure $m) {
            return $m->withConvertedLevels();
        });

        $this->assertEquals($m->toArray(), $response->json());
    }

    /**
     * Test update.
     *
     * @return void
     */
    public function testAdminCanUpdateMeasure()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $m = Measure::first()->toArray();

        $response = $this->actingAs($user)->putJson(route('api.measures.put', ['id' => $m['id']]),
            [
                'name' => 'NAME',
                'short_name' => 'SHORT_NAME',
                'levels' => [
                    [
                        'actual_label' => 'LEVEL 1 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 1 PRECONIZED LABEL',
                        'description' => 'test1',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                    [
                        'actual_label' => 'LEVEL 2 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 2 PRECONIZED LABEL',
                        'description' => 'test2',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                    [
                        'actual_label' => 'LEVEL 3 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 3 PRECONIZED LABEL',
                        'description' => 'test3',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                ],
            ]);

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();
        $data = $response->json();

        $m = Measure::find($m['id']);

        $this->assertEquals('LEVEL 1 ACTUAL LABEL', $data['levels'][1]['actual_label']);
        $this->assertEquals('LEVEL 1 ACTUAL LABEL', $m->level1_actual_label);
        $this->assertEquals('LEVEL 2 ACTUAL LABEL', $data['levels'][2]['actual_label']);
        $this->assertEquals('LEVEL 2 ACTUAL LABEL', $m->level2_actual_label);
        $this->assertEquals('LEVEL 3 ACTUAL LABEL', $data['levels'][3]['actual_label']);
        $this->assertEquals('LEVEL 3 ACTUAL LABEL', $m->level3_actual_label);
        $this->assertEquals('LEVEL 1 PRECONIZED LABEL', $data['levels'][1]['preconized_label']);
        $this->assertEquals('LEVEL 1 PRECONIZED LABEL', $m->level1_preconized_label);
        $this->assertEquals('LEVEL 2 PRECONIZED LABEL', $data['levels'][2]['preconized_label']);
        $this->assertEquals('LEVEL 2 PRECONIZED LABEL', $m->level2_preconized_label);
        $this->assertEquals('LEVEL 3 PRECONIZED LABEL', $data['levels'][3]['preconized_label']);
        $this->assertEquals('LEVEL 3 PRECONIZED LABEL', $m->level3_preconized_label);
    }

    /**
     * Test update.
     *
     * @return void
     */
    public function testAdminCanUploadFileToMeasure()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $m = Measure::first()->toArray();

        $docName = 'documents/mesures/name-' . $m['id'] . '/name-niveau-1.pdf';

        $fullPath = storage_path('app/public/' . $docName);

        if (file_exists($fullPath)) {
            unlink($fullPath);
        }

        $response = $this->actingAs($user)->putJson(route('api.measures.put', ['id' => $m['id']]),
            [
                'name' => 'name',
                'short_name' => 'SHORT_NAME',
                'levels' => [
                    [
                        'actual_label' => 'LEVEL 1 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 1 PRECONIZED LABEL',
                        'description' => 'test1',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                        'file' => 'data:image/pdf;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
                    ],
                    [
                        'actual_label' => 'LEVEL 2 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 2 PRECONIZED LABEL',
                        'description' => 'test2',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                    [
                        'actual_label' => 'LEVEL 3 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 3 PRECONIZED LABEL',
                        'description' => 'test3',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                ],
            ]);

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();
        $data = $response->json();

        $m = Measure::find($m['id']);

        $this->assertEquals($m->withConvertedLevels(), $data);

        $this->assertTrue(file_exists($fullPath));

        $file = file_get_contents($fullPath);

        $this->assertNotNull($file);

        $this->assertEquals('/storage/' . $docName, $data['levels'][1]['file']);
        $this->assertEquals('LEVEL 1 ACTUAL LABEL', $data['levels'][1]['actual_label']);
        $this->assertEquals('LEVEL 1 ACTUAL LABEL', $m->level1_actual_label);
        $this->assertEquals('LEVEL 2 ACTUAL LABEL', $data['levels'][2]['actual_label']);
        $this->assertEquals('LEVEL 2 ACTUAL LABEL', $m->level2_actual_label);
        $this->assertEquals('LEVEL 3 ACTUAL LABEL', $data['levels'][3]['actual_label']);
        $this->assertEquals('LEVEL 3 ACTUAL LABEL', $m->level3_actual_label);
        $this->assertEquals('LEVEL 1 PRECONIZED LABEL', $data['levels'][1]['preconized_label']);
        $this->assertEquals('LEVEL 1 PRECONIZED LABEL', $m->level1_preconized_label);
        $this->assertEquals('LEVEL 2 PRECONIZED LABEL', $data['levels'][2]['preconized_label']);
        $this->assertEquals('LEVEL 2 PRECONIZED LABEL', $m->level2_preconized_label);
        $this->assertEquals('LEVEL 3 PRECONIZED LABEL', $data['levels'][3]['preconized_label']);
        $this->assertEquals('LEVEL 3 PRECONIZED LABEL', $m->level3_preconized_label);
    }

    /**
     * Test update.
     *
     * @return void
     */
    public function testUploadWrongFileTypeThrowsError()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $m = Measure::first()->toArray();

        $docName = 'documents/mesures/name-' . $m['id'] . '/name-' . $m['id'] . '-niveau1.png';

        $fullPath = storage_path('app/public/' . $docName);

        if (file_exists($fullPath)) {
            unlink($fullPath);
        }

        $response = $this->actingAs($user)->putJson(route('api.measures.put', ['id' => $m['id']]),
            [
                'name' => 'name',
                'short_name' => 'SHORT_NAME',
                'levels' => [
                    [
                        'actual_label' => 'LEVEL 1 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 1 PRECONIZED LABEL',
                        'description' => 'test1',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                        'file' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
                    ],
                    [
                        'actual_label' => 'LEVEL 2 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 2 PRECONIZED LABEL',
                        'description' => 'test2',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                    [
                        'actual_label' => 'LEVEL 3 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 3 PRECONIZED LABEL',
                        'description' => 'test3',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                ],
            ]);

        $response->assertStatus(422);

        $response->assertJson(['message' => 'Le fichier du niveau 1 doit être un fichier PDF']);
    }

    /*
     * Test update.
     *
     * @return void
     */

    public function testNonAdminCannotUpdateMeasure()
    {
        $user = User::where('role', '>', User::ROLE_MANAGER)->first();

        $m = Measure::first()->toArray();

        $response = $this->actingAs($user)->putJson(route('api.measures.put', ['id' => $m['id']]),
            [
                'name' => 'NAME',
                'short_name' => 'SHORT_NAME',
                'levels' => [
                    [
                        'actual_label' => 'LEVEL 1 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 1 PRECONIZED LABEL',
                        'description' => 'desc1',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                        'file' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
                    ],
                    [
                        'actual_label' => 'LEVEL 2 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 2 PRECONIZED LABEL',
                        'description' => 'errrr',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                    [
                        'actual_label' => 'LEVEL 3 ACTUAL LABEL',
                        'preconized_label' => 'LEVEL 3 PRECONIZED LABEL',
                        'description' => 'desc3',
                        'difficulty' => '2',
                        'cost' => '3',
                        'duration' => '4',
                        'assistance' => '5',
                    ],
                ],
            ]);

        $response->assertStatus(403);
    }
}
