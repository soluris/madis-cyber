<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BasicTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test redirect.
     *
     * @return void
     */
    public function testShouldRedirectToLogin()
    {
        $response = $this->get('/');

        $response->assertRedirect('/login');
    }

    /**
     * Test Home.
     *
     * @return void
     */
    public function testLoggedInShouldShowHome()
    {
        $user = User::with('organization')->first();
        $response = $this->actingAs($user)->get('/');

        $response->assertOk();
    }
}
