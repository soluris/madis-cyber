<?php

namespace Tests\Feature;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get self.
     *
     * @return void
     */
    public function testRegularUserCanGetSelf()
    {
        $user = User::with('organization')->first();
        $response = $this->actingAs($user)->getJson(route('api.users.me'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $this->assertEquals($user->toArray(), $response->json());
    }

    /**
     * Test Other users.
     *
     * @return void
     */
    public function testRegularUserCannotGetOtherUsers()
    {
        $user = User::where('role', User::ROLE_USER)->with('organization')->first();
        $response = $this->actingAs($user)->getJson(route('api.users.all'));

        $response->assertStatus(403);
    }

    /**
     * Test admin can get all users.
     *
     * @return void
     */
    public function testAdminCanGetAllUsers()
    {
        $user = User::where('role', User::ROLE_ADMIN)->with('organization')->first();
        $all = User::with('organization')->get();
        $response = $this->actingAs($user)->getJson(route('api.users.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $this->assertEquals($all->toArray(), $response->json());
    }

    /**
     * Test Manager can get all users from their organization.
     *
     * @return void
     */
    public function testManagerCanGetOrganizationUsers()
    {
        $user = User::where('role', User::ROLE_MANAGER)->with('organization')->first();
        $all = User::where('organization_id', $user->organization_id)
            ->with('organization')
            ->get();
        $response = $this->actingAs($user)->getJson(route('api.users.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $this->assertEquals($all->toArray(), $response->json());
    }

    /**
     * Test profile update.
     */
    public function testUserCanSaveProfile()
    {
        $user = User::where('role', User::ROLE_USER)->with('organization')->first();

        $response = $this->actingAs($user)->putJson(route('api.users.put', ['id' => $user->id]), [
            'firstname' => 'NEW FIRSTNAME',
            'lastname' => 'NEW LASTNAME',
            'email' => 'email@email.fr',
            'role' => User::ROLE_USER,
            'civility' => 'Madame',
        ]);

        $response->assertOk();

        $newUser = User::find($user->id);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $this->assertEquals($newUser->firstname, $data['firstname']);
    }

    /**
     * Test profile update.
     */
    public function testUserCannotSaveOtherProfile()
    {
        $user = User::where('role', User::ROLE_USER)->with('organization')->first();

        $response = $this->actingAs($user)->putJson(route('api.users.put', ['id' => $user->id + 1]), [
            'firstname' => 'NEW FIRSTNAME',
            'civility' => 'Madame',
        ]);

        $response->assertStatus(403);
    }

    /**
     * Test profile update.
     */
    public function testUserCannotUpdateRole()
    {
        $user = User::where('role', User::ROLE_MANAGER)->with('organization')->first();

        $response = $this->actingAs($user)->putJson(route('api.users.put', ['id' => $user->id]), [
            'role' => User::ROLE_ADMIN,
            'firstname' => 'FIRSTNAME',
            'lastname' => 'LASTNAME',
            'email' => 'test@test.fr',
            'civility' => 'Madame',
        ]);

        $response->assertOk();

        $newUser = User::find($user->id);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $this->assertEquals(User::ROLE_MANAGER, $newUser->role);
        $this->assertEquals(User::ROLE_MANAGER, $data['role']);
        $this->assertEquals('FIRSTNAME', $data['firstname']);
        $this->assertEquals('LASTNAME', $data['lastname']);
        $this->assertEquals('test@test.fr', $data['email']);
    }

    public function testAdminCanUpdateRole()
    {
        $admin = User::where('role', User::ROLE_ADMIN)->with('organization')->first();
        $manager = User::where('role', User::ROLE_MANAGER)->with('organization')->first();

        $postUser = $manager->toArray();
        $postUser['role'] = User::ROLE_ADMIN;

        $response = $this->actingAs($admin)->putJson(route('api.users.put', ['id' => $manager->id]), $postUser);

        $response->assertOk();

        $newUser = User::find($manager->id);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $this->assertEquals(User::ROLE_ADMIN, $data['role']);
        $this->assertEquals(User::ROLE_ADMIN, $newUser->role);
    }

    /**
     * Test profile update.
     */
    public function testAdminCanSaveOtherProfile()
    {
        $admin = User::where('role', User::ROLE_ADMIN)->with('organization')->first();
        $user = User::where('role', User::ROLE_USER)->with('organization')->first();

        $postUser = $user->toArray();
        $postUser['firstname'] = 'NEW FIRSTNAME';
        $postUser['civility'] = 'Madame';

        $response = $this->actingAs($admin)->putJson(route('api.users.put', ['id' => $user->id]), $postUser);

        $response->assertOk();
        $newUser = User::find($user->id);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $this->assertEquals('NEW FIRSTNAME', $data['firstname']);
        $this->assertEquals('NEW FIRSTNAME', $newUser->firstname);
    }

    /**
     * Test profile update.
     */
    public function testAdminCanSaveProfile()
    {
        $admin = User::where('role', User::ROLE_ADMIN)->with('organization')->first();

        $postUser = $admin->toArray();
        $postUser['firstname'] = 'NEW FIRSTNAME';
        $postUser['civility'] = 'Madame';

        $response = $this->actingAs($admin)->putJson(route('api.users.put', ['id' => $admin->id]), $postUser);

        $response->assertOk();
        $newUser = User::find($admin->id);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $this->assertEquals('NEW FIRSTNAME', $data['firstname']);
        $this->assertEquals('NEW FIRSTNAME', $newUser->firstname);
    }

    /**
     * Test user create.
     */
    public function testAdminCanCreateUser()
    {
        $user = User::where('role', User::ROLE_ADMIN)->with('organization')->first();

        $response = $this->actingAs($user)->postJson(route('api.users.post'), [
            'firstname' => 'FIRSTNAME',
            'lastname' => 'LASTNAME',
            'email' => 'test@datakode.fr',
            'password' => '1strongpassword',
            'role' => User::ROLE_USER,
            'organization_id' => 1,
            'civility' => 'Madame',
        ]);

        $response->assertStatus(200);

        if ($response->exception) {
            dump($response->exception);
        }

        $data = $response->json();

        $newUser = User::with('organization')->find($data['id']);

        $this->assertNotNull($newUser);

        $data['email_verified_at'] = null;
        $data['last_login'] = null;
        $data['active'] = $data['active'] ? 1 : 0;

        $this->assertEquals($newUser->toArray(), $data);
        $this->assertEquals('FIRSTNAME', $newUser->firstname);
        $this->assertEquals('LASTNAME', $newUser->lastname);
        $this->assertEquals('test@datakode.fr', $newUser->email);
        $this->assertEquals(User::ROLE_USER, $newUser->role);
        $this->assertEquals(1, $newUser->organization_id);
    }

    /**
     * Test user create.
     */
    public function testShortPasswordFails()
    {
        $user = User::where('role', User::ROLE_ADMIN)->with('organization')->first();

        $response = $this->actingAs($user)->postJson(route('api.users.post'), [
            'firstname' => 'FIRSTNAME',
            'lastname' => 'LASTNAME',
            'email' => 'test@datakode.fr',
            'password' => 'test1',
            'role' => User::ROLE_USER,
            'organization_id' => 1,
            'civility' => 'Madame',
        ]);

        $response->assertStatus(422);

        $response->assertJson(['message' => 'Le texte password doit contenir au moins 12 caractères.']);
    }

    /**
     * Test user create.
     */
    public function testNoNumberPasswordFails()
    {
        $user = User::where('role', User::ROLE_ADMIN)->with('organization')->first();

        $response = $this->actingAs($user)->postJson(route('api.users.post'), [
            'firstname' => 'FIRSTNAME',
            'lastname' => 'LASTNAME',
            'email' => 'test@datakode.fr',
            'password' => 'testtesttest',
            'role' => User::ROLE_USER,
            'organization_id' => 1,
            'civility' => 'Madame',
        ]);

        $response->assertStatus(422);

        $response->assertJson(['message' => 'Le format du champ password est invalide.']);
    }

    /**
     * Test user create.
     */
    public function testUserCannotCreateUser()
    {
        $user = User::where('role', '>', User::ROLE_MANAGER)->with('organization')->first();

        $response = $this->actingAs($user)->postJson(route('api.users.post'), [
            'firstname' => 'FIRSTNAME',
            'lastname' => 'LASTNAME',
            'email' => 'test@datakode.fr',
            'password' => '1strongpassword',
            'role' => User::ROLE_USER,
            'civility' => 'Madame',
        ]);

        $response->assertStatus(403);
    }

    /**
     * Test user delete.
     */
    public function testAdminCanDeleteAnyUser()
    {
        $admin = User::where('role', User::ROLE_ADMIN)->with('organization')->first();
        $user = User::where('role', User::ROLE_MANAGER)->with('organization')->first();

        $response = $this->actingAs($admin)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(204);
    }

    /**
     * Test user delete.
     */
    public function testManagerCanDeleteUsersInOrganization()
    {
        $organization = Organization::first();
        $manager = User::where('role', User::ROLE_MANAGER)
            ->where('organization_id', $organization->id)
            ->with('organization')
            ->first();
        $user = User::where('organization_id', $organization->id)
            ->with('organization')
            ->first();

        $response = $this->actingAs($manager)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(204);
    }

    /**
     * Test user delete.
     */
    public function testManagerCannotDeleteUsersInOtherOrganizations()
    {
        $organizations = Organization::all();
        $manager = User::where('role', User::ROLE_MANAGER)
            ->where('organization_id', $organizations[0]->id)
            ->with('organization')
            ->first();
        $user = User::where('organization_id', $organizations[1]->id)
            ->with('organization')
            ->first();

        $response = $this->actingAs($manager)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(403);
    }

    /**
     * Test user delete.
     */
    public function testManagerCannotDeleteAdmins()
    {
        $manager = User::where('role', User::ROLE_MANAGER)
            ->with('organization')
            ->first();
        $user = User::where('role', User::ROLE_ADMIN)
            ->with('organization')
            ->first();

        $response = $this->actingAs($manager)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(403);
    }

    /**
     * Test user delete.
     */
    public function testUserCannotDeleteUsersInOrganization()
    {
        $organization = Organization::first();
        $user = User::where('role', User::ROLE_USER)
            ->where('organization_id', $organization->id)
            ->with('organization')
            ->first();
        $deleteUser = User::where('organization_id', $organization->id)
            ->where('id', '!=', $user->id)
            ->with('organization')
            ->first();

        $response = $this->actingAs($user)->deleteJson(route('api.users.delete', ['id' => $deleteUser->id]));

        $response->assertStatus(403);
    }

    /**
     * Test user delete.
     */
    public function testUserCannotDeleteUsersInOtherOrganizations()
    {
        $organizations = Organization::all();
        $manager = User::where('role', User::ROLE_USER)
            ->where('organization_id', $organizations[0]->id)
            ->with('organization')
            ->first();
        $user = User::where('organization_id', $organizations[1]->id)
            ->with('organization')
            ->first();

        $response = $this->actingAs($manager)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(403);
    }

    /**
     * Test user delete.
     */
    public function testUserCannotDeleteAdmins()
    {
        $manager = User::where('role', User::ROLE_USER)
            ->with('organization')
            ->first();
        $user = User::where('role', User::ROLE_ADMIN)
            ->with('organization')
            ->first();

        $response = $this->actingAs($manager)->deleteJson(route('api.users.delete', ['id' => $user->id]));

        $response->assertStatus(403);
    }
}
