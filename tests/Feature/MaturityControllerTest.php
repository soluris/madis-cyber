<?php

namespace Tests\Feature;

use App\Models\MaturityQuestion;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MaturityControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testCanGetQuestions()
    {
        $user = User::where('role', User::ROLE_USER)->first();
        $response = $this->actingAs($user)->getJson(route('api.maturity.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $questions = MaturityQuestion::with(['answers'])->get();

        $this->assertEquals($questions->toArray(), $response->json());

        $this->assertCount(5, $questions);
        $this->assertCount(4, $questions[0]->answers);
    }
}
