<?php

namespace Tests\Feature;

use App\Models\Danger;
use App\Models\DangerLevel;
use App\Models\DangerLevelEvaluation;
use App\Models\Evaluation;
use App\Models\EvaluationMaturityAnswer;
use App\Models\MaturityAnswer;
use App\Models\MaturityQuestion;
use App\Models\Measure;
use App\Models\MeasureLevel;
use App\Models\Organization;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EvaluationsControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testAdminUserCanGetAllEvaluations()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();
        $response = $this->actingAs($user)->getJson(route('api.evaluations.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $evals = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->get();

        $this->assertEquals($evals->toArray(), $response->json());
    }

    /**
     * Test get all.
     *
     * @return void
     */
    public function testUserCanOnlyGetEvaluationsFromOwnOrganization()
    {
        $user = User::where('role', User::ROLE_USER)->first();
        $response = $this->actingAs($user)->getJson(route('api.evaluations.all'));

        $response->assertOk();

        $evals = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])
            ->where('organization_id', $user->organization_id)
            ->get()
        ;

        $this->assertEquals($evals->toArray(), $response->json());
    }

    /**
     * Test get all.
     *
     * @return void
     */
    public function testManagerCanOnlyGetEvaluationsFromOwnOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();
        $response = $this->actingAs($user)->getJson(route('api.evaluations.all'));

        $response->assertOk();

        $evals = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])
            ->where('organization_id', $user->organization_id)
            ->get()
        ;

        $this->assertEquals($evals->toArray(), $response->json());
    }

    /**
     * test delete.
     */
    public function testAdminCanDeleteAnyEvaluation()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $eval = Evaluation::first();

        $response = $this->actingAs($user)->deleteJson(route('api.evaluations.delete', ['id' => $eval->id]));

        $response->assertStatus(204);
    }

    /**
     * test delete.
     */
    public function testManagerCanDeleteEvaluationInOwnOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::where('organization_id', $user->organization_id)->first();

        $response = $this->actingAs($user)->deleteJson(route('api.evaluations.delete', ['id' => $eval->id]));

        $response->assertStatus(204);
    }

    /**
     * test delete.
     */
    public function testManagerCannotDeleteEvaluationInOtherOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::where('organization_id', '!=', $user->organization_id)->first();

        $response = $this->actingAs($user)->deleteJson(route('api.evaluations.delete', ['id' => $eval->id]));

        $response->assertStatus(403);
    }

    /**
     * test delete.
     */
    public function testUserCannotDeleteEvaluation()
    {
        $user = User::where('role', User::ROLE_USER)->first();

        $eval = Evaluation::where('organization_id', $user->organization_id)->first();

        $response = $this->actingAs($user)->deleteJson(route('api.evaluations.delete', ['id' => $eval->id]));

        $response->assertStatus(403);
    }

    public function testPostDraftEvaluationShouldCreateDraftEvaluation()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $nextId = Evaluation::orderBy('id', 'desc')->first()->id + 1;

        $eval = [
            'organization_id' => $user->organization_id,
            'current_step' => 1,
            'status' => 2,
            'draft' => 1,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'author' => $user->firstname . ' ' . $user->lastname,
            'danger_levels' => [
                [
                    'danger_level_id' => DangerLevel::first()->id,
                    'danger_id' => Danger::first()->id,
                ],
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.evaluations.post'), $eval);
        $response->assertOk();

        $data = $response->json();

        $dangersLevelsEval = DangerLevelEvaluation::where('evaluation_id', $nextId)->get();

        $this->assertCount(1, $dangersLevelsEval);

        $this->assertEquals([
            'id' => $nextId,
            'status' => Evaluation::STATUS_ONGOING,
            'current_step' => 1,
            'author' => $user->firstname . ' ' . $user->lastname,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'organization_id' => $user->organization_id,
            'organization' => $user->organization->toArray(),
            'created_at' => Carbon::now()->setMicroseconds(0)->format('Y-m-d\TH:i:s.u\Z'),
            'updated_at' => Carbon::now()->setMicroseconds(0)->format('Y-m-d\TH:i:s.u\Z'),
            'danger_levels' => $dangersLevelsEval->toArray(),
            'measure_levels' => [],
            'maturity_levels' => [],
            'reference' => env('REFERENTIEL_VERSION'),
            'maturity_cyber' => 0,
        ], $data);

        $dbEval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->find($nextId);

        $this->assertNotNull($dbEval);

        $this->assertEquals($dbEval->toArray(), $data);
    }

    public function testPostNonDraftEvaluationWithDataShouldMoveToStep2()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $nextId = Evaluation::orderBy('id', 'desc')->first()->id + 1;

        $dangerLevels = Danger::all()->map(function (Danger $danger) {
            return [
                'danger_id' => $danger->id,
                'danger_level_id' => DangerLevel::inRandomOrder()->first()->id,
            ];
        })->toArray();

        $eval = [
            'organization_id' => $user->organization_id,
            'current_step' => 1,
            'status' => 1,
            'draft' => 0,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'author' => $user->firstname . ' ' . $user->lastname,
            'danger_levels' => $dangerLevels,
        ];

        $response = $this->actingAs($user)->postJson(route('api.evaluations.post'), $eval);

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $data = $response->json();

        $dangersLevelsEval = DangerLevelEvaluation::where('evaluation_id', $nextId)->get();

        $this->assertEquals([
            'id' => $nextId,
            'status' => Evaluation::STATUS_ONGOING,
            'current_step' => 2,
            'author' => $user->firstname . ' ' . $user->lastname,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'organization_id' => $user->organization_id,
            'organization' => $user->organization->toArray(),
            'created_at' => $data['created_at'],
            'updated_at' => $data['updated_at'],
            'danger_levels' => $dangersLevelsEval->toArray(),
            'measure_levels' => [],
            'maturity_levels' => [],
            'reference' => env('REFERENTIEL_VERSION'),
            'maturity_cyber' => 0,
        ], $data);

        $dbEval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->find($nextId);

        $this->assertNotNull($dbEval);

        $this->assertEquals($dbEval->toArray(), $data);
    }

    public function testPostNonDraftEvaluationWithMissingDataDataShouldThrowError()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = [
            'organization_id' => $user->organization_id,
            'current_step' => 1,
            'status' => 1,
            'draft' => 0,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'author' => $user->firstname . ' ' . $user->lastname,
            'danger_levels' => [
                ['id' => DangerLevel::first()->id],
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.evaluations.post'), $eval);
        $response->assertJson(['message' => 'Un ou plusieurs niveaux de dangers sont manquants']);
        $response->assertStatus(422);
    }

    public function testPutDraftEvaluationShouldSaveDraftEvaluation()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::where('organization_id', $user->organization_id)
            ->with('dangerLevels')
            ->with('maturityLevels')
            ->where('current_step', 1)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->first()
            ->toArray();

        $eval['draft'] = 1;

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);
        $response->assertOk();

        $data = $response->json();

        $newDLS = DangerLevelEvaluation::where('evaluation_id', $eval['id'])->get()->toArray();

        $this->assertEquals([
            'id' => $eval['id'],
            'status' => Evaluation::STATUS_ONGOING,
            'current_step' => $eval['current_step'],
            'author' => $eval['author'],
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'organization_id' => $user->organization_id,
            'organization' => $user->organization->toArray(),
            'created_at' => $data['created_at'],
            'updated_at' => Carbon::now()->setMicroseconds(0)->format('Y-m-d\TH:i:s.u\Z'),
            'danger_levels' => $newDLS,
            'measure_levels' => [],
            'maturity_levels' => [],
            'reference' => env('REFERENTIEL_VERSION'),
            'maturity_cyber' => $eval['maturity_cyber'],
        ], $data);

        $dbEval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->find($eval['id']);

        $this->assertNotNull($dbEval);

        $this->assertEquals($dbEval->toArray(), $data);
    }

    public function testPutDraftEvaluationWithStep2ShouldSaveMeasures()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::where('organization_id', $user->organization_id)
            ->with('dangerLevels')
            ->with('maturityLevels')
            ->where('current_step', Evaluation::STEP_MEASURES)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->first()
            ->toArray();

        $eval['draft'] = 1;

        $measureId = Measure::first()->id;

        $eval['measure_levels'] = [[
            'measure_id' => $measureId,
            'actual_level' => 1,
        ]];

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);

        $data = $response->json();

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $measureLevel = MeasureLevel::where('evaluation_id', $eval['id'])
            ->where('measure_id', $measureId)
            ->where('actual_level', 1)
            ->first();

        $this->assertNotNull($measureLevel);

        $this->assertEquals([
            'id' => $eval['id'],
            'status' => Evaluation::STATUS_ONGOING,
            'current_step' => $eval['current_step'],
            'author' => $eval['author'],
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'organization_id' => $user->organization_id,
            'organization' => $user->organization->toArray(),
            'created_at' => $data['created_at'],
            'updated_at' => Carbon::now()->setMicroseconds(0)->format('Y-m-d\TH:i:s.u\Z'),
            'danger_levels' => $eval['danger_levels'],
            'measure_levels' => [$measureLevel->toArray()],
            'maturity_levels' => [],
            'reference' => env('REFERENTIEL_VERSION'),
            'maturity_cyber' => $eval['maturity_cyber'],
        ], $data);

        $dbEval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->find($eval['id']);

        $this->assertNotNull($dbEval);

        $this->assertEquals($dbEval->toArray(), $data);
    }

    public function testPutEvaluationShouldSaveEvaluationWithFinishedStep()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'measureLevels.measure', 'maturityLevels'])
            ->where('organization_id', $user->organization_id)
            ->where('current_step', 5)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->first();

        $eval = $eval->toArray();

        $eval['draft'] = 0;

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);

        if ($response->exception) {
            dump($response->exception);
        }
        $response->assertOk();

        $data = $response->json();

        $newEval = Evaluation::find($eval['id']);

        $this->assertEquals([
            'id' => $eval['id'],
            'status' => Evaluation::STATUS_DONE,
            'current_step' => Evaluation::STEP_RESULTS,
            'author' => $eval['author'],
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'organization_id' => $user->organization_id,
            'organization' => $user->organization->toArray(),
            'created_at' => $data['created_at'],
            'updated_at' => $data['updated_at'],
            'danger_levels' => $data['danger_levels'],
            'measure_levels' => $eval['measure_levels'],
            'maturity_levels' => $eval['maturity_levels'],
            'reference' => env('REFERENTIEL_VERSION'),
            'maturity_cyber' => $newEval->maturity_cyber,
        ], $data);

        $dbEval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])->find($eval['id']);

        $this->assertNotNull($dbEval);

        $this->assertEquals($dbEval->toArray(), $data);
    }

    public function testPutEvaluationWithMissingMeasureAnswersShouldThrowError()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $eval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])
            ->where('organization_id', $user->organization_id)
            ->where('current_step', 5)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->first()
            ->toArray();

        $eval['draft'] = 0;

        $eval['maturity_levels'] = [
            [
              'maturity_question_id' => MaturityQuestion::inRandomOrder()->first()->id,
              'maturity_answer_id' => MaturityAnswer::inRandomOrder()->first()->id,
            ],
        ];

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);

        $response->assertJson(['message' => 'Certaines réponses aux questions de maturité sont manquantes.']);
        $response->assertStatus(422);
    }

    public function testManagerCannotCreateEvaluationInOtherOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $org = Organization::where('id', '!=', $user->organization_id)->inRandomORder()->first();

        $eval = [
            'organization_id' => $org->id,
            'current_step' => 1,
            'status' => 1,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'author' => $user->firstname . ' ' . $user->lastname,
        ];

        $response = $this->actingAs($user)->postJson(route('api.evaluations.post'), $eval);
        $response->assertStatus(403);
    }

    public function testUserCannotCreateEvaluation()
    {
        $user = User::where('role', User::ROLE_USER)->first();

        $eval = [
            'organization_id' => $user->organization_id,
            'current_step' => 1,
            'status' => 1,
            'updated_by' => $user->firstname . ' ' . $user->lastname,
            'author' => $user->firstname . ' ' . $user->lastname,
        ];

        $response = $this->actingAs($user)->postJson(route('api.evaluations.post'), $eval);
        $response->assertStatus(403);
    }

    public function testUserCannotUpdateEvaluation()
    {
        $user = User::where('role', User::ROLE_USER)->first();

        $eval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])
            ->where('organization_id', $user->organization_id)
            ->where('reference', env('REFERENTIEL_VERSION'))
            ->first()
            ->toArray();

        $eval['draft'] = 1;

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);

        $response->assertStatus(403);
    }

    public function testAdminCannotUpdateEvaluationIfDifferentReference()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $eval = Evaluation::with(['organization', 'dangerLevels', 'measureLevels', 'maturityLevels'])
            ->where('reference', '!=', env('REFERENTIEL_VERSION'))
            ->first();

        $eval = $eval->toArray();

        $eval['draft'] = 0;

        $response = $this->actingAs($user)->putJson(route('api.evaluations.put', ['id' => $eval['id']]), $eval);

        $response->assertStatus(422);
        $response->assertJson(['message' => 'Le référentiel de cette évaluation ne correspond pas au référentiel actuel']);
    }

    public function testExecutionMaturityLevelWithOneCompletedEvaluationInOrganizationShouldBeZero()
    {
        $orgWithSingleEvaluation = Organization::whereHas('evaluations', function ($q) {
        }, '=', 1)->first();

        $evals = Evaluation::where('status', Evaluation::STATUS_DONE)

            ->where('organization_id', $orgWithSingleEvaluation->id)
            ->with(['maturityLevels', 'maturityLevels.answer', 'maturityLevels.question'])
            ->get()
        ;

        $this->assertEquals(1, $evals->count());

        /**
         * @var Evaluation $eval
         */
        $eval = $evals->first();

        $execLevel = $eval->maturityLevels->first(function (EvaluationMaturityAnswer $ml) {
            return 'Exécution' === $ml->question->title;
        });

        $this->assertEquals(0, $execLevel->answer->level);
    }
}
