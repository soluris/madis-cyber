<?php

namespace Tests\Feature;

use App\Models\Evaluation;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class ExportControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test organizations export.
     */
    public function testExportOrganizations(): void
    {
        /** @var Authenticatable $user */
        $user = User::with('organization')->first();
        $response = $this->actingAs($user)->get(route('export.organizations', ['ext' => 'json']));
        $this->_testJsonExportResponse($response, Organization::count());
    }

    /**
     * Test evaluations export.
     */
    public function testExportEvaluations(): void
    {
        /** @var Authenticatable $user */
        $user = User::with('organization')->first();
        $response = $this->actingAs($user)->get(route('export.evaluations', ['ext' => 'json']));
        $this->_testJsonExportResponse($response, Evaluation::count());
    }

    private function _testJsonExportResponse(TestResponse $response, int $expectedLineCount): void
    {
        if ($response->exception) {
            dump($response->exception);
        }
        $response->assertOk();

        $content = $response->json();
        self::assertGreaterThan(0, count($content['header']));
        self::assertSameSize($content['header'], $content['rows'][0]);
        self::assertCount($expectedLineCount, $content['rows']);
    }
}
