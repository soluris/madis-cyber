<?php

namespace Tests\Feature;

use App\Models\Danger;
use App\Models\DangerLevel;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DangersControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testCanGetAllDangers()
    {
        $user = User::where('role', User::ROLE_USER)->first();
        $response = $this->actingAs($user)->getJson(route('api.dangers.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $levels = DangerLevel::orderBy('value', 'asc')->get();
        $dangers = Danger::all()->map(function (Danger $d) use ($levels) {
            $danger = $d->toArray();
            $danger['levels'] = $levels->toArray();

            return $danger;
        });

        $this->assertEquals($dangers->toArray(), $response->json());
    }
}
