<?php

namespace Tests\Feature;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrganizationsControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testAdminUserCanGetAll()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();
        $response = $this->actingAs($user)->getJson(route('api.organizations.all'));

        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();

        $orgs = Organization::with(['address', 'referent', 'referentCyber', 'referentElu', 'territory'])->get();

        $this->assertEquals($orgs->toArray(), $response->json());
    }

    /**
     * Test get all fail.
     *
     * @return void
     */
    public function testUserOnlyHasOwnOrganizationInList()
    {
        $user = User::where('role', User::ROLE_USER)->first();
        $response = $this->actingAs($user)->getJson(route('api.organizations.all'));

        $response->assertOk();

        $org = Organization::with(['address', 'referent', 'referentCyber', 'referentElu', 'territory'])->find($user->organization_id);

        $this->assertEquals([$org->toArray()], $response->json());
    }

    /**
     * test create.
     */
    public function testAdminCanCreateOrganization()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
                'moreInfos' => 'ligne2',
                'cp' => '75001',
                'city' => 'Paris',
                'codeInsee' => '43555',
            ],
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_elu' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'territory_id' => 1,
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(200);

        $data = $response->json();

        $org = Organization::with('address')
            ->with('referent')
            ->with('referentCyber')
            ->with('referentElu')
            ->with('territory')
            ->find($data['id']);

        $this->assertNotNull($org);
        $this->assertEquals($org->toArray(), $data);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithMissingReferentsShouldGiveValidationErrors()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
                'moreInfos' => 'ligne2',
                'cp' => '75001',
                'city' => 'Paris',
                'codeInsee' => '43555',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithMissingReferentShouldGiveValidationErrors()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
                'moreInfos' => 'ligne2',
                'cp' => '75001',
                'city' => 'Paris',
                'codeInsee' => '43555',
            ],
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithWrongReferentEmailShouldGiveValidationErrors()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
                'moreInfos' => 'ligne2',
                'cp' => '75001',
                'city' => 'Paris',
                'codeInsee' => '43555',
            ],
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'mauvais email',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_elu' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithoutAddressFieldShouldGiveValidationError()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
            ],
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_elu' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithoutReferentShouldGiveValidationError()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [
                'address' => 'ligne1',
                'moreInfos' => 'ligne2',
                'cp' => '75001',
                'city' => 'Paris',
                'codeInsee' => '43555',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithoutAddressShouldGiveValidationError()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_elu' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testCreateOrganizationWithEmptyAddressShouldGiveValidationError()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
            'address' => [],
            'referent' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_cyber' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
            'referent_elu' => [
                'civility' => 'Madame',
                'firstname' => 'Laurence',
                'lastname' => 'Fournier',
                'function' => 'Chef de service',
                'email' => 'll@mg.com',
                'phone' => '0546535456',
            ],
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testManagerCannotCreateOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(403);
    }

    /**
     * test create.
     */
    public function testUserCannotCreateOrganization()
    {
        $user = User::where('role', User::ROLE_USER)->first();

        $org = [
            'name' => 'tes org',
            'short_name' => 'tes-org',
            'type' => 'je sais pas',
            'siren' => 'ZER456YZ4',
            'active' => 1,
            'website' => 'http://test.fr',
            'info' => 'blabla',
        ];

        $response = $this->actingAs($user)->postJson(route('api.organizations.post'), $org);

        $response->assertStatus(403);
    }

    /**
     * test create.
     */
    public function testAdminCanUpdateOrganization()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = Organization::with('address')
            ->with('referent')
            ->with('referentCyber')
            ->with('referentElu')
            ->with('territory')
            ->first()->toArray()
        ;

        $org['name'] = 'NEW ORG';
        $org['info'] = 'NEW INFO';

        $response = $this->actingAs($user)->putJson(route('api.organizations.put', ['id' => $org['id']]), $org);

        $response->assertOk();

        $data = $response->json();

        $org = Organization::with('address')
            ->with('referent')
            ->with('referentCyber')
            ->with('referentElu')
            ->with('territory')
            ->find($data['id'])
        ;

        $this->assertNotNull($org);
        $this->assertEquals($org->toArray(), $data);
        $this->assertEquals('NEW ORG', $org->name);
        $this->assertEquals('NEW INFO', $org->info);
    }

    /**
     * test create.
     */
    public function testManagerCanUpdateOwnOrganization()
    {
        $user = User::with('organization')
            ->with('organization.address')
            ->with('organization.referent')
            ->with('organization.referentCyber')
            ->with('organization.referentElu')
            ->where('role', User::ROLE_MANAGER)
            ->first()
        ;

        $org = Organization::with(['address', 'referent', 'referentCyber', 'referentElu', 'territory'])
            ->find($user->organization_id)->toArray();

        // Manager cannot update info block of their own organization
        $oldName = $org['name'];
        $oldInfo = $org['info'];

        $org['name'] = 'NEW ORG';
        $org['info'] = 'NEW INFO';
        $org['bilan1'] = 'NEW Bilan';

        $response = $this->actingAs($user)->putJson(route('api.organizations.put', ['id' => $org['id']]), $org);

        $response->assertOk();

        $data = $response->json();

        $org = Organization::with('address')
            ->with('referent')
            ->with('referentCyber')
            ->with('referentElu')
            ->with('territory')
            ->find($data['id'])
        ;

        $this->assertNotNull($org);
        $this->assertEquals($org->toArray(), $data);
        $this->assertEquals($oldName, $org->name);
        $this->assertEquals($oldInfo, $org->info);
        $this->assertEquals('NEW Bilan', $org->bilan1);
    }

    /**
     * test create.
     */
    public function testManagerCannotUpdateOtherOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $org = Organization::where('id', '!=', $user->organization_id)->first()->toArray();

        $org['name'] = 'NEW ORG';
        $org['info'] = 'NEW INFO';

        $response = $this->actingAs($user)->putJson(route('api.organizations.put', ['id' => $org['id']]), $org);

        $response->assertStatus(403);
    }

    /**
     * test delete.
     */
    public function testAdminCanDeleteOrganization()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = Organization::first();

        $response = $this->actingAs($user)->deleteJson(route('api.organizations.delete', ['id' => $org->id]));

        $response->assertStatus(204);
    }

    /**
     * test delete.
     */
    public function testManagerCannotDeleteOrganization()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();

        $org = $user->organization;

        $response = $this->actingAs($user)->deleteJson(route('api.organizations.delete', ['id' => $org->id]));

        $response->assertStatus(403);
    }

    /**
     * test delete.
     */
    public function testUserCannotDeleteOrganization()
    {
        $user = User::where('role', User::ROLE_USER)->first();

        $org = $user->organization;

        $response = $this->actingAs($user)->deleteJson(route('api.organizations.delete', ['id' => $org->id]));

        $response->assertStatus(403);
    }
}
