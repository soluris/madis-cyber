<?php

namespace Tests\Feature;

use App\Models\Territory;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TerritoriesControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all.
     *
     * @return void
     */
    public function testAdminUserCanGetAll()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();
        $response = $this->actingAs($user)->getJson(route('api.territories.all'));
        if ($response->exception) {
            dump($response->exception);
        }

        $response->assertOk();
        $territories = Territory::with('organizations')
            ->get();
        $this->assertEquals($territories->toArray(), $response->json());
    }

    /**
     * test create.
     */
    public function testAdminCanCreateTerritory()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $territoryExample = ['name' => 'test territory'];

        $response = $this->actingAs($user)->postJson(route('api.territories.post'), $territoryExample);

        $data = $response->json();

        $territory = Territory::with('organizations')
            ->find($data['id']);

        $this->assertNotNull($territory);
        $this->assertEquals($territory->toArray(), $data);
    }

    /**
     * test create.
     */
    public function testCreateTerritoryWithMissingNameShouldGiveValidationErrors()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $org = [];

        $response = $this->actingAs($user)->postJson(route('api.territories.post'), $org);

        $response->assertStatus(422);
    }

    /**
     * test create.
     */
    public function testUserCannotCreateTerritory()
    {
        $user = User::where('role', '!=', User::ROLE_ADMIN)->first();
        $org = ['name' => 'test territory'];

        $response = $this->actingAs($user)->postJson(route('api.territories.post'), $org);
        $response->assertStatus(403);
    }

    /**
     * test create.
     */
    public function testAdminCanUpdateTerritory()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();

        $territory = Territory::first();
        $territory['name'] = 'NEW TER';

        $response = $this->actingAs($user)->putJson(route('api.territories.put', ['id' => $territory['id']]), $territory->toArray());
        $response->assertOk();

        $data = $response->json();
        $territory = Territory::with('organizations')
            ->find($data['id']);

        $this->assertNotNull($territory);
        $this->assertEquals($territory->toArray(), $data);
        $this->assertEquals('NEW TER', $territory->name);
    }

    /**
     * test delete.
     */
    public function testAdminCanDeleteTerritory()
    {
        $user = User::where('role', User::ROLE_ADMIN)->first();
        $territory = Territory::with('organizations')
            ->first();

        $response = $this->actingAs($user)->deleteJson(route('api.territories.delete', ['id' => $territory->id]));
        $response->assertStatus(204);
    }

    /**
     * test delete.
     */
    public function testManagerCannotDeleteTerritory()
    {
        $user = User::where('role', User::ROLE_MANAGER)->first();
        $territory = Territory::with('organizations')
            ->first();

        $response = $this->actingAs($user)->deleteJson(route('api.territories.delete', ['id' => $territory->id]));
        $response->assertStatus(403);
    }

    /**
     * test delete.
     */
    public function testUserCannotDeleteTerritory()
    {
        $user = User::where('role', User::ROLE_USER)->first();
        $territory = Territory::with('organizations')
            ->first();

        $response = $this->actingAs($user)->deleteJson(route('api.territories.delete', ['id' => $territory->id]));
        $response->assertStatus(403);
    }
}
