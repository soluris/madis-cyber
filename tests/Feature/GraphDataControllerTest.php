<?php

namespace Tests\Feature;

use App\Models\DangerLevel;
use App\Models\Evaluation;
use App\Models\MaturityAnswer;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GraphDataControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testCheckEvaluationMeasuresGraphData()
    {
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 3)
            ->first();

        // Set all levels to know quantity
        foreach ($evaluation->measureLevels as $measureLevel) {
            $measureLevel->actual_level = 2;
            $measureLevel->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.measures', ['id' => $evaluation->id]));

        $response->assertOk();

        $expected = [
            'labels' => [
                    0 => 'Droits d\'accès logiques',
                    1 => 'Cloisonnement réseaux',
                    2 => 'Modes dégradés',
                    3 => 'AV et outils complémentaires',
                    4 => 'Contrats',
                    5 => 'Plan informatique',
                    6 => 'Mots de passe',
                    7 => 'Sauvegardes',
                    8 => 'MAJ',
                    9 => 'Sensibilisation',
                ],
            'data' => [
                    0 => 2,
                    1 => 2,
                    2 => 2,
                    3 => 2,
                    4 => 2,
                    5 => 2,
                    6 => 2,
                    7 => 2,
                    8 => 2,
                    9 => 2,
                ],
        ];

        $this->assertEquals($expected, $response->json());
    }

    public function testCheckEvaluationRisksGraphData()
    {
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 3)
            ->first();

        // Set all levels to know quantity
        foreach ($evaluation->measureLevels as $k => $measureLevel) {
            $measureLevel->actual_level = $k % 3;
            $measureLevel->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.risks', ['id' => $evaluation->id]));

        $response->assertOk();

        $expected = [
            'labels' => [
                    0 => 'Sabotage, demande de rançon',
                    1 => 'Vol, recel de données, publication de données des citoyens',
                    2 => 'Détournement de fonds ',
                    3 => 'Détournement de ressources informatiques',
                    4 => 'Défiguration du site internet',
                    5 => 'Piratage de compte de messagerie',
                ],
            'data' => [
                    0 => 66.625,
                    1 => 64.83333333333333,
                    2 => 63.333333333333336,
                    3 => 66.625,
                    4 => 67.6,
                    5 => 66.66666666666667,
                ],
        ];

        $this->assertEquals($expected, $response->json());
    }

    public function testCheckEvaluationAttackGraphData()
    {
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 3)
            ->first();

        // Set all levels to know quantity
        foreach ($evaluation->measureLevels as $k => $measureLevel) {
            $measureLevel->actual_level = $k % 3;
            $measureLevel->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.attack', ['id' => $evaluation->id]));

        $response->assertOk();

        $data = $response->json();

        $expected = [
            'labels' => [
                    0 => 'Hameçonnage',
                    1 => 'Installation non maitrisée',
                    2 => 'Exploitation de vulnérabilités, défaut de mise à jour',
                    3 => 'Accès distant non sécurisée',
                    4 => 'Mot de passe faible',
                    5 => 'Ingénerie sociale',
                    6 => 'Défaut de paramétrage site web, outils de sécurité ',
                    7 => 'Usage d\'outils pro à titre privé',
                    8 => 'Réseau WIFI, VOIP non sécurisée',
                    9 => 'Intrusion via un prestataire ou éditeurs',
                ],
            'data' => [
                    0 => 60,
                    1 => 70,
                    2 => 61,
                    3 => 67,
                    4 => 74,
                    5 => 65,
                    6 => 65,
                    7 => 68,
                    8 => 68,
                    9 => 68,
                ],
        ];

        $this->assertEquals($expected, $data);
    }

    public function testCheckEvaluationExpositionGraphData()
    {
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 3)
            ->first();

        // Set all levels to know quantity
        foreach ($evaluation->measureLevels as $k => $measureLevel) {
            $measureLevel->actual_level = $k % 3;
            $measureLevel->save();
        }
        // Set all levels to know quantity
        foreach ($evaluation->dangerLevels as $k => $dangerLevel) {
            $dangerLevel->danger_level_id = DangerLevel::first()->id;
            $dangerLevel->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.exposition', ['id' => $evaluation->id]));

        $response->assertOk();

        $data = $response->json();

        $vals = collect($data['data']);

        $this->assertEquals(round(100, 2), round($vals->sum(), 2));

        $expected = [
            'labels' => [
                    0 => 'Sabotage, demande de rançon',
                    1 => 'Vol, recel de données, publication de données des citoyens',
                    2 => 'Détournement de fonds ',
                    3 => 'Détournement de ressources informatiques',
                    4 => 'Défiguration du site internet',
                    5 => 'Piratage de compte de messagerie',
                ],
            'data' => [
                    0 => 16.83795964786656,
                    1 => 16.385156480350446,
                    2 => 16.00606545638347,
                    3 => 16.83795964786656,
                    4 => 17.084368813445096,
                    5 => 16.848489954087867,
                ],
        ];

        $this->assertEquals($expected, $data);
    }

    public function testCheckEvaluationMaturityGraphData()
    {
        /**
         * @var Evaluation $evaluation
         */
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 5)
            ->where('status', Evaluation::STATUS_DONE)
            ->first();

        // Set all levels to know quantity
        foreach ($evaluation->measureLevels as $k => $measureLevel) {
            $measureLevel->actual_level = 3;
            $measureLevel->save();
        }

        foreach ($evaluation->maturityLevels as $k => $maturityLevel) {
            $maturityLevel->maturity_answer_id = MaturityAnswer::where('level', $k % 3)
                ->where('maturity_question_id', $maturityLevel->maturity_question_id)
                ->first()->id;

            $maturityLevel->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.maturity', ['id' => $evaluation->id]));

        $response->assertOk();

        $data = $response->json();

        $expected = [
            'labels' => [
                    0 => 'Connaissance',
                    1 => 'Organisation',
                    2 => 'Motivation',
                    3 => 'Exécution',
                    4 => 'Fondamentaux',
                ],
            'data' => [
                    0 => 0,
                    1 => 1,
                    2 => 2,
                    3 => 0,
                    4 => 3,
                ],
        ];

        $this->assertEquals($expected, $data);
    }

    public function testEvaluationBestMeasures()
    {
        /**
         * @var Evaluation $evaluation
         */
        $evaluation = Evaluation::with('dangerLevels')
            ->with('maturityLevels')
            ->with('measureLevels')
            ->where('current_step', '>=', 4)
            ->first();

        // Set all measure levels to known quantity

        foreach ($evaluation->measureLevels as $k => $measureLevel) {
            $measureLevel->actual_level = $k % 4;
            $measureLevel->save();
        }

        // Set all dangers levels to known quantity
        $dangerLevel = DangerLevel::where('value', 25)->first();
        foreach ($evaluation->dangerLevels as $k => $dangerLevelEval) {
            $dangerLevelEval->danger_level_id = $dangerLevel->id;
            $dangerLevelEval->save();
        }

        $admin = User::where('role', User::ROLE_ADMIN)->first();

        $response = $this->actingAs($admin)->getJson(route('api.evaluations.graph.best_measures', ['id' => $evaluation->id]));

        $response->assertOk();

        $data = $response->json();

        $expected = [
            0 => 1,
            1 => 3,
            2 => 2,
            3 => 9,
            4 => 5,
            5 => 7,
            6 => 6,
            7 => 10,
            8 => 4,
            9 => 8,
        ];

        $this->assertEquals($expected, $data);
    }
}
