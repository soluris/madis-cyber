<?php

use App\Http\Controllers\Api\DangersController;
use App\Http\Controllers\Api\EvaluationsController;
use App\Http\Controllers\Api\GraphDataController;
use App\Http\Controllers\Api\MaturityController;
use App\Http\Controllers\Api\MeasuresController;
use App\Http\Controllers\Api\OrganizationsController;
use App\Http\Controllers\Api\ScenariosController;
use App\Http\Controllers\Api\TerritoriesController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/impersonate', [LoginController::class, 'impersonate'])->name('api.impersonate');
Route::post('/leave', [LoginController::class, 'leaveImpersonation'])->name('api.leaveImpersonation');

Route::prefix('/users')->middleware('auth:sanctum')->group(function () {
    Route::get('/me', [UsersController::class, 'me'])->name('api.users.me');
    Route::get('/', [UsersController::class, 'all'])->name('api.users.all');
    Route::post('/', [UsersController::class, 'save'])->name('api.users.post');
    Route::put('/{id}', [UsersController::class, 'save'])->name('api.users.put');
    Route::delete('/{id}', [UsersController::class, 'delete'])->name('api.users.delete');
});

Route::post('/forgetpwd', [UsersController::class, 'forgetpwd'])->name('api.forgetpwd');

Route::prefix('/territories')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [TerritoriesController::class, 'all'])->name('api.territories.all');
    Route::post('/', [TerritoriesController::class, 'save'])->name('api.territories.post');
    Route::put('/{id}', [TerritoriesController::class, 'save'])->name('api.territories.put');
    Route::delete('/{id}', [TerritoriesController::class, 'delete'])->name('api.territories.delete');
});

Route::prefix('/organizations')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [OrganizationsController::class, 'all'])->name('api.organizations.all');
    Route::post('/', [OrganizationsController::class, 'save'])->name('api.organizations.post');
    Route::put('/{id}', [OrganizationsController::class, 'save'])->name('api.organizations.put');
    Route::delete('/{id}', [OrganizationsController::class, 'delete'])->name('api.organizations.delete');
});
Route::prefix('/measures')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [MeasuresController::class, 'all'])->name('api.measures.all');
    Route::put('/{id}', [MeasuresController::class, 'save'])->name('api.measures.put');
    Route::get('/download', [MeasuresController::class, 'downloadAll'])->name('api.measures.downloadAll');
    Route::get('/download/{id}', [MeasuresController::class, 'download'])->name('api.measures.download');
});
Route::prefix('/evaluations')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [EvaluationsController::class, 'all'])->name('api.evaluations.all');
    Route::put('/{id}', [EvaluationsController::class, 'save'])->name('api.evaluations.put');
    Route::post('/', [EvaluationsController::class, 'save'])->name('api.evaluations.post');
    Route::get('/{id}', [EvaluationsController::class, 'get'])->name('api.evaluations.get');
    Route::get('/{id}/graphs/measures', [GraphDataController::class, 'measures'])->name('api.evaluations.graph.measures');
    Route::get('/{id}/graphs/risks', [GraphDataController::class, 'risks'])->name('api.evaluations.graph.risks');
    Route::get('/{id}/graphs/futurerisks', [GraphDataController::class, 'futurerisks'])->name('api.evaluations.graph.futurerisks');
    Route::get('/{id}/graphs/exposition', [GraphDataController::class, 'exposition'])->name('api.evaluations.graph.exposition');
    Route::get('/{id}/graphs/attack', [GraphDataController::class, 'attack'])->name('api.evaluations.graph.attack');
    Route::get('/{id}/graphs/maturity', [GraphDataController::class, 'maturity'])->name('api.evaluations.graph.maturity');
    Route::get('/{id}/graphs/best_measures', [GraphDataController::class, 'bestMeasures'])->name('api.evaluations.graph.best_measures');
    Route::delete('/{id}', [EvaluationsController::class, 'delete'])->name('api.evaluations.delete');
});

Route::prefix('/graphs')->middleware('auth:sanctum')->group(function () {
    Route::get('/actionterritory', [GraphDataController::class, 'actionterritory'])->name('api.evaluations.graph.actionterritory');
    Route::get('/organizations/{territory_id?}', [GraphDataController::class, 'organizations'])->name('api.evaluations.graph.organizations');
});

Route::prefix('/dangers')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [DangersController::class, 'all'])->name('api.dangers.all');
});
Route::prefix('/questions')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [MaturityController::class, 'all'])->name('api.maturity.all');
});
Route::prefix('/scenarios')->middleware('auth:sanctum')->group(function () {
    Route::get('/', [ScenariosController::class, 'all'])->name('api.scenarios.all');
});
