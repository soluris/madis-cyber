<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/login', function () {
    if (Auth::user()) {
        return redirect(route('home'));
    }

    return view('app');
});

Route::post('/newpassword', [UsersController::class, 'storePassword'])->name('password_reset.send');
Route::get('/newpassword/{id}/{reset_token}', [UsersController::class, 'createPassword'])->name('password_reset');

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('/export')->group(function () {
        Route::get('/structures.{ext}', [ExportController::class, 'organizations'])
            ->whereIn('ext', ['csv', 'xlsx', 'json'])->name('export.organizations');
        Route::get('/evaluations.{ext}', [ExportController::class, 'evaluations'])
            ->whereIn('ext', ['csv', 'xlsx', 'json'])->name('export.evaluations');
    });
});

Route::middleware('auth:sanctum')->prefix('/pdf')->group(function () {
    Route::get('/planAction/{organization_id}', [PdfController::class, 'ActionPlanPdf'])->name('pdf.plan_action');
    Route::get('/politiqueSecurite/{organization_id}', [PdfController::class, 'PolitiqueSecuritePdf'])->name('pdf.politique_securite');
    Route::get('/dossierCyberSecurite/{organization_id}/{evaluation_id?}', [PdfController::class, 'DossierCyberSecuritePdf'])->name('pdf.dossier_cyber_securite');
});

Route::middleware('auth:sanctum')->get('/{any}', function () {
    return view('app');
})->where('any', '^(?!storage_).+');

Route::get('/', function () {
    if (!Auth::user()) {
        return redirect(route('login'));
    }

    return view('app');
})->name('home');
